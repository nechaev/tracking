var profile = {
    basePath: "../",
    releaseDir: "./",
    releaseName: "public",
    action: "release",

    layerOptimize: "closure",
    //layerOptimize: false,
    //optimize: false,
    cssOptimize: "comments",
    mini: false,
    stripConsole: "warn",
    selectorEngine: "acme",

    defaultConfig: {
        parseOnLoad: true,
        baseUrl: '/assets/dojo',
        async: false
    },

    staticHasFeatures: {
        "config-deferredInstrumentation": 1,
        "config-useDeferredInstrumentation": 1,
        "config-dojo-loader-catches": 0,
        "config-tlmSiblingOfDojo": 0,
        "dijit-legacy-requires": 0,
        "dojo-amd-factory-scan": 0,
        "dojo-combo-api": 0,
        "dojo-config-api": 1,
        "dojo-config-require": 0,
        "dojo-debug-messages": 0,
        "dojo-dom-ready-api": 1,
        "dojo-firebug": 0,
        "dojo-guarantee-console": 1,
        "dojo-has-api": 1,
        "dojo-inject-api": 1,
        "dojo-loader": 1,
        "dojo-log-api": 0,
        "dojo-modulePaths": 0,
        "dojo-moduleUrl": 0,
        "dojo-publish-privates": 0,
        "dojo-requirejs-api": 1,
        "dojo-sniff": 0,
        "dojo-sync-loader": 1,
        "dojo-test-sniff": 0,
        "dojo-timeout-api": 0,
        "dojo-trace-api": 0,
        "dojo-undef-api": 0,
        "dojo-v1x-i18n-Api": 1,
        "dom": 1,
        "host-browser": 1,
        "host-node": 0,
        "extend-dojo": 1
    },

    packages: [
        {
            name: "tracking",
            location: "app/assets/tracking"
        },
        {
            name: "comet",
            location: "app/assets/comet"
        },
        {
            name: "gis",
            location: "app/assets/gis"
        },
        {
            name: "tools",
            location: "app/assets/tools"
        },
        {
            name: "types",
            location: "app/assets/types"
        },
        {
            name: "dijit",
            location: "../../tools/dojo/dijit"
        },
        {
            name: "dojo",
            location: "../../tools/dojo/dojo"
        },
        {
            name: "dgrid",
            location: "../../tools/dojo/dgrid"
        },
        {
            name: "xstyle",
            location: "../../tools/dojo/xstyle"
        },
        {
            name: "put-selector",
            location: "../../tools/dojo/put-selector"
        },
        {
            name: "modules"
        }
    ],

    layers: {
        "modules/login": {
            include: [ "dijit/dijit", "tracking/login/LoginDialog" ],
            customBase: true,
            boot: true
        },
        "modules/dashboard": {
            include: [ "dijit/dijit", "tracking/DashboardGrid", "tracking/ui/AppBar" ],
            customBase: true,
            boot: true
        },
        "modules/tracking": {
            include: [ "dijit/dijit", "tracking/MapView", "tracking/ui/LeftFoldingWindow", "tracking/ui/AppBar" ],
            customBase: true,
            boot: true
        },
        "modules/reports/mileage": {
            include: [ "dijit/dijit", "tracking/reports/mileage", "tracking/ui/AppBar" ],
            customBase: true,
            boot: true
        },
        "modules/admin-trackers": {
            include: [ "dijit/dijit", "tracking/AdminTrackersGrid", "tracking/AdminTrackersToolbar",
                "tracking/AdminGroupsGrid", "tracking/AdminGroupsToolbar", "tracking/GroupTrackerButtons", "tracking/ui/AppBar" ],
            customBase: true,
            boot: true
        }
    }

};
