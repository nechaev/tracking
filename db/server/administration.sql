ALTER TABLE trackers ADD owner INT;
ALTER TABLE trackers ADD FOREIGN KEY (owner) REFERENCES users;

ALTER TABLE trackerGroups ADD owner INT;
ALTER TABLE trackerGroups ADD FOREIGN KEY (owner) REFERENCES users;


