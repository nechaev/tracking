CREATE OR REPLACE VIEW TRACKING_V_TRACKERS AS
  SELECT
    t.id        ID,
    t.trackerId TRACKERID,
    t.name      TRACKERNAME
  FROM trackers t;

CREATE TABLE TRACKING_TRACKER_SELECTION (
  selectionId INT,
  trackerId VARCHAR(40) NOT NULL
);
CREATE INDEX trackerSelectionIndex ON TRACKING_TRACKER_SELECTION(selectionId);

CREATE OR REPLACE VIEW TRACKING_V_MILEAGE AS
  SELECT
    p.tracker                      TRACKER,
    p.eventTime                    EVENTTIME,
    date_trunc('day', p.eventTime) EVENTDATE,
    p.distance / 1000              MILEAGE
  FROM positionEvents p;

CREATE OR REPLACE VIEW TRACKING_V_PARKINGS AS
  SELECT
    m.tracker                             TRACKER,
    m.eventTime                           EVENTTIME,
    EXTRACT(MILLISECONDS FROM m.duration) DURATION
  FROM boolValuedEvents m
  WHERE m.eventType = -1 AND NOT m.value;

CREATE OR REPLACE VIEW TRACKING_V_USERTRACKERS AS
  SELECT DISTINCT
    T.id id,
    T.trackerId,
    T.name,
    U.userId,
    U.domain userDomain,
    G.name groupname
  FROM trackerGroups G
  INNER JOIN userTrackerGroups UG ON G.id = UG."group"
  INNER JOIN users U ON UG."user" = U.id
  INNER JOIN trackerGroupTrackers GT ON G.id = GT."group"
  INNER JOIN trackers T ON GT.trackerId = T.trackerId;

CREATE OR REPLACE VIEW TRACKING_V_DASHBOARD AS
  SELECT tracker, EVENTTIME, distance / 1000 MILEAGE, 0 ENGINEHOURS, 0 GPSLOSTTIME, 0 PARKINGTIME
  FROM positionEvents
  UNION ALL
  SELECT i.tracker, i.EVENTTIME, 0, EXTRACT(MILLISECONDS FROM i.DURATION), 0, 0
  FROM boolValuedEvents i INNER JOIN trackers t ON i.tracker = t.id AND i.eventType = t.ignitionSwitch WHERE "value"
  UNION ALL
  SELECT tracker, EVENTTIME, 0, 0, EXTRACT(MILLISECONDS FROM DURATION), 0
  FROM boolValuedEvents WHERE eventType = -2 AND NOT "value"
  UNION ALL
  SELECT tracker, EVENTTIME, 0, 0, 0, EXTRACT(MILLISECONDS FROM DURATION)
  FROM boolValuedEvents WHERE eventType = -1 AND NOT "value";
