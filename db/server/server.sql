CREATE TABLE trackers (
  id SERIAL PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  trackerId VARCHAR(40) NOT NULL UNIQUE,
  trackerType VARCHAR(10),
  ignitionSwitch INT,
  fuelSensor INT
);

CREATE TABLE trackerKeys (
  tracker INTEGER NOT NULL,
  trackerKey VARCHAR(100) NOT NULL UNIQUE,
  FOREIGN KEY (tracker) REFERENCES trackers
);

CREATE TABLE positionEvents (
  tracker INTEGER NOT NULL,
  eventTime TIMESTAMP NOT NULL,
  latitude FLOAT8 NOT NULL,
  longitude FLOAT8 NOT NULL,
  altitude FLOAT4 NULL,
  speed FLOAT4 NULL,
  heading FLOAT4 NULL,
  accuracy FLOAT4 NULL,
  duration INTERVAL NULL,
  distance FLOAT4 NULL,
  UNIQUE(tracker, eventTime),
  FOREIGN KEY (tracker) REFERENCES trackers
);

CREATE TABLE boolValuedEvents (
  tracker INTEGER NOT NULL,
  eventTime TIMESTAMP NOT NULL,
  eventType INTEGER NOT NULL,
  value BOOLEAN NOT NULL,
  duration INTERVAL,
  UNIQUE(tracker, eventType, eventTime),
  FOREIGN KEY (tracker) REFERENCES trackers
);

CREATE TABLE floatValuedEvents (
  tracker INTEGER NOT NULL,
  eventTime TIMESTAMP NOT NULL,
  eventType INTEGER NOT NULL,
  value FLOAT NOT NULL,
  duration INTERVAL,
  UNIQUE(tracker, eventType, eventTime),
  FOREIGN KEY (tracker) REFERENCES trackers
);

CREATE TABLE signalEvents (
  tracker INTEGER NOT NULL,
  eventTime TIMESTAMP NOT NULL,
  eventType INTEGER NOT NULL,
  UNIQUE(tracker, eventTime),
  FOREIGN KEY (tracker) REFERENCES trackers
);

CREATE TABLE textMessageEvents (
  tracker INTEGER NOT NULL,
  eventTime TIMESTAMP NOT NULL,
  eventType INTEGER NOT NULL,
  text TEXT NOT NULL,
  UNIQUE(tracker, eventTime),
  FOREIGN KEY (tracker) REFERENCES trackers
);

CREATE TABLE pendingAggregateCalculations (
  tracker INT NOT NULL UNIQUE,
  fromTime TIMESTAMP NULL,
  toTime TIMESTAMP NULL,
  FOREIGN KEY (tracker) REFERENCES trackers
);

CREATE TABLE receivedPackets (
  receivedTime TIMESTAMP NOT NULL,
  fromTime TIMESTAMP NOT NULL,
  toTime TIMESTAMP NOT NULL,
  eventCount INT NOT NULL,
  tracker INT,
  FOREIGN KEY (tracker) REFERENCES trackers
);

CREATE FUNCTION greatCircleDistance(lat1 FLOAT, lon1 FLOAT, lat2 FLOAT, lon2 FLOAT) RETURNS FLOAT AS $greatCircleDistance$
  BEGIN
    RETURN 6371000 * 2 * asin(sqrt(
      (sin(PI()*(lat2 - lat1)/360))^2 + (sin(PI()*(lon2 - lon1)/360))^2 *
        cos(PI()*(lat1)/180) * cos(PI()*(lat2)/180)
      ));
END;
$greatCircleDistance$ LANGUAGE plpgsql;

CREATE FUNCTION fccDistance(lat1 FLOAT, lon1 FLOAT, lat2 FLOAT, lon2 FLOAT) RETURNS FLOAT AS $fccDistance$
  DECLARE
    phi FLOAT;
    param1 FLOAT;
    param2 FLOAT;
  BEGIN
    phi = PI() * (lat1 + lat2)/360;
    param1 = 111132.09 - 566.05 * cos(2 * phi) + 1.20 * cos(4 * phi);
    param2 = 111415.13 * cos(phi) - 94.55 * cos(3 * phi) + 0.12 * cos(5 * phi);
    RETURN sqrt((param1 * (lat2 - lat1))^2 + (param2 * (lon2 - lon1))^2);
  END;
$fccDistance$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION updatePendingAggregateCalculation() RETURNS TRIGGER AS $updatePendingAggregateCalculation$
DECLARE
  eventTimeFrom TIMESTAMP;
  eventTimeTo TIMESTAMP;
BEGIN
  SELECT fromTime, toTime INTO eventTimeFrom, eventTimeTo FROM pendingAggregateCalculations
    WHERE tracker = NEW.tracker FOR UPDATE;
  IF FOUND THEN
    IF eventTimeFrom = NULL THEN
      eventTimeFrom = NEW.eventTime;
    ELSE
      eventTimeFrom = LEAST(eventTimeFrom, NEW.eventTime);
    END IF;
    IF eventTimeTo = NULL THEN
      eventTimeTo = NEW.eventTime;
    ELSE
      eventTimeTo = GREATEST(eventTimeTo, NEW.eventTime);
    END IF;

    UPDATE pendingAggregateCalculations
    SET fromTime = eventTimeFrom, toTime = eventTimeTo
    WHERE tracker = NEW.tracker;
  ELSE
    INSERT INTO pendingAggregateCalculations(tracker, fromTime, toTime) VALUES(NEW.tracker, NEW.eventTime, NEW.eventTime);
  END IF;
  RETURN NULL;
END;
$updatePendingAggregateCalculation$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION positionEventRecalculateAggregates(tracker_id INT, startTime TIMESTAMP, endTime TIMESTAMP)
  RETURNS VOID AS $positionEventRecalculateAggregates$
DECLARE
  fromEventTime TIMESTAMP;
  toEventTime TIMESTAMP;
  prev RECORD;
  curr RECORD;
  notFirst BOOLEAN;
BEGIN
  SELECT eventTime INTO fromEventTime FROM positionEvents
    WHERE tracker = tracker_id AND eventTime < startTime
    LIMIT 1;
  IF NOT FOUND THEN
    fromEventTime = startTime;
  END IF;
  SELECT eventTime INTO toEventTime FROM positionEvents
  WHERE tracker = tracker_id AND eventTime > endTime
  LIMIT 1;
  IF NOT FOUND THEN
    toEventTime = endTime;
  END IF;

  notFirst = false;
  FOR curr IN SELECT * FROM positionEvents
              WHERE tracker = tracker_id AND eventTime BETWEEN fromEventTime AND toEventTime
              ORDER BY eventTime LOOP
    IF notFirst THEN
      UPDATE positionEvents SET duration = curr.eventTime - prev.eventtime,
        distance = fccDistance(prev.latitude, prev.longitude, curr.latitude, curr.longitude)
      WHERE tracker = tracker_id and eventTime = prev.eventTime;
    END IF;
    notFirst = true;
    prev = curr;
  END LOOP;
END;
$positionEventRecalculateAggregates$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION boolEventRecalculateAggregates(tracker_id INT, startTime TIMESTAMP, endTime TIMESTAMP)
  RETURNS VOID AS $boolEventRecalculateAggregates$
DECLARE
  fromEventTime TIMESTAMP;
  toEventTime TIMESTAMP;
  prev RECORD;
  curr RECORD;
  notFirst BOOLEAN;
  mEventType INT;
BEGIN
  FOR mEventType IN SELECT DISTINCT eventType FROM boolValuedEvents WHERE eventTime BETWEEN startTime AND endTime LOOP

    SELECT eventTime INTO fromEventTime FROM boolValuedEvents
      WHERE tracker = tracker_id AND eventType = mEventType AND eventTime < startTime
      LIMIT 1;
    IF NOT FOUND THEN
      fromEventTime = startTime;
    END IF;
    SELECT eventTime INTO toEventTime FROM boolValuedEvents
    WHERE tracker = tracker_id AND eventType = mEventType AND eventTime > endTime
    LIMIT 1;
    IF NOT FOUND THEN
      toEventTime = endTime;
    END IF;

    notFirst = false;
    FOR curr IN SELECT * FROM boolValuedEvents
                WHERE tracker = tracker_id AND eventType = mEventType AND eventTime BETWEEN fromEventTime AND toEventTime
                ORDER BY eventTime LOOP
      IF notFirst THEN
        UPDATE boolValuedEvents SET duration = curr.eventTime - prev.eventtime
        WHERE tracker = tracker_id AND eventType = mEventType and eventTime = prev.eventTime;
      END IF;
      notFirst = true;
      prev = curr;
    END LOOP;

  END LOOP;
END;
$boolEventRecalculateAggregates$ LANGUAGE plpgsql;

-- TODO: same function for floatValuedEvents


CREATE OR REPLACE FUNCTION doPendingAggregateCalculations() RETURNS VOID AS $doPendingAggregateCalculations$
  DECLARE
    pendingRange RECORD;
  BEGIN
  FOR pendingRange IN SELECT * FROM pendingAggregateCalculations
                      WHERE fromTime IS NOT NULL AND toTime IS NOT NULL FOR UPDATE LOOP
    PERFORM positionEventRecalculateAggregates(pendingRange.tracker, pendingRange.fromTime, pendingRange.toTime);
    PERFORM boolEventRecalculateAggregates(pendingRange.tracker, pendingRange.fromTime, pendingRange.toTime);
    PERFORM floatEventRecalculateAggregates(pendingRange.tracker, pendingRange.fromTime, pendingRange.toTime);

    UPDATE pendingAggregateCalculations SET fromTime = NULL, toTime = NULL
      WHERE tracker = pendingRange.tracker;
  END LOOP;
END;
$doPendingAggregateCalculations$ LANGUAGE plpgsql;


CREATE TRIGGER positionEventUpdatePendingAggregateOnInsert AFTER INSERT
ON positionEvents FOR EACH ROW
EXECUTE PROCEDURE updatePendingAggregateCalculation();
CREATE TRIGGER boolEventUpdatePendingAggregateOnInsert AFTER INSERT
ON boolValuedEvents FOR EACH ROW
EXECUTE PROCEDURE updatePendingAggregateCalculation();
CREATE TRIGGER floatEventUpdatePendingAggregateOnInsert AFTER INSERT
ON floatValuedEvents FOR EACH ROW
EXECUTE PROCEDURE updatePendingAggregateCalculation();
