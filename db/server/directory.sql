CREATE TABLE domains (
  id VARCHAR(50) PRIMARY KEY,
  name VARCHAR(100),
  reportingDataSource VARCHAR(30),
  geocodingServiceName VARCHAR(30)
);

CREATE TABLE domainProperties (
  domain VARCHAR(50),
  propertyKey VARCHAR(50),
  propertyValue VARCHAR(100),
  FOREIGN KEY (domain) REFERENCES domains
);
CREATE INDEX domainPropertiesIndex ON domainProperties(domain, propertyKey);

CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  domain VARCHAR(50) NOT NULL,
  userId VARCHAR(50) NOT NULL,
  name VARCHAR(100) NOT NULL,
  email VARCHAR(50) NULL,
  country VARCHAR(30) NULL,
  lang VARCHAR(10) NULL,
  UNIQUE(domain, userId),
  FOREIGN KEY (domain) REFERENCES domains
);

CREATE TABLE userCredentials (
  "user" INTEGER,
  credential TEXT,
  FOREIGN KEY ("user") REFERENCES users
);
CREATE INDEX userCredentialsIndex ON userCredentials("user");

CREATE TABLE trackerGroups (
  id SERIAL PRIMARY KEY,
  name VARCHAR(50)
);
CREATE TABLE trackerGroupTrackers (
  "group" INTEGER,
  trackerId VARCHAR(40),
  FOREIGN KEY ("group") REFERENCES trackerGroups
);
CREATE INDEX trackerGroupTrackersIndex ON trackerGroupTrackers("group");

CREATE TABLE userTrackerGroups (
  "user" INTEGER,
  "group" INTEGER,
  FOREIGN KEY ("user") REFERENCES users,
  FOREIGN KEY ("group") REFERENCES trackerGroups
);
CREATE INDEX userTrackerGroupsIndex ON userTrackerGroups("user");
