name := "tracking"

organization := "ru.osfb"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
    jdbc,
    anorm,
    "com.typesafe.akka" %% "akka-actor" % "2.3.0",
//    "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.0.0", //To be enabled on Scala 2.11
    "org.eclipse.birt.runtime" % "org.eclipse.birt.runtime" % "4.2.2",
    "com.google.zxing" % "core" % "2.2",
    "com.google.zxing" % "javase" % "2.2"
//    "ru.osfb.autotracker" %% "reports" % "0.1-SNAPSHOT"
  )


scalaVersion := "2.10.3"

scalacOptions += "-feature"

resolvers += "Local maven repository" at "file:///"+Path.userHome.absolutePath+"/.m2/repository"

play.Project.playScalaSettings

javascriptEntryPoints := PathFinder.empty //<<= (sourceDirectory in Compile)(base => base / "assets" ** "*.js")

lessEntryPoints := PathFinder.empty //<<= (sourceDirectory in Compile)(base => base / "assets" ** "*.less")
