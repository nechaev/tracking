import framework.ModuleLoader
import play.api._

object Global extends GlobalSettings {
  override def onStart(app: Application) {
    super.onStart(app)
    ModuleLoader.startup(app.configuration.getConfig("modules").getOrElse(throw new RuntimeException("Module configuration not found")))
  }
}
