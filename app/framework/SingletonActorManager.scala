package framework

import scala.reflect.ClassTag
import play.api.libs.concurrent.Akka
import play.api.Play.current

/**
 * Created by: sgl
 * Date: 11.02.13
 */
abstract class SingletonActorManager[T](implicit classTag: ClassTag[T]) {
  lazy val instancePath = ModuleLoader.instancesOf(classTag.runtimeClass).head
  def apply() = Akka.system.actorSelection(instancePath)
}
