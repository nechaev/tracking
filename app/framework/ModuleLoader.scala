package framework

import akka.actor.{Actor, ActorRef, ActorPath, Props}
import play.api.Configuration
import play.api.libs.concurrent.Akka
import play.api.Play.current
import org.slf4j.LoggerFactory
import com.typesafe.config.ConfigValueType
import scala.collection.JavaConversions._
import scala.language.existentials

/**
 * User: vnechaev
 * Date: 16.09.13
 */
object ModuleLoader {
  private val log = LoggerFactory.getLogger(this.getClass)

  private var moduleClassIndex: Map[Class[_], List[ActorPath]] = Map.empty

  private var moduleInstances: Map[String, ActorRef] = Map.empty

  def startup(configuration: Configuration) {
    configuration.subKeys foreach runModule(configuration)
  }

  def runModule(configuration: Configuration)(key: String) {
    if (!moduleInstances.keySet.contains(key)) {
      val entryType = configuration.underlying.getValue(key).valueType()
      log.info("Starting module {}", key)
      val props = entryType match {
        case ConfigValueType.STRING => Props(Class.forName(configuration.getString(key).getOrElse(throw new RuntimeException())))
        case ConfigValueType.OBJECT => {
          val conf = configuration.getConfig(key).getOrElse(throw new RuntimeException())
          conf.getStringList("dependsOn").foreach(deps => deps foreach runModule(configuration))
          Props(Class.forName(conf.getString("moduleClass").getOrElse(throw new RuntimeException())), conf)
        }
        case _ => throw new IllegalArgumentException
      }
      val actorRef = Akka.system.actorOf(props, key)
      moduleInstances += key -> actorRef

      def createIndex(cls :Class[_], ref: ActorPath) {
        moduleClassIndex += cls -> (ref :: moduleClassIndex.get(cls).getOrElse(Nil))
        if (!cls.isInterface) {
          for (childCls <- cls.getInterfaces) if (childCls != classOf[Actor]) createIndex(childCls, ref)
          val parent = cls.getSuperclass
          if (parent != null && parent != classOf[Actor]) createIndex(parent, ref)
        }
      }

      createIndex(props.actorClass(), actorRef.path)
    }
  }

  def instancesOf(moduleClass: Class[_]) = moduleClassIndex.get(moduleClass).getOrElse(Nil)
}
