package framework

import gis.Coordinates
import org.joda.time.{Duration, LocalDate, Instant}
import play.api.data.validation.ValidationError
import play.api.libs.json._
import user.{UserManager, UserId, User}
import org.joda.time.format.ISODateTimeFormat

/**
 * Created by: sgl
 * Date: 28.01.13
 */
object JsonFormats {

  private object InstantWrites extends Writes[Instant] {
    def writes(o: Instant) = JsNumber(BigDecimal(o.getMillis))
  }
  private object InstantReads extends Reads[Instant] {
    def reads(json: JsValue) = json match {
      case JsNumber(n) => JsSuccess(new Instant(n.toLong))
      case _ => JsError(Seq(JsPath() -> Seq(ValidationError("validate.error.expected.jsnumber"))))
    }
  }
  implicit val instantFormat = Format(InstantReads, InstantWrites)

  private object DurationReads extends Reads[Duration] {
    def reads(json: JsValue): JsResult[Duration] = json match {
      case JsNumber(n) => JsSuccess(new Duration(n))
      case _ => JsError(Seq(JsPath() -> Seq(ValidationError("validate.error.expected.jsnumber"))))
    }
  }
  private object DurationWrites extends Writes[Duration] {
    def writes(o: Duration) = JsNumber(BigDecimal(o.getMillis))
  }
  implicit val durationFormat = Format(DurationReads, DurationWrites)

  private object LocalDateWrites extends Writes[LocalDate] {
    def writes(o: LocalDate): JsValue = JsString(ISODateTimeFormat.date().print(o))
  }
  private object LocalDateReads extends Reads[LocalDate] {
    def reads(json: JsValue): JsResult[LocalDate] = json match {
      case JsString(jsString) => JsSuccess(ISODateTimeFormat.date().parseLocalDate(jsString))
      case _ => JsError("validate.error.expected.jsstring-pattern-YYYY-MM-DD")
    }
  }
  implicit val localDateFormat = Format(LocalDateReads, LocalDateWrites)
  private object userIdReads extends Reads[UserId] {
    def reads(json: JsValue): JsResult[UserId] = json match {
      case JsString(str) => UserManager.parseUserIdOpt(str).map(JsSuccess(_)).getOrElse(JsError("Cannot parse userId"))
      case _ => JsError("Expected JsString")
    }
  }
  private object userIdWrites extends Writes[UserId] {
    def writes(o: UserId): JsValue = JsString(o.toString)
  }
  implicit val userIdFormat = Format(userIdReads, userIdWrites)

  implicit val coordinatesFormat = Json.format[Coordinates]
  implicit val userFormat = Json.format[User]
}
