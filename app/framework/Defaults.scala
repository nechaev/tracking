package framework

import akka.util.Timeout
import scala.concurrent.duration._
import scala.language.postfixOps

/**
 * User: vnechaev
 * Date: 28.05.13
 */
object Defaults {
  implicit val timeout: Timeout = 60 seconds
}
