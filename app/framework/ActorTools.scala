package framework

import akka.pattern.pipe
import scala.concurrent.Future
import akka.actor.Actor
import akka.event.LoggingAdapter

/**
 * Author: sgl
 * Date: 06.07.13
 */
trait ActorTools {
  self: Actor =>
  import context.dispatcher
  protected def asyncReply[T](fn: =>T) {
    Future(fn) pipeTo sender
  }

  protected def logThrows[T](fn: =>T)(implicit log: LoggingAdapter) = try {
    fn
  } catch {
    case e: Throwable => {
      log.error(e, "DB operation failed")
      throw new RuntimeException(e)
    }
  }
}
