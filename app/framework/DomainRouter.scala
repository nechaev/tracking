package framework

import akka.actor.{Props, Actor, ActorRef, ActorSelection}
import org.slf4j.LoggerFactory
import play.api.libs.concurrent.Akka
import play.api.Play.current

/**
 * User: vnechaev
 * Date: 17.09.13
 */

abstract class DomainRouter {
  private val log = LoggerFactory.getLogger(this.getClass)

  type Classifier
  type RoutableMessage
  def classify(msg: RoutableMessage): Classifier

  private var handlers: Map[Classifier, ActorSelection] = Map.empty

  def route(domain: Classifier, handler: ActorSelection) {
    handlers += domain -> handler
    log.info("Added route: {} -> {}", domain, handler)
  }
  def route(domain: Classifier, handler: ActorRef) {
    route(domain, Akka.system.actorSelection(handler.path))
  }

  def get(domain:Classifier) = handlers.get(domain)

  lazy val routerInstance = Akka.system.actorOf(Props(classOf[RouterActor[this.type]], this))

  def apply() = Akka.system.actorSelection(routerInstance.path)

}

class RouterActor[T<:DomainRouter](domainRouter: T) extends Actor {
  def receive: Actor.Receive = {
    case message => {
      val msg = message.asInstanceOf[RouterActor.this.domainRouter.RoutableMessage]
      domainRouter.get(domainRouter.classify(msg)).foreach(_.tell(msg, sender))
    }
  }
}