package framework

import anorm._
import java.sql.Connection
import scala.concurrent.Future
import play.api.db.DB
import play.api.Play.current
import akka.actor.Actor
import akka.event.LoggingAdapter

/**
 * Author: sgl
 * Date: 08.09.13
 */
object DbTools {
  def buildInClauseForSeq(inValues: Seq[Any], prefix:String = "p") = {
    val paramNames = (1 to inValues.length).map(prefix + _.toString)
    (paramNames map ("{"  + _ + "}") reduce (_ + "," + _), paramNames zip inValues.map(toParameterValue(_)))
  }
}

trait ActorDbTools extends ActorTools {
  self: Actor =>

  protected def dataSource: String

  protected def asyncDbOp(block: Connection => Unit)(implicit log: LoggingAdapter) {
    import context.dispatcher
    Future {
      logThrows {
        DB.withTransaction(dataSource)(block)
      }
    }
  }

  protected def asyncReplyDbOp(block: Connection => Any)(implicit log: LoggingAdapter) =
    asyncReply {
      logThrows {
        DB.withTransaction(dataSource)(block)
      }
    }

}
