package controllers

import akka.pattern.ask
import framework.Defaults.timeout
import play.api.mvc._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.Some
import session.SessionManager
import session.SessionManager.ConnectSession
import scala.concurrent.Future
import org.slf4j.LoggerFactory

/**
 * Created by: sgl
 * Date: 11.04.13
 */
object EntryPoints extends Controller {
  private val log = LoggerFactory.getLogger(this.getClass)

  def login = Action { implicit request =>
    log.trace("Accepted languages: {}", request.acceptLanguages)
    request.session.get(SessionManager.SESSION_ID_KEY) match {
      case Some(_) => Redirect(routes.EntryPoints.view().absoluteURL())
      case None => Ok(views.html.login())
    }
  }
  def view = ensureSession(implicit request => Ok(views.html.view()))
  def report(reportName: String) = ensureSession(implicit request => Ok(views.html.report(reportName)))
  def dashboard = ensureSession(implicit request => Ok(views.html.dashboard()))
  def adminTrackers = ensureSession(implicit request => Ok(views.html.adminTrackers()))

  private def ensureSession(result: Request[AnyContent] => SimpleResult) = Action.async { implicit request =>
    log.trace("Accepted languages: {}", request.acceptLanguages)
    request.session.get(SessionManager.SESSION_ID_KEY) match {
    case Some(sessionId) => {
      (SessionManager() ? ConnectSession(sessionId, request.remoteAddress))
        .map(_ => result(request))
        .recover({
          case _ => Redirect(routes.EntryPoints.login().absoluteURL()).withSession(request.session - SessionManager.SESSION_ID_KEY)
      })
    }
    case None => Future.successful(Redirect(routes.EntryPoints.login().absoluteURL()))
  } }
}
