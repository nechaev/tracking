package controllers

import akka.pattern.ask
import framework.Defaults.timeout
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import tracking.web.{TrackingServerManager, AdminDao}
import tracking.web.TrackingJsonFormats._
import tracking._
import java.text.ParseException
import com.google.zxing.qrcode.QRCodeWriter
import com.google.zxing.common.BitMatrix
import com.google.zxing.BarcodeFormat
import com.google.zxing.client.j2se.MatrixToImageWriter
import play.api.libs.json.JsString
import play.api.mvc.SimpleResult
import play.api.libs.iteratee.Enumerator
import org.slf4j.LoggerFactory
import tracking.Tracker
import play.api.libs.json.JsString
import scala.Some
import play.api.mvc.SimpleResult
import play.api.mvc.ResponseHeader
import tracking.TrackerId

/**
 * User: vnechaev
 * Date: 08.10.13
 */
object TrackerAdminController extends Controller {

  import AdminDao._

  private val log = LoggerFactory.getLogger(this.getClass)

  def getTrackers = Action.async { implicit request => WithUserSession { session =>
    (session ? GetTrackers).mapTo[List[Tracker]].map(trackers =>
      Ok(Json.toJson(trackers)).withHeaders("Content-Range" -> s"items=0-${trackers.length}/${trackers.length}")
    )
  } }

  def getTracker(id: String) = Action.async { implicit request => WithUserSession { session =>
    parseTrackerId(id) { trackerId => (session ? GetTracker(trackerId)).mapTo[Option[Tracker]].map({
      case Some(tracker) => Ok(Json.toJson(tracker))
      case None => NotFound
    })
  } } }

  def create = Action.async(parse.json) { implicit request => WithUserSession { session =>
    implicit val reads = (
      (__ \ "name").read[String] ~
      (__ \ "trackerType").readNullable[String]
    ).tupled
    request.body.validate[(String, Option[String])].map({
      case (name, trackerType) => (session ? Create(name, trackerType)).mapTo[Tracker].map(tracker => Ok(Json.toJson(tracker)))
    }).recoverTotal(err => Future.successful(BadRequest("Cannot parse message: " + err.toString)))
  } }
  def update(id:String) = Action.async(parse.json) { implicit request => WithUserSession { session =>
    request.body.validate[Tracker].map({ tracker =>
      (session ? Update(tracker)).mapTo[Tracker].map(tracker => Ok(Json.toJson(tracker)))

    }).recoverTotal(err => Future.successful(BadRequest("Cannot parse message: " + err.toString)))
  } }
  /* //TODO: implement tracker deletion
  def delete(id: String) = Action { request =>
    JsString(id).validate[TrackerId].map({ trackerId =>
      AdminDao() ! Delete(trackerId)
      Ok
    }).recoverTotal(err => BadRequest("Cannot parse message: " + err.toString))
  }
  */
  def getKey(id: String) = Action.async { implicit request => WithUserSession { session =>
    parseTrackerId(id) { trackerId =>
      (session ? GetKey(trackerId)).mapTo[String]
        .map(key => trackerSettings(key))
        .recover {
          case _:IllegalArgumentException => NotFound(s"Tracker $id not found")
        }
    }
  } }
  def newKey(id: String) = Action.async { implicit request => WithUserSession { session =>
    parseTrackerId(id) { trackerId =>
      (session ? NewKey(trackerId))
        .map(_ => Ok)
        .recover {
          case _:IllegalArgumentException => NotFound(s"Tracker $id not found")
        }
    }
  } }

  private def trackerSettings(key: String)(implicit request: Request[_]) = {
    SimpleResult(
      header = ResponseHeader(200, Map(CONTENT_TYPE -> "image/gif")),
      body = Enumerator.outputStream(stream => {
        try {
          val codeWriter = new QRCodeWriter()
          val bitMatrix = codeWriter.encode(Json.stringify(
            Json.obj(
              "key" -> JsString(key),
              "url" -> JsString(routes.TrackingServerListener.postNewData().absoluteURL())
            )
          ), BarcodeFormat.QR_CODE, 300, 300)
          MatrixToImageWriter.writeToStream(bitMatrix, "gif", stream)
        } catch {
          case e: Throwable => log.error("Cannot generate QR code", e)
        } finally {
          stream.close()
        }
      })
    )
  }

  private def parseTrackerId(id: String)(block: TrackerId => Future[SimpleResult]): Future[SimpleResult] = {
    try {
      block(TrackingServerManager.parseTrackerId(id))
    } catch {
      case _:ParseException => Future.successful(BadRequest("Cannot parse tracker ID: " + id))
    }
  }

  def getGroups =  Action.async { implicit request => WithUserSession { session =>
    (session ? GetGroups).mapTo[List[TrackerGroup]].map(groups =>
      Ok(Json.toJson(groups)).withHeaders("Content-Range" -> s"items=0-${groups.length}/${groups.length}")
    )
  } }
  def getGroup(id: String) = Action.async { implicit request => WithUserSession { session =>
    (session ? GetGroup(id)).mapTo[Option[TrackerGroup]].map({
      case Some(group) => Ok(Json.toJson(group))
      case None => NotFound
    })
  } }
  def createGroup = Action.async(parse.json) { implicit request => WithUserSession { session =>
    implicit val reads = (
      (__ \ "name").read[String] ~
      (__ \ "trackers").read[List[String]]
    ).tupled
    request.body.validate[(String, List[String])].map({
      case (name, trackers) => (session ? CreateGroup(name)).mapTo[TrackerGroup].map(group => Ok(Json.toJson(group)))
    }).recoverTotal(err => Future.successful(BadRequest("Cannot parse message: " + err.toString)))
  } }
  def updateGroup(id:String) = Action.async(parse.json) { implicit request => WithUserSession { session =>
    request.body.validate[TrackerGroup].map({ group =>
      (session ? UpdateGroup(group)).mapTo[TrackerGroup].map(group => Ok(Json.toJson(group)))
    }).recoverTotal(err => Future.successful(BadRequest("Cannot parse message: " + err.toString)))
  } }
  def addTrackerToGroup(group: String, tracker:String) = Action.async { implicit request => WithUserSession { session =>
    (session ? AddTrackerToGroup(group, tracker)).map(_ => Ok)
  } }
  def removeTrackerFromGroup(group: String, tracker:String) = Action.async { implicit request => WithUserSession { session =>
    (session ? RemoveTrackerFromGroup(group, tracker)).map(_ => Ok)
  } }
}
