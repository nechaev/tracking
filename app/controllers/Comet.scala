package controllers

import akka.pattern.{AskTimeoutException, ask}
import concurrent.Future
import framework.Defaults.timeout
import play.api.Play.current
import play.api.libs.concurrent.Akka
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.iteratee.{Iteratee, Concurrent, Input}
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.duration._
import scala.language.postfixOps
import session.{SessionNotFoundException, NextMessage, Disconnect, SessionManager}
import org.slf4j.LoggerFactory

object Comet extends Controller { //with Logging {
  private lazy val logger = LoggerFactory.getLogger(getClass)

  import SessionManager._

  def websocketPing = WebSocket.using[String] { request => (
    Iteratee.ignore[String],
    Concurrent.unicast[String](onStart = { channel => channel.push("OK") })
  ) }

  private def buildCometConnection(request: RequestHeader) =
    request.session.get(SessionManager.SESSION_ID_KEY) match {
      case Some(sessionId) => SessionManager() ? ConnectSession(sessionId, request.remoteAddress) flatMap {
        case Connected(_, session) => (session ? StartConnection).mapTo[ConnectionStarted]
      }
      case None => Future.failed(SessionNotFoundException(""))
    }

  def startConnection = Action.async { request =>
    buildCometConnection(request) map {
      case ConnectionStarted(_, _, connection) => Ok(connection.path.name)
    } recover {
      case SessionNotFoundException(sessionId) => NotFound(s"Session $sessionId not found, relogin required")
    }
  }

  def websocket = WebSocket.async[JsValue] { request =>
    getCometConnection(request) map {
      case ConnectionStarted(input, output, _) => (input, output)
    }
  }

  def longpollStreamGet = Action.async { request =>
    getCometConnection(request) map {
      case ConnectionStarted(_, output, connection) => {
        Akka.system.scheduler.scheduleOnce(30 seconds, connection, Disconnect)
        Ok.chunked(output)
      }
    } recover {
      case _: Throwable => BadRequest
    }
  }
  def longpollGet = Action.async { request =>
    getCometConnection(request) flatMap {
      case ConnectionStarted(_, _, connection) => {
        (connection ? NextMessage).mapTo[List[JsValue]].map(msgs =>
          Ok(msgs.foldLeft("")((text, msg) => msg.toString() + text)))
      } recover {
        case _:AskTimeoutException => Ok
      }
    } recover {
      case _: Throwable => BadRequest
    }
  }

  def longpollPost = Action.async(parse.json) { request =>
      getCometConnection(request) map {
        case ConnectionStarted(inputIteratee, _, _) => {
          inputIteratee.feed(Input.El(request.body))
          Ok
        }
      } recover {
        case _: Throwable => BadRequest
      }
  }

  private def getCometConnection(request: RequestHeader) = {
    (for {
      sessionId <- request.session.get(SESSION_ID_KEY)
      connectionId <- request.getQueryString(CONNECTION_ID_KEY)
    } yield {
      SessionManager() ? ConnectSession(sessionId, request.remoteAddress) flatMap {
        case Connected(_, session) => (session ? FindConnection(connectionId)).mapTo[ConnectionStarted]
      }
    }) getOrElse {
      logger.error("Comet connection request error")
      Future.failed(new RuntimeException("Cannot get connection"))
    }
  }

}