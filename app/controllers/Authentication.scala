package controllers

import akka.pattern.ask
import framework.Defaults.timeout
import framework.JsonFormats._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.mvc._
import session.SessionManager
import user.{UserId, UserManager, UserMessage, Login}
import scala.concurrent.Future
import play.api.libs.openid.OpenID

/**
 * Created by: vnechaev
 * Date: 21.01.13
 */
object Authentication extends Controller {

  import SessionManager._
  private val openIdProviders = Map(
    "google" -> "https://www.google.com/accounts/o8/id"
  )

  def passwordLogin = Action.async(parse.json) { request =>
    implicit val authRequestReads = (
      (__ \ "login").read[UserId] and
      (__ \ "password").read[String]
    ).tupled
    request.body.validate[(UserId, String)].map({
      case (userId, password) => authenticateAndOpenSession(userId, password, request.remoteAddress)
    }).recoverTotal {
      e => Future.successful(BadRequest("Error " + JsError.toFlatJson(e)))
    }
  }

  def openIdLogin = Action.async(parse.json) { implicit request =>
    implicit val authRequestReads = (
      (__ \ "provider").read[String] and
      (__ \ "relocateTo").read[String]
    ).tupled
    request.body.validate[(String, String)].map({
      case (providerName, relocateTo) => {
        OpenID.redirectURL(openIdProviders(providerName), routes.Authentication.openIdCallback.absoluteURL(), Seq(
          "email" -> "http://schema.openid.net/contact/email",
          "firstname" -> "http://axschema.org/namePerson/first",
          "lastname" -> "http://axschema.org/namePerson/last",
          "country" -> "http://axschema.org/contact/country/home",
          "language" -> "http://axschema.org/pref/language"
        )).map(url => Ok(JsString(url))).recover {
          case _ => BadRequest(s"Cannot determine OpenID request url for $providerName")
        }
      }
    }).recoverTotal {
      e => Future.successful(BadRequest("Error " + JsError.toFlatJson(e)))
    }
  }

  def openIdCallback = Action.async { implicit request =>
    OpenID.verifiedId.flatMap({ info =>
      UserManager.parseOpenIdInfo(info).flatMap(userId =>
        (SessionManager() ? UserMessage(userId, Connect(request.remoteAddress))).map({
          case Connected(sessionId, _) => Redirect(routes.EntryPoints.login().absoluteURL()).withSession(SESSION_ID_KEY -> sessionId)
          case _ => InternalServerError
        }).recover({
          case _ => Unauthorized("Authentication error")
        })
      )
    }).recover({
      case _ => InternalServerError
    })
  }

  private def authenticateAndOpenSession(userId: UserId, password: String, address: String) = (UserManager() ? UserMessage(userId, Login(password))).mapTo[Boolean]
    .flatMap(authenticated => if (authenticated) {
    (SessionManager() ? UserMessage(userId, Connect(address))).map({
      case Connected(sessionId, _) => Ok(JsString(sessionId)).withSession(SESSION_ID_KEY -> sessionId)
      case _ => InternalServerError
    }).recover({
      case _ => Unauthorized("Authentication error")
    })
  } else Future.successful(Unauthorized("Not authenticated")))

  def logout = Action { implicit request =>
    request.session.get(SESSION_ID_KEY).map(SessionManager() ? ConnectSession(_, request.remoteAddress) map {
      case Connected(_, session) => session ! Logout
    })
    Redirect(routes.EntryPoints.login().absoluteURL())
  }

}
