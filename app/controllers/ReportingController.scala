package controllers

import akka.pattern.ask
import framework.Defaults.timeout
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.iteratee.Enumerator
import play.api.libs.json.Json
import play.api.mvc._
import reporting._
import scala.concurrent.Future
import tracking.web.{TrackerDescriptionRequest, TrackingServerManager, TrackingUserContextRequest, TrackingUserContext}
import tracking.web.TrackingJsonFormats._
import tracking.{TrackerGroup, Tracker}
import akka.actor.ActorRef

/**
 * Created by: sgl
 * Date: 21.03.13
 */
object ReportingController extends Controller {
  private val contentTypes = Map(
    "embeddable" -> "text/plain",
    "html" -> "text/html",
    "pdf" -> "application/pdf",
    "xls" -> "vnd.ms-excel",
    "xlsx" -> "vnd.ms-excel"
  )
  def generate(reportRequest: String) = Action.async { implicit request =>
    Json.parse(reportRequest).validate[TrackingReportRequest] map { reportRequest => WithUserSession { session =>
      (session ? ReportingContextRequest).mapTo[ReportingContext]
        .flatMap(reportingContext => (ReportManager() ? (reportRequest, reportingContext)).mapTo[Enumerator[Array[Byte]]])
        .map(enumerator => SimpleResult(
          header = ResponseHeader(200, Map(
            CONTENT_TYPE -> contentTypes(reportRequest.format),
            CONTENT_DISPOSITION -> s"attachment; filename=${"\""}${reportRequest.name}.${reportRequest.format}${"\""}"
            )),
            body = enumerator
          ))
        }
      } recoverTotal(_ => Future.successful(BadRequest))
  }
  def reportContext = Action.async { implicit request => WithUserSession { session: ActorRef => for {
    ctx <- (session ? TrackingUserContextRequest).mapTo[TrackingUserContext]
    trackerIds = ctx.groups.flatMap(group => group.trackers).toSet
    trackers <- Future.sequence(trackerIds.map(trackerId =>
      (TrackingServerManager() ? TrackerDescriptionRequest(trackerId)).mapTo[Tracker]
    ))
  } yield {
      Ok(Json.toJson(trackers.toList))
    }
  } }

}
