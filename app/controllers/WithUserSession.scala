package controllers

import akka.actor.ActorRef
import akka.pattern.ask
import framework.Defaults.timeout
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc.{SimpleResult, RequestHeader}
import scala.concurrent.Future
import session.SessionManager
import session.SessionManager.{Connected, ConnectSession}

/**
 * User: vnechaev
 * Date: 05.07.13
 */
object WithUserSession {
  def apply(block: ActorRef => Future[SimpleResult])(implicit request: RequestHeader): Future[SimpleResult] =
    findSession(request) flatMap block

  private def findSession(request: RequestHeader): Future[ActorRef] = request.session.get(SessionManager.SESSION_ID_KEY) match {
    case Some(sessionKey) => SessionManager() ? ConnectSession(sessionKey, request.remoteAddress) map {
      case Connected(_, session) => session
    }
    case _ => Future.failed(new IllegalAccessException())
  }
}
