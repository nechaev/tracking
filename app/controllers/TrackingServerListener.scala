package controllers

import org.joda.time.Instant
import org.slf4j.LoggerFactory
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.mvc.{Action, Controller}
import tracking.server._
import tracking.server.TrackerDataJsonFormats._

/**
 * Author: sgl
 * Date: 21.07.13
 */
object TrackingServerListener extends Controller {
  private val log = LoggerFactory.getLogger(this.getClass)

  def postNewData = Action(parse.json) { request =>
    log.info("postNewData request")
    implicit val packetReads = (
      (__ \ "k").read[String] ~
      (__ \ "e").read[List[TrackerEventData]]
    ).tupled
    (request.body match {
      case eventArray: JsArray => Json.obj("k" -> eventArray.value.head \ "k", "e" -> eventArray)
      case v => v
    }).validate[(String, List[TrackerEventData])].map({
      case (key, events) => {
        TrackerServer() ! TrackerDataPacket(key, events, Instant.now())
        Ok
      }
    }).recoverTotal({ err =>
      log.error("Cannot parse message: {}", request.body)
      BadRequest("Unknown message format")
    })
  }

}