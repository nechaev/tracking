package controllers

import akka.pattern.ask
import framework.Defaults.timeout
import framework.JsonFormats._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import tracking.web.{DashboardEntry, DashboardRequest}
import tracking.web.TrackingJsonFormats._
import org.slf4j.LoggerFactory

/**
 * User: vnechaev
 * Date: 04.07.13
 */
object DashboardController extends Controller {
  private val log = LoggerFactory.getLogger(this.getClass)
  private implicit val dashboardEntryWrites = Json.writes[DashboardEntry]

  def dashboardInfo = Action.async { implicit request => WithUserSession { session =>
    (session ? DashboardRequest).mapTo[List[DashboardEntry]].map(dashboardInfo => {
      Ok(Json.toJson(dashboardInfo)).withHeaders("Content-Range" -> s"items=0-${dashboardInfo.length}/${dashboardInfo.length}")
    }).recover {
      case e: Throwable => {
        log.error("Request error", e)
        InternalServerError
      }
    }
  } }

}
