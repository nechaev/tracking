package user

import akka.actor.ActorRef
import akka.event.{SubchannelClassification, LookupClassification, ActorEventBus}
import akka.util.Subclassification

/**
 * Created by: sgl
 * Date: 27.01.13
 */
object UserEventBus extends ActorEventBus with SubchannelClassification {
  type Classifier = String
  type Event = UserMessage

  protected val subclassification = new Subclassification[String] {
    def isEqual(x: String, y: String): Boolean = x == y
    def isSubclass(x: String, y: String): Boolean = x == y || (x.split("@") match {
      case Array(userUd, domain) => domain == y
      case _ => false
    })
  }
  protected def classify(event: UserMessage): String = event.userId.toString
  protected def publish(event: UserMessage, subscriber: ActorRef) = subscriber ! event

}
