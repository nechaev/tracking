package user

import akka.actor.ActorSelection
import akka.pattern.ask
import framework.DomainRouter
import framework.Defaults.timeout
import java.text.ParseException
import play.api.libs.openid.UserInfo
import play.api.Play.current
import scala.concurrent.Future
import scala.language.existentials
import play.api.Play

/**
 * Created by: vnechaev
 * Date: 22.01.13
 * Operations:
 *
 */

object UserManager extends DomainRouter {
  type Classifier = String
  type RoutableMessage = UserMessage
  def classify(message: UserMessage): String = message.userId.domain

  var openIdDomain: Option[ActorSelection] = None
  lazy val defaultDomain: Option[String] = Play.configuration.getString("userManager.defaultDomain")

  def parseUserId(userIdString: String) = userIdString split "@" match {
    case Array(userId, domainId) => UserId(userId, domainId)
    case Array(userId) if defaultDomain != None => UserId(userId, defaultDomain.get)
    case _ => throw new ParseException("User ID must be in form <id>@<domain>", 0)
  }
  def parseUserIdOpt(userIdString: String) = try {
    Some(parseUserId(userIdString))
  } catch {
    case _:ParseException => None
  }

  def parseOpenIdInfo(userInfo: UserInfo): Future[UserId] = openIdDomain match {
    case Some(openIdHandler) => (openIdHandler ? userInfo).mapTo[UserId]
    case None => Future.failed(new RuntimeException("OpenID domain manager not found"))
  }

}
