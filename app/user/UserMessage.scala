package user

/**
 * User: vnechaev
 * Date: 03.09.13
 */
case class UserMessage(
  userId: UserId,
  message: Any
)
