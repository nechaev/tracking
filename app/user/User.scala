package user

/**
 * Created by: sgl
 * Date: 13.01.13
 */

case class User (
  userId: UserId,
  name: String,
  email: Option[String],
  country: Option[String],
  language: Option[String]
)

case class UserId(userId: String, domain: String) {
  override def toString = userId + "@" + domain
}