package gis

/**
 * Author: sgl
 * Date: 25.06.13
 */
case class ReverseGeocodingRequest(points: List[Coordinates])
