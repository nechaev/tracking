package gis

import play.api.Configuration
import scala.concurrent.Future

/**
 * Author: sgl
 * Date: 01.07.13
 */
class MockGeocodingProvider(conf: Configuration) extends GeocodingProvider(conf) {
  def reverseGeocode(requestCoords: List[Coordinates]): Future[List[String]] =
    Future.successful(requestCoords map (_ => "Somewhere in the Universe :)"))
}
