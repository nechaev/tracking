package gis

import akka.actor.Actor
import akka.pattern.pipe
import scala.concurrent.Future
import org.joda.time.Instant
import tracking.{TrackerId, AddressState, TrackingEvent, PositionState}
import play.api.Configuration
import play.api.libs.concurrent.Akka
import akka.event.Logging

/**
 * Created by: sgl
 * Date: 15.09.13
 */
abstract class GeocodingProvider(conf: Configuration) extends Actor {
  private val log = Logging(context.system, this)

  def reverseGeocode(requestCoords: List[Coordinates]): Future[List[String]]

  import context.dispatcher

  val serviceName = conf.getString("serviceName").getOrElse("Service name must be defined")

  override def preStart() {
    GeocodingServiceManager.route(serviceName, self)
  }

  def receive = {
    case ReverseGeocodingRequest(points) => reverseGeocode(points) pipeTo sender
    case GenerateAddressEvents(events) => {
      val positions = events.foldLeft(List.empty[(Instant, TrackerId, PositionState)])((acc, event) => event match {
        case TrackingEvent(timestamp, trackerId, positionState: PositionState) => (timestamp, trackerId, positionState) :: acc
        case _ => acc
      })
      log.debug("Requested address events for {}", positions)
      (for {
        addresses <- reverseGeocode(positions map {
          case (_, _, positionState) => positionState.coordinates
        })
      } yield (addresses zip positions).map({
          case (address, (timestamp, trackerId, _)) => TrackingEvent(timestamp, trackerId, AddressState(address))
        })
      ) pipeTo sender
    }
  }
}
