package gis

case class Coordinates(latitude: Double, longitude: Double)
