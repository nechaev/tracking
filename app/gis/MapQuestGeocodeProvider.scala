package gis

import akka.event.Logging
import play.api.Configuration
import play.api.libs.json.{JsValue, JsString, JsArray}
import play.api.libs.ws.WS
import scala.concurrent.Future

/**
 * Author: sgl
 * Date: 25.06.13
 */
class MapQuestGeocodeProvider(conf: Configuration) extends GeocodingProvider(conf) {
  private val log = Logging(context.system, this)
  //val maxRequestSize = 100

  import context.dispatcher

  val key = conf.getString("apiKey").getOrElse(throw new IllegalArgumentException("MapQuest API key must be defined"))
  def url(method: String) = s"http://open.mapquestapi.com/geocoding/v1/$method?key=$key"

  def stringValue(v: JsValue) = v match {
    case JsString(s) => s
    case _ => ""
  }

  def reverseGeocode(requestCoords: List[Coordinates]): Future[List[String]] = {
    log.debug("Reverse geocoding: {}, calling {}", requestCoords, url("reverse"))
    Future.traverse(requestCoords)(coord =>
      WS.url(url("reverse")).post(Map("location" -> List(s"${coord.latitude},${coord.longitude}"), "thumbMaps" -> List("false")))
        .map(response => {
        log.debug("Received response: {}", response.json)
        (for {
          locations <- (response.json \\ "locations").headOption
          location <- locations match {
            case arr: JsArray => arr.value.headOption
            case _ => None
          }
        } yield stringValue(location \ "street") + ", " + stringValue(location \ "adminArea5")).getOrElse("")
    }).recover({
        case _ => ""
      }))
  }

}
