package gis

import scala.math._

/**
 * Created by: sgl
 * Date: 04.01.13
 */
object GisFunctions {
  private val earthRadius = 6371000; //meters

  @inline private def square(x: Double) = x * x

  def greatCircleDistance(p1: Coordinates, p2: Coordinates) = earthRadius * 2 * asin(sqrt(
    square(sin(toRadians(p2.latitude - p1.latitude)/2)) + square(sin(toRadians(p2.longitude - p1.longitude)/2)) *
    cos(toRadians(p1.latitude)) * cos(toRadians(p2.latitude))
  ))

  def fccDistanceParams(phi: Double) = (
    111132.09 - 566.05 * cos(2 * phi) + 1.20 * cos(4 * phi),
    111415.13 * cos(phi) - 94.55 * cos(3 * phi) + 0.12 * cos(5 * phi)
    )

  def fccDistanceSquared(p1: Coordinates, p2: Coordinates, params: (Double,Double)) =
    square(params._1 * (p2.latitude - p1.latitude)) + square(params._2 * (p2.longitude - p1.longitude))

  def fccDistance(p1: Coordinates, p2: Coordinates) = sqrt(fccDistanceSquared(p1, p2,
    fccDistanceParams(toRadians(p1.latitude + p2.latitude)/2)))

  def bearing(p1: Coordinates, p2: Coordinates) = {
    val bearing = toDegrees(atan2(
      (p2.longitude - p1.longitude) * cos(toRadians(p1.latitude)),
      p2.latitude - p1.latitude
    ))
    if (bearing < 0) 360 + bearing else bearing
  }
}
