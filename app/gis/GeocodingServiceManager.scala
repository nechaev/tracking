package gis

import framework.DomainRouter
import session.SessionForwardable
import user.UserManager

/**
 * Author: sgl
 * Date: 26.06.13
 */
object GeocodingServiceManager extends DomainRouter {
  type Classifier = String
  type RoutableMessage = GeocodingRequest
  def classify(msg: GeocodingRequest): String = msg.service
}

case class GeocodingRequest(service: String, request: Any)

case object GeocodingServiceRequest extends SessionForwardable {
  def forwardTo = UserManager()
}