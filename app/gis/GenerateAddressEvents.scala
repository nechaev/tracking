package gis

import tracking.TrackingEvent

/**
 * Author: sgl
 * Date: 08.09.13
 */
case class GenerateAddressEvents(events: List[TrackingEvent[_]])
