define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/dom-construct',
    './userGroupsStore',
    './userTrackersStore',
    'dgrid/OnDemandList',
    'dgrid/Selection',
    'tools/loadStyle',
    'dojo/text!./AdminGroupsGrid.css'
], function (
    declare,
    lang,
    domConstruct,
    userGroupsStore,
    userTrackersStore,
    List,
    Selection,
    loadStyle,
    css
    ) {
    loadStyle(css);
    var GroupsGrid = declare([List, Selection], {
        store: userGroupsStore,
        renderRow: function(item, options) {
            var rowDom = domConstruct.toDom('<div class="group-grid-row">' +
                '<div class="group-grid-group-item">' + item.name + '</div>' +
            '</div>');
            item.trackers.forEach(function(trackerId) {
                userTrackersStore.get(trackerId).then(function(tracker) {
                    domConstruct.place('<div class="group-grid-tracker-item">' + tracker.name + '</div>', rowDom);
                });
            });
            return rowDom;
        },
        selectionMode: 'single',
        postCreate: function() {
            this.inherited(arguments);
            GroupsGrid.instance = this;
        }
    });
    GroupsGrid.getSelectedGroup = function() {
        var sel = this.instance.selection;
        for (var id in sel) if (sel.hasOwnProperty(id) && sel[id]) return this.instance.row(id).data;
        return undefined;
    };
    return GroupsGrid;
});
