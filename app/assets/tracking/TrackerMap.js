define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/Evented',
    'gis/Map',
    './TrackerDisplayPlugin',
    './TrackDisplayPlugin'
], function (
    declare,
    lang,
    Evented,
    Map,
    TrackerDisplayPlugin,
    TrackDisplayPlugin
    ) {
    var TrackerMap = declare(Map, {
        constructor: function() {
            var trackerDisplayPlugin = this.trackerDisplayPlugin = new TrackerDisplayPlugin();
            this.plugins = [trackerDisplayPlugin, new TrackDisplayPlugin()];
            this.inherited(arguments);
            var followHandle = TrackerMap.on('FollowTracker', function (tracker) {
                trackerDisplayPlugin.startFollow(tracker);
            });
            var unfollowHandle = TrackerMap.on('UnfollowTracker', function() {
                trackerDisplayPlugin.stopFollow();
            });
            this.destroy = function() {
                followHandle.remove();
                unfollowHandle.remove();
                this.inherited(arguments);
            }
        },
        buildRendering: function() {
            this.inherited(arguments);
            var trackerDisplayPlugin = this.trackerDisplayPlugin;
            this.map.on('dragstart', function () {
                trackerDisplayPlugin.stopFollow();
            });
        },
        settingsKey: 'trackers'
    });
    lang.mixin(TrackerMap, Evented.prototype);
    return TrackerMap;
});
