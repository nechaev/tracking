define([
    'types/Map',
    'types/Float',
    'types/String',
    'gis/Coordinates'
], function(
    MapType,
    FloatType,
    StringType,
    CoordsType
    ) {
    return MapType({
        coordinates: CoordsType,
        altitude: FloatType,
        accuracy: FloatType,
        speed: FloatType,
        heading: FloatType
    })
});
