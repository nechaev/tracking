define([
    'dojo/_base/declare',
    'dijit/Dialog',
    'dojo/i18n!./nls/resources',
    './TrackerEditForm',
    './userTrackersStore'
], function(
    declare,
    Dialog,
    nls,
    TrackerEditForm,
    trackerStore
) {

    var AdminTrackerDialog = declare(Dialog, {
        nls: nls,
        title: 'Tracker properties',
        postCreate: function() {
            this.inherited(arguments);
            var form = new TrackerEditForm();
            var dialog = this;
            form.on('cancel', function() {
                dialog.hide();
            });
            form.on('save', function() {
                var tracker = form.getTracker();
                if (tracker.id) trackerStore.put(tracker);
                else trackerStore.add(tracker);
                dialog.hide();
            });
            this.setTracker = function(tracker) {
                form.setTracker(tracker);
            };
            this.set('content', form);
        }
    });
    AdminTrackerDialog.show = function(tracker) {
        if (!this.instance) this.instance = new AdminTrackerDialog();
        this.instance.setTracker(tracker);
        this.instance.show();
    };
    AdminTrackerDialog.hide = function() {
        this.instance.hide();
    };
    return AdminTrackerDialog;
});
