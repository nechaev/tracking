define([
    'dojo/_base/declare',
    'dijit/form/Button',
    'dojo/i18n!./nls/resources'
], function(
    declare,
    Button,
    nls
    ) {
    return declare(Button, {
        label: nls.trackerListTitle,
        onClick: function() {
            //controller.showTrackerList();
        }
    });
});
