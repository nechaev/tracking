define(['types/Map', 'types/Boolean'], function(MapType, BoolType) {
    return MapType({
        isGpsConnected: BoolType,
        isGsmConnected: BoolType
    })
});

