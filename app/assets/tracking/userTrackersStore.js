define([
    'dojo/store/Observable',
    'dojo/store/JsonRest'
], function(
    Observable,
    JsonRest
) {
    return Observable(new JsonRest({
        target: '/rest/trackerAdmin/userTrackers/',
        getIdentity: function(item) {
            return item.id ? encodeURIComponent(item.id) : undefined;
        }
    }));
});