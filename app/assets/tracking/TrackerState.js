define([
    'types/Map',
    'types/String',
    './PositionState',
    'types/Instant',
    './ConnectionEvent'
], function(
    MapType,
    StringType,
    PositionEventType,
    InstantType,
    ConnectionEventType
    ) {
    return MapType({
        trackerId: StringType,
        timestamp: InstantType,
        position: PositionEventType,
        connection: ConnectionEventType,
        address: MapType({address: StringType})
    });
});
