define([
    'dojo/Deferred',
    'comet/main'
], function(
    Deferred,
    comet
    ) {
    return comet.ask('tracking.user-context')
});
