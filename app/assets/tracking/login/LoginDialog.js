define([
    'dojo/_base/declare',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dojo/query',
    'dojo/on',
    'dojo/request/xhr',
    'dojo/json',
    'dojo/dom-construct',
    'dojo/dom-style',
    'dojo/text!./LoginDialog.html',
    'dojo/text!./LoginDialog.css',
    'tools/loadStyle',
    'dojo/i18n!tracking/nls/resources'
],
    function (
        declare,
        WidgetBase,
        TemplatedMixin,
        query,
        on,
        xhr,
        json,
        construct,
        domStyle,
        template,
        css,
        loadStyle,
        nls
        ) {
        loadStyle(css);
        return declare([WidgetBase, TemplatedMixin], {
            nls: nls,
            relocateTo: undefined,
            openIdEnabled: false,
            openIdButton: undefined,
            templateString: template,
            postCreate: function() {
                this.inherited(arguments);
                if (!this.openIdEnabled) query(".openid-button", this.domNode).forEach(function(node) {
                    domStyle.set(node, "display","none");
                });
            },
            buildRelocationUrl: function() {
                return location.protocol + '//' + location.host + '/' + this.relocateTo;
            },
            onLoginFailed: function() {
                alert(this.nls.loginFailedMessage);
                this.passwordField.value = '';
            },
            performLogin: function(loginMethod, loginData, callback) {
                xhr('/auth/' + loginMethod, {
                    handleAs: 'json',
                    method: 'POST',
                    data: json.stringify(loginData),
                    headers: {'Content-Type': 'application/json'}
                }).then(
                    callback,
                    this.onLoginFailed.bind(this)
                );
            },
            doLoginPassword: function() {
                var url = this.buildRelocationUrl();
                this.performLogin('passwordLogin', {
                    login: this.loginField.value,
                    password: this.passwordField.value
                }, function (data) {
                    location.assign(url);
                });
            },
            doLoginOpenId: function(provider) {
                this.performLogin('openIdLogin', {
                    provider: provider,
                    relocateTo: this.buildRelocationUrl()
                }, function(data) {
                    location.assign(data);
                });
            },
            doLoginWithGoogle: function() {
                this.doLoginOpenId('google');
            }
        })
    });
