define([
    'dojo/_base/declare',
    './ui/Workspace',
    './TrackerMap',
    './ui/StateIndicator',
    './TrackerList',
    './TrackerInfo',
    './TrackerView',
    './TrackDialog',
    './TrackDisplayPlugin',
    './TrackerDisplayPlugin',
    'dojo/i18n!./nls/resources'
], function (
    declare,
    Workspace,
    TrackerMap,
    StateIndicator,
    TrackerList,
    TrackerInfo,
    TrackerView,
    TrackDialog,
    TrackDisplayPlugin,
    TrackerDisplayPlugin,
    nls
    ) {

    function showTrackerView(tracker, clickEvent) {
        Workspace.openWindow({
            key: 'tracking.tracker-view-' + tracker.id,
            windowTitle: tracker.name,
            contentClass: TrackerView,
            contentProps: {
                tracker: tracker
            },
            draggable: true,
            initialStyle: {
                //width: '200px',
                //height: '200px'
            }
        }, clickEvent);
    }

    function showTrackerInfo(tracker, clickEvent) {
        Workspace.openWindow({
            key: 'tracking.tracker-info-' + tracker.id,
            windowTitle: tracker.name + ' - ' + nls.infoTitle,
            contentClass: TrackerInfo,
            contentProps: {
                tracker: tracker
            },
            draggable: true,
            initialStyle: {}
        }, clickEvent);
    }

    function showTrackDialog(tracker, clickEvent) {
        Workspace.openWindow({
            key: 'tracking.tracker-track-' + tracker.id,
            windowTitle: tracker.name + ' - ' + nls.eventLogTitle,
            contentClass: TrackDialog,
            contentProps: {
                tracker: tracker
            },
            draggable: true,
            initialStyle: {}
        }, clickEvent);
    }

    return declare(null, {
        startup: function () {
            TrackerList.on('TrackerClick', showTrackerView);
            TrackerDisplayPlugin.on('TrackerClick', showTrackerView);

            TrackerView.on('FollowClick', function(tracker) {
                TrackerMap.emit('FollowTracker', tracker);
            });
            TrackerView.on('TrackClick', showTrackDialog);
            TrackerView.on('InfoClick', showTrackerInfo);
            TrackDialog.on('ShowTrack', function(log) {
                TrackDisplayPlugin.emit('ShowTrack', log);
            });
            TrackDialog.on('ClearTrack', function() {
                TrackDisplayPlugin.emit('ClearTrack');
            });
        }
    });
});