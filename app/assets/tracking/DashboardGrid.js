define([
    'dojo/_base/declare',
    'dojo/ready',
    'dojo/request/xhr',
    'dojo/store/Memory',
    'dojo/store/Observable',
    'dgrid/OnDemandGrid',
    './formats',
    'dojo/i18n!./nls/resources'
], function (
    declare,
    ready,
    xhr,
    Memory,
    Observable,
    Grid,
    formats,
    nls
    ) {
    var dashboardStore = Observable(new Memory({data:[]}));
    return declare(Grid, {
        store: dashboardStore,
        columns: {
            name: nls.nameField,
            state: nls.stateField,
            address: nls.address,
            mileage: nls.mileageField,
            engineHours: nls.engineHoursField,
            gpsLostTime: nls.gpsLostTimeField,
            parkingTime: nls.parkingTimeField
        },
        postCreate: function () {
            this.inherited(arguments);
            var self = this;

            function update() {
                self.updateDashboard();
            }

            ready(update);
            setInterval(update, 60000);
        },
        updateDashboard: function () {
            xhr('/dashboard-info', {
                handleAs: 'json'
            }).then(function (data) {
                    data.map(function (entry) {
                        return {
                            id: entry.trackerId,
                            name: entry.name,
                            state: formats.positionState(entry.positionState),
                            address: entry.address,
                            mileage: entry.dailyMileage ? Math.round(entry.dailyMileage) : nls.notAvailable,
                            engineHours: entry.dailyEngineHours ? formats.duration(entry.dailyEngineHours) :  nls.notAvailable,
                            gpsLostTime: entry.dailyGpsLostTime ? formats.duration(entry.dailyGpsLostTime) :  nls.notAvailable,
                            parkingTime: entry.dailyParkingTime ? formats.duration(entry.dailyParkingTime) :  nls.notAvailable
                        };
                    }).forEach(function(entry) {
                            dashboardStore.put(entry);
                        });
                });
        }
    });
});
