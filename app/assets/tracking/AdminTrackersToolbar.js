define([
    'dojo/_base/declare',
    'dojo/request/xhr',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    './AdminTrackerDialog',
    './AdminTrackersGrid',
    './TrackerSettingsPopup',
    'dojo/i18n!./nls/resources',
    'dijit/form/Button'
], function(
    declare,
    xhr,
    WidgetBase,
    TemplatedMixin,
    WidgetsInTemplateMixin,
    TrackerDialog,
    TrackersGrid,
    TrackerSettings,
    nls
    ) {
    return declare([WidgetBase, TemplatedMixin, WidgetsInTemplateMixin], {
        nls: nls,
        templateString: '<div>' +
            '<button data-dojo-type="dijit/form/Button" data-dojo-attach-event="onClick:newTracker">${nls.newTrackerButton}</button>' +
            '<button data-dojo-type="dijit/form/Button" data-dojo-attach-event="onClick:editTracker">${nls.editTrackerButton}</button>' +
            '<button data-dojo-type="dijit/form/Button" data-dojo-attach-event="onClick:getKey">${nls.showSettingsQR}</button>' +
            '<button data-dojo-type="dijit/form/Button" data-dojo-attach-event="onClick:newKey">${nls.changeKeyAndShowQR}</button>' +
            '</div>',
        newTracker: function() {
            TrackerDialog.show();
        },
        editTracker: function() {
            var tracker = TrackersGrid.getSelectedTracker();
            if (tracker) TrackerDialog.show(tracker);
        },
        getKey: function() {
            var tracker = TrackersGrid.getSelectedTracker();
            if (tracker) TrackerSettings.show(tracker.id);
        },
        newKey: function() {
            var tracker = TrackersGrid.getSelectedTracker();
            if (tracker) {
                xhr('/rest/trackerAdmin/trackerKeyNew', {
                    query: {trackerId: tracker.id},
                    nocache: new Date().getTime().toString()
                }).then(function() {
                    TrackerSettings.show(tracker.id);
                })
            }
        }
    })
});