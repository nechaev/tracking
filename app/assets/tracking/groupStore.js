define([
    './CachedStore',
    './TrackingUserContext'
], function(
    CachedStore,
    TrackingUserContext
    ) {
    return new CachedStore({
        loadData: function() {
            return TrackingUserContext.map(function(context) {return context.groups;})
        }
    });
});
