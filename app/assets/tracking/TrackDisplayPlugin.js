define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/Evented',
    'gis/leaflet/leaflet',
    './formats',
    'tools/loadStyle',
    'dojo/text!./TrackDisplayPlugin.css'
], function(
    declare,
    lang,
    Evented,
    leaflet,
    formats,
    loadStyle,
    css
    ) {
    loadStyle(css);

    var minParkingDurationMs = 10000;

    var parkingIcon = new leaflet.DivIcon({
        html: '<div class="parking-icon-outer"><div class="parking-icon-inner"><div class="parking-icon-text">P</div></div></div>',
        iconSize: new leaflet.Point(16, 16),
        iconAnchor: new leaflet.Point(8, 8),
        className: 'tracking-icon-container'
    });
    var pointIcon = new leaflet.DivIcon({
        html: '',
        iconSize: new leaflet.Point(4, 4),
        iconAnchor: new leaflet.Point(2, 2),
        className: 'track-point-icon'
    });
    var TrackDisplayPlugin = declare(null, {
        mapInit: function(map) {
            var displayedPolyline = undefined;
            var trackerWatchHandle = undefined;
            var displayedMarkers = [];
            function clear() {
                if (displayedPolyline) {
                    map.removeLayer(displayedPolyline);
                    displayedPolyline = undefined;
                    displayedMarkers.forEach(function(marker) {
                        map.removeLayer(marker);
                    });
                    displayedMarkers = [];
                }
                if (trackerWatchHandle) {
                    trackerWatchHandle.unwatch();
                    trackerWatchHandle = undefined;
                }
            }
            function drawPoint(trackPoint) {
                var markerConfig;
                var pos = trackPoint.position.coordinates;
                if (trackPoint.position.speed == 0 && trackPoint.duration > minParkingDurationMs) markerConfig = {
                    icon: parkingIcon,
                    title: '' + formats.datetime(trackPoint.timestamp) + ', ' + formats.duration(trackPoint.duration)
                };
                else markerConfig = {
                    icon: pointIcon,
                    title: '' + formats.datetime(trackPoint.timestamp) +
                        (trackPoint.position.speed ? (',V=' + formats.speed(trackPoint.position)) : '') +
                        (trackPoint.position.altitude ? (',H=' + formats.altitude(trackPoint.position)) : '')
                };
                var marker = new leaflet.Marker([pos.latitude, pos.longitude], markerConfig);
                marker.addTo(map);
                displayedMarkers.push(marker);
            }
            var showLogHandle = TrackDisplayPlugin.on('ShowTrack', function(trackData) {
                clear();
                displayedPolyline = new leaflet.Polyline(trackData.trackInfo.map(function(trackPoint) {
                    var coords = trackPoint.position.coordinates;
                    return new leaflet.LatLng(coords.latitude, coords.longitude)
                }), {color: 'blue'}).addTo(map);
                if (trackData.now) trackerWatchHandle = trackData.tracker.watch('position', function(prop, oldVal, newVal) {
                    var coords = newVal.coordinates;
                    displayedPolyline.addLatLng([coords.latitude, coords.longitude]);
                    drawPoint({position: newVal, timestamp: trackData.tracker.timestamp});
                    //TODO: remove old points using log.duration
                });
                trackData.trackInfo.forEach(drawPoint);
            });
            var clearLogHandle = TrackDisplayPlugin.on('ClearTrack', clear);

            this.destroy = function() {
                if (trackerWatchHandle) trackerWatchHandle.remove();
                showLogHandle.remove();
                clearLogHandle.remove();
            }
        }
    });
    lang.mixin(TrackDisplayPlugin, Evented.prototype);
    return TrackDisplayPlugin;
});
