define(['types/Map','types/List','types/String'], function(MapType, ListType, StringType) {
    return MapType({
        'id': StringType,
        'name': StringType,
        'trackers': ListType(StringType)
    });
});
