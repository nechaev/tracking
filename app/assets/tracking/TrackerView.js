define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/Evented',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'gis/Map',
    './TrackerDisplayPlugin',
    './formats',
    'tools/loadStyle',
    'dojo/text!./TrackerView.html',
    'dojo/text!./TrackerView.css',
    'dojo/i18n!./nls/resources'
], function(
    declare,
    lang,
    Evented,
    WidgetBase,
    TemplatedMixin,
    Map,
    TrackerDisplayPlugin,
    formats,
    loadStyle,
    template,
    css,
    nls) {

    loadStyle(css);

    var TrackerInfoMap = declare(Map, {
        zoomPosition: 'topleft',
        constructor: function(params) {
            this.tracker = params.tracker;
            this.trackerDisplayPlugin = new TrackerDisplayPlugin();
            this.plugins = [this.trackerDisplayPlugin];
            this.inherited(arguments);
        },
        buildRendering: function() {
            this.inherited(arguments);
            this.trackerDisplayPlugin.startFollow(this.tracker);
            this.map.setZoom(15);
        }
    });

    var TrackerView = declare([WidgetBase, TemplatedMixin], {
        nls: nls,
        templateString: template,
        postCreate: function() {
            this.inherited(arguments);
            var map = new TrackerInfoMap({tracker: this.tracker}, this.mapNode);
            var self = this;
            var watchHandle = this.tracker.watch('position', function(property,oldValue,newValue) {
                self.updateTrackerStatus(newValue);
            });
            this.destroy = function() {
                watchHandle.unwatch();
            };
            this.updateTrackerStatus(this.tracker.position, this.tracker.address);
        },
        enableFollow: function(event) {
            TrackerView.emit("FollowClick", this.tracker, event);
        },
        showTrack: function(event) {
            TrackerView.emit("TrackClick", this.tracker, event);
        },
        showInfo: function(event) {
            TrackerView.emit("InfoClick", this.tracker, event);
        },
        updateTrackerStatus: function(position, address) {
            var stateStr = formats.positionState(position);
            if (address && address.address.length > 0) {
                stateStr = stateStr + '<br>' + address.address;
            }
            this.statusNode.innerHTML = stateStr;
        }
    });
    lang.mixin(TrackerView, Evented.prototype);
    return TrackerView;

});