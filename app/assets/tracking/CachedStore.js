define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/store/util/QueryResults',
    'dojo/store/util/SimpleQueryEngine',
    'tools/Deferred',
    'comet/main'
], function(
    declare,
    lang,
    QueryResults,
    SimpleQueryEngine,
    Deferred,
    comet
    ) {

    return declare(null, {
        idProperty: 'id',
        queryMessageType: undefined,
        elementType: undefined,
        queryEngine: SimpleQueryEngine,
        loadData: function() {
            return comet.ask(this.queryMessageType, this.elementType);
        },
        getIdentity: function(item) {
            return item[this.idProperty];
        },
        constructor: function(parameters) {
            lang.mixin(this, parameters);
            var dataDef = this.loadData();
            var cachedStore = this;
            var indexDef = dataDef.map(function(dataSet) {
                var index = {};
                dataSet.forEach(function(elem) {
                    index[elem[cachedStore.idProperty]] = elem;
                });
                return index;
            });
            this.query = function(query, options) {
                var queryEngine = this.queryEngine;
                return QueryResults(dataDef.map(queryEngine(query, options)));
            };

            this.get = function(id) {
                return indexDef.map(function(index) {
                    return index[id];
                });
            };
        }
    });
});
