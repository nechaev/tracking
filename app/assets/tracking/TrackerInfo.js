define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/Evented',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'tools/loadStyle',
    './formats',
    'dojo/text!./TrackerInfo.html',
    'dojo/text!./TrackerInfo.css',
    'dojo/i18n!./nls/resources'
], function (
    declare,
    lang,
    Evented,
    WidgetBase,
    TemplatedMixin,
    loadStyle,
    formats,
    template,
    css,
    nls
    ) {

    loadStyle(css);

    return declare([WidgetBase, TemplatedMixin], {
        nls: nls,
        templateString: template,
        speedNode:     undefined,
        latitudeNode:  undefined,
        longitudeNode: undefined,
        altitudeNode:  undefined,
        headingNode:   undefined,
        addressNode:   undefined,
        timestampNode: undefined,
        postCreate: function () {
            this.inherited(arguments);
            var trackerInfo = this;

            function updatePosition(position) {
                trackerInfo.speedNode.innerHTML =     formats.speed(position);
                trackerInfo.latitudeNode.innerHTML =  formats.latitude(position);
                trackerInfo.longitudeNode.innerHTML = formats.longitude(position);
                trackerInfo.altitudeNode.innerHTML =  formats.altitude(position);
                trackerInfo.headingNode.innerHTML =   formats.heading(position);
                var address = trackerInfo.tracker.address;
                trackerInfo.addressNode.innerHTML = address && address.address && address.address.length > 0 ? address.address : "---";
            }

//            function updateConnection(conn) {
//                trackerInfo.stateNode.innerHTML = formats.connection(conn);
//            }

            function updateTimestamp() {
                var ts = trackerInfo.tracker.timestamp;
                trackerInfo.timestampNode.innerHTML = ts ? formats.datetime(ts) : '---';
            }

            var watchHandle = this.tracker.watch(function (property, oldValue, newValue) {
                if (property == 'position') updatePosition(newValue);
                //else if (property == 'connection') updateConnection(newValue);
                updateTimestamp();
            });

            this.destroy = function () {
                watchHandle.unwatch();
            };

            updatePosition(this.tracker.position);
            //updateConnection(this.tracker.connection);
            updateTimestamp();

        }

    });
});
