define([
    'dojo/_base/declare',
    'dojo/Evented',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/i18n!./nls/resources',
    'dojo/text!./TrackerEditForm.html',
    'dijit/form/TextBox',
    'dijit/form/Select',
    'dijit/form/Button'
], function(
    declare,
    Evented,
    WidgetBase,
    TemplatedMixin,
    WidgetsInTemplateMixin,
    nls,
    template
    ) {
    return declare([WidgetBase, TemplatedMixin, WidgetsInTemplateMixin, Evented], {
        nls: nls,
        templateString: template,
        nameField: undefined,
        typeField: undefined,
        setTracker: function(tracker) {
            this.trackerId = tracker ? tracker.id : undefined;
            this.nameField.set('value', tracker ? tracker.name : '');
            this.typeField.set('value', tracker ? tracker.trackerType : 'null');
        },
        getTracker: function() {
            var ttype = this.typeField.get('value');
            return {
                id: this.trackerId,
                name: this.nameField.get('value'),
                trackerType: ttype == 'null' ? undefined : ttype
            }
        },
        saveClick: function() {
            this.emit('save');
        },
        cancelClick: function() {
            this.emit('cancel');
        }
    });
});
