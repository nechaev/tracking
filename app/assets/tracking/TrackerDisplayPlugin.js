define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/Evented',
    'gis/leaflet/leaflet',
    'gis/DirectionalMarker',
    './trackerStore'
], function (
    declare,
    lang,
    Evented,
    leaflet,
    DirectionalMarker,
    trackerStore
    ) {

    var markerIcon = new leaflet.DivIcon({
        html: '<img src="/assets/tracking/images/tracker-marker.png"/>',
        iconSize: new leaflet.Point(16, 16),
        iconAnchor: new leaflet.Point(8, 8),
        className: 'tracking-icon-container'
    });
    var specialIcons = {
        man: new leaflet.DivIcon({
            html: '<img src="/assets/tracking/images/trackers/man.png"/>',
            iconSize: [32, 32],
            iconAnchor: [16, 16],
            className: 'tracking-icon-container'
        }),
        car: new leaflet.DivIcon({
            html: '<img src="/assets/tracking/images/trackers/car.png"/>',
            iconSize: [32, 32],
            iconAnchor: [16, 16],
            className: 'tracking-icon-container'
        }),
        truck: new leaflet.DivIcon({
            html: '<img src="/assets/tracking/images/trackers/truck.png"/>',
            iconSize: [32, 32],
            iconAnchor: [16, 16],
            className: 'tracking-icon-container'
        }),
        container: new leaflet.DivIcon({
            html: '<img src="/assets/tracking/images/trackers/container.png"/>',
            iconSize: [32, 32],
            iconAnchor: [16, 16],
            className: 'tracking-icon-container'
        }),
        motorcycle: new leaflet.DivIcon({
            html: '<img src="/assets/tracking/images/trackers/motorcycle.png"/>',
            iconSize: [32, 32],
            iconAnchor: [16, 16],
            className: 'tracking-icon-container'
        })
    };
    var earthRadius = 6371000;
    var maxSimulationDelayMs = 60000;
    var predictionTimeMs = 10000;
    var simulationStepMs = 100;
    var degToRad = Math.PI / 180;
    var radToDeg = 180 / Math.PI;

    var TrackerDisplayPlugin = declare(null, {
        mapInit: function (map) {

            var modelMarkers = {};
            var followTracker = undefined;
            var metersPerPixel = 1;
            var suspendSimulation = false;

            function updateMetersPerPixel() {
                var mapBounds = map.getBounds();
                var dLat = mapBounds.getNorthEast().lat - mapBounds.getSouthWest().lat;
                var mapSize = map.getSize().y;
                metersPerPixel = earthRadius * dLat * degToRad / mapSize;
            }

            updateMetersPerPixel();

            function rawPan(newPosition) {
                map.fire('movestart');
                map._rawPanBy(map._getCenterOffset(newPosition));
                map.fire('move');
                map.fire('moveend');
            }

            function needSimulation(tracker) {
                // it must move at least by 1 pixel in 5 seconds
                return (new Date().getTime() - tracker.timestamp.getTime()) < maxSimulationDelayMs
                    && (tracker.position.speed / 3.6) * 5 > metersPerPixel;
            }

            function updateMarker(tracker) {
                if (!(tracker.position && tracker.position.coordinates)) return;
                var marker = modelMarkers[tracker.id];
                var pos = tracker.position.coordinates;
                if (pos && (map.getBounds().contains(new leaflet.LatLng(pos.latitude, pos.longitude))
                    || (marker !== undefined && map.getBounds().contains(marker.getLatLng()))
                    || tracker === followTracker)) {

                    if (marker === undefined) {
                        var icon = markerIcon;
                        var directional = true;
                        if (tracker.trackerType && tracker.trackerType != '' && specialIcons.hasOwnProperty(tracker.trackerType)) {
                            icon = specialIcons[tracker.trackerType];
                            directional = false;
                        }
                        modelMarkers[tracker.id] = marker = new DirectionalMarker([pos.latitude, pos.longitude], {
                            icon: icon,
                            title: tracker.name
                        });
                        marker.directional = directional;
                        lang.mixin(marker, {
                            simulation: false,
                            updatePosition: function () {
                                this.setDirection(tracker.position.heading);
                                if (this.simulation) {
                                    var curLatLng = this.getLatLng();
                                    this.simStartTime = new Date;
                                    this.simStartPoint = {
                                        latitude: curLatLng.lat,
                                        longitude: curLatLng.lng
                                    };
                                    this.simMotionVector = calculateMotionVector(this.simStartPoint, tracker, this.simTimeOffset);
                                } else {
                                    var pos = tracker.position.coordinates;
                                    this.setLatLng([pos.latitude, pos.longitude]);
                                }
                            },
                            startSimulation: function () {
                                var pos = tracker.position;
                                if (!this.simulation) {
                                    this.simTimeOffset = pos.timestamp.getTime() - pos.clientTimestamp.getTime();
                                    this.simStartTime = pos.clientTimestamp;
                                    this.simStartPoint = pos.coordinates;
                                    this.simMotionVector = calculateMotionVector(this.simStartPoint, tracker, this.simTimeOffset);
                                    this.simulation = true;
                                }
                            },
                            stopSimulation: function () {
                                if (this.simulation) {
                                    this.simulation = false;
                                }
                            }
                        });
                        marker.on('click', function (event) {
                            TrackerDisplayPlugin.emit('TrackerClick', tracker, event.originalEvent);
                        });
                        marker.addTo(map);
                        marker.setDirection(tracker.position.heading);
                        marker.showLabel(tracker.name);
                    } else {
                        marker.updatePosition();
                    }
                    if (needSimulation(tracker)) marker.startSimulation();
                    else marker.stopSimulation();
                    if (tracker === followTracker) map.panTo(marker.getLatLng());
                } else if (marker !== undefined) {
                    map.removeLayer(marker);
                    delete modelMarkers[tracker.id];
                }
            }

            trackerStore.query().then(function(trackers) {
                trackers.forEach(function(tracker) {
                    tracker.watch('position', function () {
                        updateMarker(tracker);
                    });
                    updateMarker(tracker);
                });
            });

            this.startFollow = function (tracker) {
                var marker = modelMarkers[tracker.id];
                if (marker === undefined || !marker.simulation) {
                    var coords = tracker.position.coordinates;
                    map.panTo([coords.latitude, coords.longitude]);
                }
                followTracker = tracker;
            };
            this.stopFollow = function () {
                followTracker = undefined;
            };
            map.on('movestart zoomstart', function () {
                suspendSimulation = true;
            });
            map.on('moveend zoomend', function () {
                updateMetersPerPixel();
                trackerStore.query().then(function(trackers) { trackers.forEach(function(tracker) {
                    updateMarker(tracker);
                })});
                suspendSimulation = false;
            });

            function calculateMotionVector(markerCoords, tracker, timeOffset) {
                var position = tracker.position;
                var v = position.speed / 3.6; // m/s
                var ds = (new Date().getTime() + predictionTimeMs - (position.timestamp.getTime() - timeOffset)) * v / 1000;
                var headingRad = position.heading * degToRad;
                var commonCoeff = radToDeg * ds / earthRadius;
                var pos = position.coordinates;
                return {
                    latitude: (pos.latitude + (commonCoeff * Math.cos(headingRad)) - markerCoords.latitude),
                    longitude: (pos.longitude + (commonCoeff * Math.sin(headingRad) / Math.cos(degToRad * pos.longitude))
                        - markerCoords.longitude)
                };
            }

            setInterval(function () {
                if (suspendSimulation) return;
                var currTime = new Date().getTime();
                for (var trackerId in modelMarkers) {
                    var marker = modelMarkers[trackerId];
                    if (marker.simulation && currTime - marker.simStartTime.getTime() < maxSimulationDelayMs) {
                        var dt = currTime - marker.simStartTime.getTime();
                        marker.setLatLng([
                            marker.simStartPoint.latitude + (marker.simMotionVector.latitude * dt / predictionTimeMs),
                            marker.simStartPoint.longitude + (marker.simMotionVector.longitude * dt / predictionTimeMs)
                        ]);
                        if (followTracker && followTracker.id == trackerId) rawPan(marker.getLatLng());
                    }
                }
            }, simulationStepMs);
        },
        stopFollow: function () {
        }
    });
    lang.mixin(TrackerDisplayPlugin, Evented.prototype);
    return TrackerDisplayPlugin;
});
