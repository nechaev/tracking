define([
    'dojo/_base/declare',
    'dojo/request/xhr',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    './AdminTrackersGrid',
    './AdminGroupsGrid',
    './userGroupsStore',
    'dojo/i18n!./nls/resources',
    'dijit/form/Button'
], function(
    declare,
    xhr,
    WidgetBase,
    TemplatedMixin,
    WidgetsInTemplateMixin,
    TrackersGrid,
    GroupsGrid,
    userGroupsStore,
    nls
    ) {
    return declare([WidgetBase, TemplatedMixin, WidgetsInTemplateMixin], {
        nls: nls,
        templateString: '<div>' +
                '<div><button data-dojo-type="dijit/form/Button" data-dojo-attach-event="onClick:addTracker">&gt;</button></div>' +
                '<div><button data-dojo-type="dijit/form/Button" data-dojo-attach-event="onClick:removeTracker">&lt;</button></div>' +
            '</div',
        addTracker: function () {
            var group = GroupsGrid.getSelectedGroup();
            var tracker = TrackersGrid.getSelectedTracker();
            if (tracker && group) xhr('/rest/trackerAdmin/addTrackerToGroup', {
                query: {
                    tracker: tracker.id,
                    group: group.id,
                    nocache: new Date().getTime()
                }
            }).then(function () {
                    group.trackers.push(tracker.id);
                    userGroupsStore.put(group);
                })
        },
        removeTracker: function () {
            var group = GroupsGrid.getSelectedGroup();
            var tracker = TrackersGrid.getSelectedTracker();
            if (tracker && group) xhr('/rest/trackerAdmin/removeTrackerFromGroup', {
                query: {
                    tracker: tracker.id,
                    group: group.id,
                    nocache: new Date().getTime()
                }
            }).then(function () {
                    group.trackers = group.trackers.filter(function (trackerId) {
                        return trackerId != tracker.id;
                    });
                    userGroupsStore.put(group);
                })
        }
    })
});
