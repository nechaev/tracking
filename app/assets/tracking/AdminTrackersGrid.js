define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    './userTrackersStore',
    'dgrid/OnDemandGrid',
    'dgrid/Selection',
    'dojo/i18n!./nls/resources'
], function (
    declare,
    lang,
    userTrackersStore,
    Grid,
    Selection,
    nls
    ) {
    var TrackersGrid = declare([Grid, Selection], {
        store: userTrackersStore,
        columns: {
            id: nls.trackerIdField,
            name: nls.nameField,
            trackerType: nls.trackerTypeField
        },
        selectionMode: 'single',
        postCreate: function() {
            this.inherited(arguments);
            TrackersGrid.instance = this;
        }
    });
    TrackersGrid.getSelectedTracker = function() {
        var sel = this.instance.selection;
        for (var id in sel) if (sel.hasOwnProperty(id) && sel[id]) return this.instance.row(id).data;
        return undefined;
    };
    return TrackersGrid;
});
