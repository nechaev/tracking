define([
    'dojo/_base/declare',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    './AdminGroupDialog',
    './AdminGroupsGrid',
    'dojo/i18n!./nls/resources',
    'dijit/form/Button'
], function(
    declare,
    WidgetBase,
    TemplatedMixin,
    WidgetsInTemplateMixin,
    GroupDialog,
    GroupsGrid,
    nls
    ) {
    return declare([WidgetBase, TemplatedMixin, WidgetsInTemplateMixin], {
        nls: nls,
        templateString: '<div>' +
            '<button data-dojo-type="dijit/form/Button" data-dojo-attach-event="onClick:newGroup">${nls.newGroupButton}</button>' +
            '<button data-dojo-type="dijit/form/Button" data-dojo-attach-event="onClick:editGroup">${nls.editGroupButton}</button>' +
            '</div>',

        newGroup: function() {
            GroupDialog.show();
        },
        editGroup: function() {
            var group = GroupsGrid.getSelectedGroup();
            if (group) GroupDialog.show(group);
        }
    })
});