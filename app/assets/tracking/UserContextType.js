define([
    'types/Map',
    'types/List',
    './user/User',
    './Tracker',
    './TrackerGroup'
],function(
    MapType,
    ListType,
    UserType,
    TrackerType,
    TrackerGroupType
    ) {
    return MapType({
        'trackers': ListType(TrackerType),
        'groups': ListType(TrackerGroupType)
    })
});
