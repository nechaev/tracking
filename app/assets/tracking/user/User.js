define(['types/Map','types/String'], function(MapType,StringType) {
    return MapType({
        id: StringType,
        name: StringType
    });
});
