define([
    'dojo/_base/declare',
    'dojo/Evented',
    'dojo/topic',
    'dojo/Stateful',
    'tracking/TrackingUserContext',
    'types/Map',
    'types/String',
    'types/Instant',
    './PositionState',
    './ConnectionEvent'
], function (
    declare,
    Evented,
    topic,
    Stateful,
    TrackingUserContext,
    MapType,
    StringType,
    InstantType,
    PositionEventType,
    ConnectionEventType
    ) {

    function nameSorter (objA, objB) {
        if (objA.name < objB.name) return -1;
        else if (objA.name > objB.name) return 1;
        else return 0;
    }
    var TrackerCollection = declare([Evented], {
        constructor: function () {
            var self = this;
            this.trackers = {};
            this.groups = [];
            TrackingUserContext.then(function(context) {
                context.trackers.forEach(function(tracker) {
                    var trackerModel = new Stateful(tracker);
                    self.trackers[trackerModel.id] = trackerModel;
                    self.emit('add', trackerModel);
                });
                self.groups = context.groups.map(function(trackerGroup) {
                    trackerGroup.trackers = trackerGroup.trackers.map(function(trackerId) {
                        return self.trackers[trackerId];
                    });
                    trackerGroup.trackers.sort(nameSorter);
                    return trackerGroup;
                });
                self.groups.sort(nameSorter);
                self.emit('set-groups');
                context.trackersState.forEach(function(state) {
                    if(state.position) {
                        state.position.timestamp = state.lastTimestamp;
                        state.position.clientTimestamp = state.lastTimestamp;
                    }
                    self.trackers[state.trackerId].set({
                        lastTimestamp: state.lastTimestamp,
                        position: state.position,
                        ignition: state.ignition,
                        connection: state.connection
                    });
                })
            });
            function TrackingEventType(eventType) {
                return MapType({
                    timestamp: InstantType,
                    trackerId: StringType,
                    event: eventType
                })
            }
            function updateStateFn(prop, eventType) {
                var trackingEventType = TrackingEventType(eventType);
                return function(msg) {
                    var trackingEvent = trackingEventType.parseJSON(msg);
                    var tracker = self.trackers[trackingEvent.trackerId];
                    trackingEvent.event.timestamp = trackingEvent.timestamp;
                    trackingEvent.event.clientTimestamp = new Date();
                    tracker.set(prop, trackingEvent.event);
                    tracker.set('lastTimestamp', trackingEvent.timestamp);
                }
            }
            topic.subscribe('comet.tracking.event.position', updateStateFn('position', PositionEventType));
            topic.subscribe('comet.tracking.event.connection', updateStateFn('connection', ConnectionEventType));
        }
    });
    return new TrackerCollection();
});
