define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/Evented',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dojo/dom-class',
    'dojo/dom-construct',
    './trackerCollection',
    'tools/loadStyle',
    'dojo/text!./TrackerList.css',
    'dojo/i18n!./nls/resources'
], function(
    declare,
    lang,
    Evented,
    WidgetBase,
    TemplatedMixin,
    domClass,
    construct,
    trackerCollection,
    loadStyle,
    css,
    nls
    ) {

    loadStyle(css);

    var TrackerList = undefined;

    var ListItem = declare([WidgetBase,TemplatedMixin], {
        nls: nls,
        templateString: '<div data-dojo-attach-event="onclick:itemClick">' +
            '<span data-dojo-attach-point="indicatorNode" class="state-indicator"></span>' +
            '<span class="tracker-name"> ${model.name}</span>' +
            '</div>',
        postCreate: function() {
            this.inherited(arguments);
            var self = this;
            this.watchHandle = this.model.watch('connection', function(property, oldState, newState) {
                self.setState(newState);
            });
            this.setState(this.model.connection);
        },
        destroy: function() {
            this.watchHandle.unwatch();
            this.inherited(arguments);
        },
        indicatorNode: undefined,
        setState: function(state) {
            var stateCls;
            if (state) {
                if (state.isGsmConnected) {
                    if (state.isGpsConnected) stateCls = 'online';
                    else stateCls = 'delayed';
                } else stateCls = 'offline';
            } else stateCls = 'offline';
            domClass.replace(this.indicatorNode, stateCls, "offline delayed online");
        },
        itemClick: function(event) {
            TrackerList.emit('TrackerClick', this.model, event);
        }
    });
    TrackerList = declare([WidgetBase,TemplatedMixin], {
        nls: nls,
        templateString: '<div class="tracker-list"></div>',
        postCreate: function() {
            this.inherited(arguments);
            var self = this;
            function update() {
                trackerCollection.groups.forEach(function(trackerGroup) {
                    var groupNode = construct.create("div", null, self.domNode);
                    construct.create("div", {
                        "class":"tracker-group"
                    }, groupNode).innerHTML = trackerGroup.name;
                    trackerGroup.trackers.forEach(function(trackerModel) {
                        groupNode.appendChild(new ListItem({model: trackerModel}).domNode);
                    });
                });
            }
            this.eventHandler = trackerCollection.on('set-groups', update);
            update();
        },
        destroy: function() {
            this.eventHandler.remove();
            this.inherited(arguments);
        }
    });
    lang.mixin(TrackerList, Evented.prototype);
    return TrackerList;
});
