define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/query',
    'dojo/Evented',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'comet/main',
    './TrackInfo',
    'dojo/i18n!./nls/resources',
    'dojo/text!./TrackDialog.html',
    'tools/loadStyle',
    'dojo/text!./TrackDialog.css',
    'dijit/form/RadioButton',
    'dijit/form/DateTextBox',
    'dijit/form/TimeTextBox'
], function(
    declare,
    lang,
    query,
    Evented,
    WidgetBase,
    TemplatedMixin,
    WidgetsInTemplateMixin,
    comet,
    TrackInfoType,
    nls,
    template,
    loadStyle,
    css
    ) {
    loadStyle(css);
    var TrackDialog = declare([WidgetBase,TemplatedMixin,WidgetsInTemplateMixin], {
        nls: nls,
        templateString: template,
        dateSelect: undefined,
        timeSelect: undefined,
        postCreate: function() {
            this.inherited(arguments);
            this.periodChange();
        },
        queryLog: function() {
            var end = this.selectedPeriod.period().getTime();
            var duration = parseInt(query('input[name="range"]:checked', this.domNode)[0].value)*1000;
            var start = end - duration;
            var tracker = this.tracker;
            var now = this.selectedPeriod.now;
            comet.ask('tracking.track', {
                trackerId: tracker.id,
                start: start,
                end: end
            }).then(function(trackInfo) {
                TrackDialog.emit('ShowTrack', {
                    trackInfo: TrackInfoType.parseJSON(trackInfo),
                    tracker: tracker,
                    duration: duration,
                    now: now
                });
            });
        },
        periodChange: function() {
            var trackDialog = this;
            var periodVariant = query('input[name="period"]:checked', this.domNode)[0].value;
            if (periodVariant == "now") {
                this.selectedPeriod = {
                    now: true,
                    period: function() {
                        return new Date()
                    }
                };
                this.dateSelect.setDisabled(true);
                this.timeSelect.setDisabled(true);
                this.queryLog();
            } else {
                this.selectedPeriod = {
                    now: false,
                    period: function() {
                        var d = trackDialog.dateSelect.get('value'),
                            t = trackDialog.timeSelect.get('value');
                        return new Date(d.getFullYear(), d.getMonth(), d.getDate(),
                                        t.getHours(), t.getMinutes(), t.getSeconds());
                    }
                };
                this.dateSelect.setDisabled(false);
                this.timeSelect.setDisabled(false);
                var date = new Date();
                this.dateSelect.set('value', date);
                this.timeSelect.set('value', date);
            }
        },
        clearLog: function() {
            TrackDialog.emit('ClearTrack');
        },
        destroy: function() {
            this.clearLog();
            this.inherited(arguments);
        }
    });
    lang.mixin(TrackDialog, Evented.prototype);
    return TrackDialog;

});