define([
    'dojo/_base/declare',
    'dijit/Dialog',
    'tools/Deferred',
    'dojo/i18n!./nls/resources'
], function(
    declare,
    Dialog,
    Deferred,
    nls
    ) {
    var TrackerSettingsPopup = declare(Dialog, {
        nls: nls,
        title: 'Tracker settings'
    });
    TrackerSettingsPopup.show = function(id) {
        if (!this.instance) this.instance = new TrackerSettingsPopup();
        var dialog = this.instance;
        var imageUrl = '/rest/trackerAdmin/trackerKey?trackerId=' + encodeURIComponent(id) + '&nocache=' + (new Date().getTime().toString());
        dialog.set('content', '<img src="' + imageUrl + '"/>');
        dialog.show();
    };
    return TrackerSettingsPopup;
});