define([
    'dojo/_base/declare',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dojo/Evented',
    'dojo/dom-class',
    'dojo/dom-style',
    'dojo/on',
    'tools/loadStyle',
    'dojo/text!./Window.css',
    'dojo/text!./Window.html'
], function(declare, WidgetBase, TemplatedMixin, Evented, domClass, domStyle, on, loadStyle, css, template) {

    loadStyle(css);
    function enableDraggingSupport(self) {
        var offsetX = 0,
            offsetY = 0,
            moveListener = undefined,
            upListener = undefined,
            touchId = undefined;

        function dragStart(event) {
            self.emit('focus');
            offsetX = event.clientX - self.domNode.offsetLeft;
            offsetY = event.clientY - self.domNode.offsetTop;
            domClass.add(self.header, 'dragging');
        }
        function mouseDown(event) {
            moveListener = on(document, 'mousemove', mouseMove);
            upListener = on(document, 'mouseup', dragEnd);
            dragStart(event);
            event.stopPropagation();
            event.preventDefault();
        }

        function touchStart(event) {
            if(!touchId) {
                var touch = event.changedTouches[0];
                touchId = touch.identifier;
                moveListener = on(document, 'touchmove', touchMove);
                upListener = on(document, 'touchend', touchEnd);
                dragStart(touch);
                event.stopPropagation();
                event.preventDefault();
            }
        }
        function dragEnd() {
            domClass.remove(self.header, 'dragging');
            moveListener.remove();
            upListener.remove();
        }
        function touchEnd(event) {
            var touches = event.changedTouches;
            for (var i=0; i<touches.length; i++) if(touches[i].identifier == touchId) {
                dragEnd();
                touchId = undefined;
            }
        }
        function dragMove(event) {
            domStyle.set(self.domNode, {
                top: Math.max(event.clientY - offsetY,0).toString() + 'px',
                left: Math.max(event.clientX - offsetX,0).toString() + 'px'
            });
        }
        function mouseMove(event) {
            dragMove(event);
            event.preventDefault();
        }
        function touchMove(event) {
            var touches = event.changedTouches;
            for (var i=0; i<touches.length; i++) if(touches[i].identifier == touchId) {
                dragMove(touches[i]);
                event.preventDefault();
            }
        }
        on(self.header, 'mousedown', mouseDown);
        on(self.header, 'touchstart', touchStart);
    }
    function enableResizeSupport(self) {
        domClass.remove(self.resizeHandle, 'hidden');
        var offsetX = 0,
            offsetY = 0,
            moveListener = undefined,
            upListener = undefined,
            touchId = undefined;

        function dragStart(event) {
            self.emit('focus');
            offsetX = event.clientX - (self.domNode.clientWidth - 10); //2x5 padding
            offsetY = event.clientY - (self.domNode.clientHeight - 26); //21 + 5 padding
            domClass.add(self.resizeHandle, 'dragging');
            event.stopPropagation();
            event.preventDefault();
        }
        function mouseDown(event) {
            moveListener = on(document, 'mousemove', mouseMove);
            upListener = on(document, 'mouseup', dragEnd);
            dragStart(event);
        }
        function touchStart(event) {
            if(!touchId) {
                var touch = event.changedTouches[0];
                touchId = touch.identifier;
                moveListener = on(document, 'touchmove', touchMove);
                upListener = on(document, 'touchend', touchEnd);
                dragStart(touch);
            }
        }
        function dragEnd() {
            domClass.remove(self.resizeHandle, 'dragging');
            moveListener.remove();
            upListener.remove();
        }
        function touchEnd(event) {
            var touches = event.changedTouches;
            for (var i=0; i<touches.length; i++) if(touches[i].identifier == touchId) {
                dragEnd();
                touchId = undefined;
            }
        }
        function dragMove(event) {
            self.initialStyle.width = Math.max(event.clientX - offsetX,0).toString() + 'px';
            self.initialStyle.height = Math.max(event.clientY - offsetY,0).toString() + 'px';
            domStyle.set(self.domNode, {
                width: self.initialStyle.width,
                height: self.initialStyle.height
            });
        }
        function mouseMove(event) {
            dragMove(event);
            event.preventDefault();
        }
        function touchMove(event) {
            var touches = event.changedTouches;
            for (var i=0; i<touches.length; i++) if(touches[i].identifier == touchId) {
                dragMove(touches[i]);
                event.preventDefault();
            }
        }
        on(self.resizeHandle, 'mousedown', mouseDown);
        on(self.resizeHandle, 'touchstart', touchStart);
    }
    return declare([WidgetBase, TemplatedMixin, Evented], {
        templateString: template,
        header: undefined,
        resizeHandle: undefined,
        sizing: {},
        postCreate: function() {
            this.inherited(arguments);
            this.initialStyle = this.initialStyle || {};
            domStyle.set(this.domNode, this.initialStyle);
            var contentProps = this.contentProps || {};
            if (this.contentClass) {
                this.content = new this.contentClass(contentProps, this.containerNode);
                this.containerNode = this.content.domNode;
            }
            if (this.draggable) enableDraggingSupport(this);
            if (this.resizeable) enableResizeSupport(this);
        },
        toggleFold: function() {
            if (domClass.contains(this.domNode, 'fold')) {
                domClass.remove(this.domNode, 'fold');
                if (this.initialStyle.height) domStyle.set(this.domNode, "height", this.initialStyle.height);
                if (!this.initialStyle.width) domStyle.set(this.domNode, "width", "");
                domClass.remove(this.containerNode, 'hidden');
            } else {
                domClass.add(this.domNode, 'fold');
                //Fix width to prevent header collapse (clientWidth - 2x5px padding)
                if (!this.initialStyle.width) domStyle.set(this.domNode, "width", (this.domNode.clientWidth - 10).toString() + 'px');
                domClass.add(this.containerNode, 'hidden');
                domStyle.set(this.domNode, "height", "");
            }
        },
        closeWindow: function(event) {
            event.preventDefault();
            if(this.content) this.content.destroy();
            this.destroy();
            this.emit('close');
        },
        focusWindow: function() {
            this.emit('focus');
        }
    });
});
