define([
    'dojo/_base/declare',
    './Window',
    'dojo/dom-construct',
    'dojo/dom-style',
    'dojo/dom-geometry'
], function (declare, Window, construct, domStyle, domGeom) {

    var Workspace = declare(null, {
        constructor: function (props, domNode) {
            var openedWindows = {};
            var autoId = 1;
            var zIndex = 100;
            function showWindow(wnd) {
                domStyle.set(wnd.domNode, "zIndex", zIndex);
                zIndex = zIndex + 1;
            }
            Workspace.openWindow = function(windowProps, clickEvent) {
                var key;
                if (windowProps.key) key = windowProps.key;
                else {
                    key = 'auto-' + autoId;
                    autoId = autoId +  1;
                }
                if (!openedWindows[key]) {
                    if (clickEvent) {
                        windowProps.initialStyle.left = '' + clickEvent.clientX + 'px';
                        windowProps.initialStyle.top = '' + clickEvent.clientY + 'px';
                    }
                    var wnd = new Window(windowProps, construct.create("div", null, domNode));
                    var workspaceGeometry = domGeom.position(domNode);
                    var windowGeometry = domGeom.position(wnd.domNode);
                    if (windowGeometry.y + windowGeometry.h > workspaceGeometry.h) domStyle.set(wnd.domNode, 'top',
                        '' + (workspaceGeometry.h - windowGeometry.h) + 'px');
                    if (windowGeometry.x + windowGeometry.w > workspaceGeometry.w) domStyle.set(wnd.domNode, 'left',
                        '' + (workspaceGeometry.w - windowGeometry.w) + 'px');
                    openedWindows[key] = wnd;
                    wnd.on('close', function() {
                        delete openedWindows[key];
                    });
                    wnd.on('focus', function() {
                        showWindow(wnd)
                    });
                }
                showWindow(openedWindows[key]);
            };
        }
    });

    return Workspace;
});
