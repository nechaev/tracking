define([
    'dojo/_base/declare',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'comet/main',
    'dojo/dom-class',
    'tools/loadStyle',
    'dojo/text!./StateIndicator.css',
    'dojo/i18n!../nls/resources'
], function(
        declare,
        WidgetBase,
        TemplatedMixin,
        comet,
        domClass,
        loadStyle,
        css,
        nls
    ) {

    loadStyle(css);

    return declare([WidgetBase, TemplatedMixin], {
        templateString: '<div>' +
            '<div class="comet-state-indicator-image" data-dojo-attach-point="indicator"></div>' +
            '</div>',
        indicator: undefined,
        postCreate: function() {
            this.inherited(arguments);
            var indicator = this.indicator;
            function setOffline() {
                domClass.replace(indicator, 'disconnected', 'connected');
                indicator.title = nls.disconnected;
            }
            function setOnline(method) {
                domClass.replace(indicator, 'connected', 'disconnected');
                indicator.title = nls.connected + ' (' + method + ')';
            }
            setOffline();
            comet.on('connected', setOnline);
            comet.on('disconnected', setOffline);
        }
    });
});
