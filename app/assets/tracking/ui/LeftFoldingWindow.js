define([
    'dojo/_base/declare',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'tools/loadStyle',
    'dojo/text!./LeftFoldingWindow.css',
    'dojo/text!./LeftFoldingWindow.html',
    'dojo/dom-class',
    'dojo/string'
], function (
    declare,
    WidgetBase,
    TemplatedMixin,
    loadStyle,
    css,
    template,
    domClass,
    string
    ) {
    return declare([WidgetBase, TemplatedMixin], {
        templateString: template,
        width: 300,
        postCreate: function () {
            loadStyle(string.substitute(css, this));
            this.inherited(arguments);
        },
        foldButtonClick: function () {
            domClass.toggle(this.domNode, 'left-folding-window-fold-' + this.id);
        }
    });
});
