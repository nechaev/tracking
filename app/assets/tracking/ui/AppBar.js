define([
    'dojo/_base/declare',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'tools/loadStyle',
    'dojo/text!./AppBar.css',
    'dojo/i18n!../nls/resources'
], function(
    declare,
    WidgetBase,
    TemplatedMixin,
    WidgetsInTemplateMixin,
    loadStyle,
    css,
    nls
    ) {

    loadStyle(css);
    return declare([WidgetBase, TemplatedMixin, WidgetsInTemplateMixin], {
        nls: nls,
        templateString: '<div class="app-bar-anchor"><div class="app-bar-container">' +
            //'<span class="app-title">${nls.trackingAppTitle}</span>' +
            '<span class="app-bar-item"><a href="/view" target="_blank">${nls.mapView}</a></span>' +
            '<span class="app-bar-separator">|</span>' +
            '<span class="app-bar-item"><a href="/dashboard" target="_blank">${nls.dashboard}</a></span>' +
            '<span class="app-bar-separator">|</span>' +
            '<span class="app-bar-item"><a href="/report/mileage" target="_blank">${nls.mileageReportTitle}</a></span>' +
            '<span class="app-bar-separator">|</span>' +
            '<span class="app-bar-item"><a href="/admin-trackers" target="_blank">${nls.administration}</a></span>' +
            '<span class="app-bar-separator">|</span>' +
            '<span class="app-bar-item"><a href="/auth/logout" target="_blank">${nls.logout}</a></span>' +
            //'<span class="app-bar-item"><a href="/dashboard">${nls.</a></span>'
            '</div></div>'
    });
});
