define(['dojo/_base/declare','dijit/DropDownMenu', './ActionMenuItem', 'dijit/PopupMenuItem', '../reports/ReportsMenu',
    'dojo/i18n!../nls/resources'], function( declare, DropDownMenu, ActionMenuItem, PopupMenuItem, ReportsMenu, nls) {
    return declare([DropDownMenu], {
        postCreate: function() {
            this.inherited(arguments);
            [
                new ActionMenuItem({
                    actionName: 'tracker-list',
                    label: nls.trackerListTitle
                }),
                new PopupMenuItem({
                    popup: new ReportsMenu,
                    label: nls.reportsMenu
                })
            ].forEach(this.addChild, this);
        }
    });
});
