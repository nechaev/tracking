define(['dojo/_base/declare', 'dijit/MenuItem', 'dojo/on', 'dojo/topic'], function(declare, MenuItem, on, topic) {
    return declare([MenuItem], {
        actionName: undefined,
        constructor: function(options, domNode) {
            this.inherited(arguments);
        },
        postCreate: function() {
            this.inherited(arguments);
            var eventName = 'action-' + this.actionName;
            on(this.domNode, 'click', function() {
                topic.publish(eventName);
            })
        }
    });
});