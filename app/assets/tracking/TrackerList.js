define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/Evented',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/form/Select',
    'dijit/registry',
    'dojo/dom-class',
    'dojo/dom-construct',
    'dojo/store/Memory',
    'dojo/store/Observable',
    'dgrid/List',
    'dgrid/extensions/DijitRegistry',
    './groupStore',
    './trackerStore',
    'tools/loadStyle',
    'dojo/text!./TrackerList.css',
    'dojo/i18n!./nls/resources'
], function (
    declare,
    lang,
    Evented,
    WidgetBase,
    TemplatedMixin,
    Select,
    registry,
    domClass,
    domConstruct,
    Memory,
    Observable,
    List,
    DijitRegistryExtension,
    groupStore,
    trackerStore,
    loadStyle,
    css,
    nls
    ) {

    loadStyle(css);

    var TrackerList = undefined;

    var TrackerListItem = declare([WidgetBase, TemplatedMixin], {
        nls: nls,
        templateString: '<div data-dojo-attach-event="onclick:itemClick">' +
            '<span class="state-indicator" data-dojo-attach-point="indicatorNode"></span>' +
            '<span class="tracker-name"    data-dojo-attach-point="nameNode"></span>' +
            '</div>',
        indicatorNode: undefined,
        nameNode: undefined,

        postCreate: function () {
            this.inherited(arguments);
            var listItem = this;
            trackerStore.get(this.trackerId).then(function(model) {
                listItem.model = model;
                listItem.nameNode.innerHTML = model.name;
                listItem.watchHandle = model.watch('timestamp', function (property, oldState, newState) {
                    listItem.updateState(model);
                });
                listItem.timerHandle = setInterval(function() {
                    listItem.updateState(model);
                }, 10000);
                listItem.updateState(model);
            });
        },
        destroy: function () {
            if(this.watchHandle) this.watchHandle.unwatch();
            if(this.timerHandle) clearInterval(this.timerHandle);
            this.inherited(arguments);
        },
        //setState: function (state) {
        updateState: function (model) {
            var stateCls;
            if (model.timestamp) {
                var dt = new Date().getTime() - model.timestamp.getTime();
                if (dt < 60000) stateCls = 'online';
                else if (dt < 60000) stateCls = 'delayed';
                else stateCls = 'online';
            }
//            if (state) {
//                if (state.isGsmConnected) {
//                    if (state.isGpsConnected) stateCls = 'online';
//                    else stateCls = 'delayed';
//                } else stateCls = 'offline';
//            } else stateCls = 'offline';
            domClass.replace(this.indicatorNode, stateCls, "offline delayed online");
        },
        itemClick: function (event) {
            TrackerList.emit('TrackerClick', this.model, event);
        }
    });

    var TrackerListPanel = declare([List, DijitRegistryExtension], {
        showTrackers: function(trackers) {
            this.cleanup();
            //this.contentNode.innerHTML = "";
            this.renderArray(trackers);
        },
        renderRow: function (item, options) {
            return new TrackerListItem({
                trackerId: item
            }).domNode;
        },
        removeRow: function (rowElement) {
            var widget = registry.byNode(rowElement);
            if(widget){ widget.destroyRecursive(); }
            this.inherited(arguments);
        }
    });

    var localStorageSelectedGroupKey = 'tracker-list.selected-group-id';
    TrackerList = declare([WidgetBase, TemplatedMixin], {
        templateString: '<div>' +
            '<div style="position: absolute; top:0; width: 100%">' +
                '<select data-dojo-attach-point="groupSelectNode"></select>' +
            '</div>' +
            '<div style="position: absolute; top:30px; bottom: 0; width: 100%; height: auto;" data-dojo-attach-point="trackerListNode"></div>' +
            '</div>',
        groupSelectNode: undefined,
        trackerListNode: undefined,
        postCreate: function () {
            this.inherited(arguments);
            var trackerListPanel = new TrackerListPanel({}, this.trackerListNode);
            var storedValue = localStorage ? localStorage[localStorageSelectedGroupKey] : '';
            this.groupSelect = new Select({
                name: 'groupSelect',
                store: groupStore,
                labelAttr: 'name',
                value: storedValue || '',
                onChange: function(selectedGroupId) {
                    //var selectedGroupId = groupSelect.get('value');
                    groupStore.get(selectedGroupId).then(function(group) {
                        trackerListPanel.showTrackers(group.trackers);
                    });
                    if (localStorage) localStorage[localStorageSelectedGroupKey] = selectedGroupId;
                },
                onSetStore: function() {
                    this.onChange(this.get('value'));
                }
            }, this.groupSelectNode);
        }
    });
    lang.mixin(TrackerList, Evented.prototype);
    return TrackerList;
});

