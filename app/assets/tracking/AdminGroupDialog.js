define([
    'dojo/_base/declare',
    'dijit/Dialog',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/i18n!./nls/resources',
    './userGroupsStore',
    'dijit/form/TextBox',
    'dijit/form/Button'
], function(
    declare,
    Dialog,
    WidgetBase,
    TemplatedMixin,
    WidgetsInTemplateMixin,
    nls,
    groupsStore
    ) {

    var GroupForm = declare([WidgetBase,TemplatedMixin,WidgetsInTemplateMixin], {
        dialog: undefined,
        templateString:
            '<div>' +
                '<div class="dijitDialogPaneContentArea"><input data-dojo-type="dijit/form/TextBox" data-dojo-attach-point="nameField"/></div>' +
                '<div class="dijitDialogPaneActionBar">' +
                    '<button data-dojo-type="dijit/form/Button" data-dojo-attach-event="onClick:saveClick">Save</button>' +
                    '<button data-dojo-type="dijit/form/Button" data-dojo-attach-event="onClick:cancelClick">Cancel</button>' +
                '</div>' +
            '</div>',
        saveClick: function() {
            var name = this.nameField.get('value');
            if (this.group) {
                this.group.name = name;
                groupsStore.put(this.group);
            } else {
                groupsStore.add({
                    name: name,
                    trackers: []
                })
            }
            this.dialog.hide();
        },
        cancelClick: function() {
            this.dialog.hide();
        }
    });

    var AdminGroupDialog = declare(Dialog, {
        nls: nls,
        title: 'Group',
        postCreate: function() {
            this.inherited(arguments);
            var form = new GroupForm({dialog: this});
            this.set('content', form);
            this.setGroup = function(group) {
                if (group) {
                    form.group = group;
                    form.nameField.set('value', group.name);
                } else {
                    form.group = undefined;
                    form.nameField.set('value', '');
                }
            }
        }
    });
    AdminGroupDialog.show = function(group) {
        if (!this.instance) this.instance = new AdminGroupDialog();
        this.instance.setGroup(group);
        this.instance.show();
    };
    AdminGroupDialog.hide = function() {
        this.instance.hide();
    };
    return AdminGroupDialog;
});

