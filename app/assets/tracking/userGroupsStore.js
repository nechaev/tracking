define([
    'dojo/store/Observable',
    'dojo/store/JsonRest'
], function(
    Observable,
    JsonRest
    ) {
    return Observable(new JsonRest({
        target: '/rest/trackerAdmin/userGroups/',
        getIdentity: function(item) {
            return item.id ? encodeURIComponent(item.id) : undefined;
        }
    }));
});