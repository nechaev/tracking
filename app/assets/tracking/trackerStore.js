define([
    'dojo/store/Observable',
    'dojo/Stateful',
    'dojo/store/util/QueryResults',
    'dojo/store/util/SimpleQueryEngine',
    'comet/main',
    'tools/Deferred',
    './CachedStore',
    './TrackingUserContext',
    './TrackerState'
], function(
    Observable,
    Stateful,
    QueryResults,
    SimpleQueryEngine,
    comet,
    Deferred,
    CachedStore,
    TrackingUserContext,
    TrackerStateType
    ) {

    var defTrackers = new Deferred;

    var store = Observable({
        queryEngine: SimpleQueryEngine,
        get: function(id) {
            return defTrackers.map(function(trackers) {
                return trackers[id];
            });
        },
        query: function(query, options) {
            var queryEngine = this.queryEngine;
            return QueryResults(defTrackers.map(function(trackers) {
                var trackerList = [];
                for (var trackerId in trackers) if (trackers.hasOwnProperty(trackerId)) trackerList.push(trackers[trackerId]);
                return queryEngine(query, options)(trackerList)
            }));
        }
    });
    TrackingUserContext.then(function(userContext) {
        var trackers = {};
        userContext.groups.forEach(function(group) {
            group.trackers.forEach(function(trackerId) {
                if (!trackers.hasOwnProperty(trackerId)) trackers[trackerId] = new Stateful({
                    id: trackerId
                });
            });
        });
        var pending = 0;
        for (var trackerId in trackers) if (trackers.hasOwnProperty(trackerId)) {
            comet.ask('tracking.tracker-description', trackerId).then(function(trackerProps) {
                trackers[trackerProps.id].set(trackerProps);
                pending--;
                if (pending<=0) defTrackers.resolve(trackers);
            });
            pending++;
            comet.publish('tracking.observe-tracker-state', trackerId);
        }
        comet.on('tracking.event',function(event) {
            store.get(event.trackerId).then(function(tracker) {
                var newState = TrackerStateType.parseJSON(event);
                if (newState.position) {
                    newState.position.clientTimestamp = new Date();
                    newState.position.timestamp = newState.timestamp;
                }
                tracker.set(newState);
                store.notify(tracker, tracker.id);
            });
        });
    });
    return store;
});
