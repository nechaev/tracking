define([
    'dojo/_base/declare',
    'dojo/_base/kernel',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/query',
    'dojo/dom-construct',
    'dojo/request/xhr',
    'dojo/json',
    'types/String',
    'types/Date',
    'dojo/text!./Report.html',
    'dojo/text!./Report.css',
    'tools/loadStyle',
    'dojo/i18n!../nls/resources',
    'dijit/form/CheckBox',
    'dijit/form/DateTextBox',
    'dijit/form/Button',
    'dijit/Fieldset'
], function (
    declare,
    dojo,
    WidgetBase,
    TemplatedMixin,
    WidgetsInTemplateMixin,
    query,
    domConstruct,
    xhr,
    json,
    StringType,
    DateType,
    template,
    css,
    loadStyle,
    nls,
    CheckBox) {

    loadStyle(css);

    return declare([WidgetBase, TemplatedMixin, WidgetsInTemplateMixin], {
        reportName: undefined,
        nls: nls,
        templateString: template,
        reportNode: undefined,
        trackerSelect: undefined,
        startPeriodField: undefined,
        endPeriodField: undefined,

        postCreate : function() {
            this.inherited(arguments);
            this.startPeriodField.set('value', new Date());
            this.endPeriodField.set('value', new Date());
            var trackerSelect = this.trackerSelect;
            xhr('/report/context', {
                handleAs: 'json'
            }).then(function(context) {
                context.forEach(function(tracker) {
                    var checkbox = new CheckBox({
                        value: tracker.id,
                        checked: false
                    });
                    var row = domConstruct.create('div',null, trackerSelect.containerNode);
                    domConstruct.place(checkbox.domNode, row);
                    domConstruct.create('label', {"for": checkbox.id}, row).innerHTML=tracker.name;
                });
            });
        },
        buildRequestUrl: function(format) {
            return '/report/generate?reportRequest=' + encodeURIComponent(json.stringify(this.reportRequest(format)))
        },
        generateReport: function () {
            var reportNode = this.reportNode;
            //comet.ask('reporting.generate-report', this.reportRequest(), StringType, 60000)
            xhr(this.buildRequestUrl('embeddable')).then(function (reportHtml) {
                reportNode.innerHTML = reportHtml;
            }, function () {
                 alert('Report error');
            });
        },
        generatePdf: function () {
            window.open(this.buildRequestUrl('pdf'));
        },
        generateXls: function () {
            window.open(this.buildRequestUrl('xls'));
        },
        reportRequest: function(format) {
            var trackers = [];
            query('input:checked', this.trackerSelect.domNode).forEach(function(elem) {
                trackers.push(elem.getAttribute('value'));
            });
            return {
                name: this.reportName,
                lang: dojo.locale,
                format: format,
                start: DateType.toJSON(this.startPeriodField.get('value')),
                end: DateType.toJSON(this.endPeriodField.get('value')),
                trackers: trackers
            }
        }
    });
});
