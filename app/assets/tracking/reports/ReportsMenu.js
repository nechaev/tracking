define(['dojo/_base/declare', 'dijit/DropDownMenu', './ReportMenuItem', './mileage'], function(
    declare, DropDownMenu, ReportMenuItem, MileageReport) {
    return declare([DropDownMenu], {
        postCreate: function() {
            this.inherited(arguments);
            [
                new ReportMenuItem({
                    reportClass: MileageReport
                })
            ].forEach(this.addChild, this);
        }
    })
})
