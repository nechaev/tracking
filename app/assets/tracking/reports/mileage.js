define(['dojo/_base/declare','./Report', 'dojo/i18n!../nls/resources'], function(declare, Report, nls) {
    var reportName = 'mileage';
    var mileageReport = declare(Report, {
        reportName: reportName
    });
    mileageReport.reportTitle = nls.mileageReportTitle;
    mileageReport.reportUrl = reportName;
    return mileageReport;
});
