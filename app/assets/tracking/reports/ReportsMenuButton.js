define(['dojo/_base/declare', 'dijit/form/DropDownButton','./ReportsMenu','dojo/i18n!../nls/resources'], function(
    declare, DropDownButton, ReportsMenu, nls) {
    return declare(DropDownButton, {
        label: nls.reportsMenu,
        startup: function() {
            this.dropDown = new ReportsMenu();
            this.inherited(arguments);
        }
    });
});
