define(['dojo/_base/declare','dijit/MenuItem', 'dojo/on', 'dojo/topic'], function(declare, MenuItem, on, topic) {
    return declare([MenuItem], {
        reportClass: undefined,
        postMixInProperties: function() {
            this.label = this.reportClass.reportTitle
        },
        postCreate: function() {
            this.inherited(arguments);
            var Report = this.reportClass;
            on(this.domNode, 'click', function() {
                window.open('/report/' + Report.reportUrl);
//                topic.publish('workspace.openWindow', {
//                    windowTitle: Report.reportTitle,
//                    contentClass: Report,
//                    initialStyle: {
//                        opacity: 1,
//                        top: '41px', //10 margin + 31 app bar
//                        left: '10px',
//                        width: (window.innerWidth - 30) + 'px',
//                        height: (window.innerHeight - 72).toString()+'px' //2x10 margins + 31 app bar + (16+5) window padding
//                    }
//                });
            });
        }
    });
});
