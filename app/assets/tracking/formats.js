define([
    'dojo/string',
    'dojo/date/locale',
    'dojo/i18n!./nls/resources'
], function(
    string,
    dateLocale,
    nls
) {
    var headings = [
        {limit: 22.5, name: nls.north},
        {limit: 67.5, name: nls.northeast},
        {limit: 112.5, name: nls.east},
        {limit: 157.5, name: nls.southeast},
        {limit: 202.5, name: nls.south},
        {limit: 247.5, name: nls.southwest},
        {limit: 292.5, name: nls.west},
        {limit: 337.5, name: nls.northwest},
        {limit: 360, name: nls.north}
    ];
    var formats = {
//        connection: function(connection) {
//            if (connection) {
//                if (connection.isGsmConnected) {
//                    if (connection.isGpsConnected) return nls.onlineState;
//                    else return nls.noGpsState;
//                } else return nls.offlineState;
//            } else return nls.offlineState;
//        },
        heading: function(position) {
            if (!position) return '---';
            return Math.round(position.heading || 0).toString();
        },
        headingString: function(position) {
            if (!position) return '---';
            for (var i=0; i<headings.length; i++) {
                if (headings[i].limit > position.heading) return headings[i].name;
            }
            return '---';
        },
        latitude: function(position) {
            if (position && position.coordinates) {
                var latitude = position.coordinates.latitude;
                return latitude  >= 0 ? (latitude.toFixed(6)  + ' N') : ((-latitude).toFixed(6)  + 'S');
            } else return '---';
        },
        longitude: function(position) {
            if (position && position.coordinates) {
                var longitude = position.coordinates.longitude;
                return longitude >= 0 ? (longitude.toFixed(6) + ' E') : ((-longitude).toFixed(6) + 'W');
            } else return '---';
        },
        altitude: function(position) {
            if (!(position && position.altitude)) return '---';
            else return '' + Math.round(position.altitude) + ' ' + nls.meters;
        },
        positionState: function(position) {
            if (!position) return '';
            if (position.speed > 0) return string.substitute(nls.movingStateTemplate, {
                speed: formats.speed(position), direction: formats.headingString(position)
            });
            else return nls.stoppedState;
        },
        speed: function(position) {
            if (!(position && position.speed)) return '---';
            return position.speed.toFixed(1).toString() + ' ' + nls.kmh;
        },
        date: function(value) {
            return dateLocale.format(value, {selector: 'date', formatLength: 'short'});
        },
        time: function(value) {
            return dateLocale.format(value, {selector: 'time', formatLength: 'medium'});
        },
        datetime: function(value) {
            return formats.date(value) + ' ' + formats.time(value);
        },
        duration: function(duration) {
            var seconds = Math.round(duration / 1000);
            var parts = ['' + seconds + ' ' + nls.seconds];
            var minutes = Math.floor(seconds / 60);
            if (minutes > 0) parts.unshift('' + minutes + ' ' + nls.minutes);
            var hours = Math.floor(minutes / 60);
            if (hours > 0) parts.unshift('' + hours + ' ' + nls.hours);
            var days = Math.floor(hours / 24);
            if (days > 0) parts.unshift('' + days + ' ' + nls.days);

            return parts.join(", ");
        }
    };
    return formats;
});
