define([
    'types/Map',
    'types/List',
    'types/Instant',
    'types/Float',
    './PositionState'
], function(
    Map,
    List,
    Instant,
    FloatType,
    PositionEvent
    ) {
    return List(Map({
            timestamp: Instant,
            position: PositionEvent,
            duration: FloatType
        }));
});
