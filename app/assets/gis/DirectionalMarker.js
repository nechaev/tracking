define(['./leaflet/leaflet', 'dojo/query', 'dojo/dom-style', 'dojo/dom-class', 'dojo/dom-construct'], function(
    leaflet, query, domStyle, domClass, domConstruct) {

    return leaflet.Marker.extend({
        directional: true,
        _initIcon: function () {
            leaflet.Marker.prototype._initIcon.apply(this, arguments);
            this._iconImg = query('img', this._icon)[0];
        },
        setDirection: function (direction) {
            if (direction && this.directional) {
                var rotationTransform = 'rotate(' + Math.round(direction).toString() + 'deg)';
                domStyle.set(this._iconImg, 'transform', rotationTransform);
                domStyle.set(this._iconImg, 'WebkitTransform', rotationTransform);
                domStyle.set(this._iconImg, 'MozTransform', rotationTransform);
                domStyle.set(this._iconImg, 'msTransform', rotationTransform);
            }
        },
        showLabel: function(labelText) {
            if (!this._labelDiv) this._labelDiv = domConstruct.create('div', {'class':'label'}, this._icon);
            else domClass.remove(this._labelDiv, 'hidden');
            if (labelText) this._labelDiv.innerHTML = labelText;
        },
        hideLabel: function() {
            if (this._labelDiv) domClass.add(this._labelDiv, 'hidden');
        }
    });
});
