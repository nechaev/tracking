define(['types/Map','types/Float'], function(MapType, FloatType) {
    return MapType({
        latitude: FloatType,
        longitude: FloatType
    });
});
