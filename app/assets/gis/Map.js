define(['dojo/_base/declare', 'dijit/_WidgetBase', './leaflet/leaflet', 'dojo/json', 'types/Map', 'types/Float'], function(
    declare, WidgetBase, leaflet, json, MapType, FloatType) {

    return declare(WidgetBase, {
        plugins: [],
        settingsKey: undefined,
        defaultSettings: {
            latitude: 0,
            longitude: 0,
            zoom: 0
        },
        zoomPosition: 'topright',
        buildRendering: function () {
            this.inherited(arguments);
            var settings = this.defaultSettings;
            var storageKey = 'gis.map.' + this.settingsKey;
            if (this.settingsKey) {
                var settingsStr = localStorage[storageKey];
                if (settingsStr) {
                    try {
                        settings = MapType({
                            latitude: FloatType,
                            longitude: FloatType,
                            zoom: FloatType
                        }, true).parseJSON(json.parse(settingsStr));
                    } catch (err) {
                        //Do nothing
                    }
                }
            }

            var map = this.map = new leaflet.Map(this.domNode, {
                attributionControl: false,
                zoomControl: false,
                center: [settings.latitude, settings.longitude],
                zoom: settings.zoom,
                layers: [new leaflet.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: '', //Map data © OpenStreetMap contributors',
                    maxZoom: 18
                })]
            });
            if (this.zoomPosition != 'none') map.addControl(new leaflet.Control.Zoom({position: this.zoomPosition}));
            this.plugins.forEach(function(plugin) {
                plugin.mapInit(map);
            });
            if (this.settingsKey) {
                map.on('move', function() {
                    var center = map.getCenter();
                    localStorage[storageKey] = json.stringify({
                        latitude: center.lat,
                        longitude: center.lng,
                        zoom: map.getZoom()
                    });
                });
            }
        },
        destroy: function() {
            this.plugins.forEach(function(plugin) {
                if (plugin.destroy) plugin.destroy();
            });
        }
    });
});
