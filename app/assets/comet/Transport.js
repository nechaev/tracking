define([
    'dojo/_base/declare',
    'dojo/Evented',
    'dojo/json'
], function(declare, Evented, json) {
    var heartbeatInterval = 60000;
    var pingTimeoutInterval = 5000;
    var Transport = declare([Evented], {
        checkPingTimeout : function() {
            return true
        },
        constructor: function() {
            var msgCounter = 0;
            var queue = [];
            var connected = false;
            var transport = this;

            this.errorCount = 0;

            var heartbeatTimer = undefined;

            function startHeartbeat() {
                heartbeatTimer = setInterval(function() {
                    transport.publish("connection.ping","");
                    startPingTimeout();
                }, heartbeatInterval);
            }
            function stopHeartbeat() {
                if (heartbeatTimer) {
                    clearInterval(heartbeatTimer);
                    heartbeatTimer = undefined;
                }
            }

            var pingTimeout = undefined;
            function startPingTimeout() {
                if (transport.checkPingTimeout()) pingTimeout = setTimeout(function() {
                    transport.reconnect();
                    pingTimeout = undefined;
                }, pingTimeoutInterval);
            }
            function stopPingTimeout() {
                if (pingTimeout) {
                    clearTimeout(pingTimeout);
                    pingTimeout = undefined;
                }
            }

            function onPingReceive() {
                stopPingTimeout();
                if (!transport.isConnected()) transport.onConnected();
            }

            this.onConnected = function() {
                connected = true;
                this.emit('connected', this.connectMethod());
                this.errorCount = 0;
                trySend();
                startHeartbeat();
            };
            this.onDisconnected = function() {
                connected = false;
                this.emit('disconnected');
                stopHeartbeat();
                stopPingTimeout();
            };
            this.isConnected = function() {
                return connected;
            };

            function trySend() {
                if (transport.isConnected()) {
                    queue.forEach(function(msg) {
                        transport.sendMessage(msg);
                    });
                    queue = [];
                }
            }

            this.publish = function(type, data, id) {
                var msgId = msgCounter.toString();
                queue.push(json.stringify({
                    id: id || msgId,
                    type: type,
                    body: data || ''
                }));
                msgCounter = msgCounter + 1;
                trySend();
                return msgId;
            };

            this.processMessage = function(msgText) {
                var data = json.parse(msgText);
                if (data.type == "connection.ping") onPingReceive();
                else this.emit('message', data);
            };
        },
        //to be implemented
        probe: function() {
            return true;
        },
        startConnection: undefined,
        sendMessage: undefined,
        connectMethod: undefined
    });
    Transport.cometUrl = function(protocol, portNumber) {
        var port = portNumber || location.port;
        port = (!port || port == "") ? '' : (':' + port);
        return (protocol || location.protocol) + '//' + location.hostname + port + '/comet/';
    };
    return Transport;
});
