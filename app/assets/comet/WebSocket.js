define([
    'dojo/_base/declare',
    './Transport',
    'tools/Deferred',
    'dojo/request/xhr'
], function(declare, Transport, Deferred, xhr) {

    var socketPort = undefined;
    var isHttps = location.protocol == 'https:';
    if (window.appConfig && appConfig.websocket) {
        if (isHttps) socketPort = appConfig.websocket.sslPort;
        else socketPort = appConfig.websocket.port;
    }
    var socketUrl = Transport.cometUrl(isHttps ? 'wss:' : 'ws:', socketPort) + 'websocket';
    var WebSocketTransport = declare([Transport], {
        connectMethod: function() {
            return 'websocket';
        },
        constructor : function() {
            this.inherited(arguments);
            var websocket = undefined;
            var transport = this;

            function connectWebSocket(connectionId) {
                websocket = new WebSocket(socketUrl + '?cid=' + connectionId);
                websocket.onmessage = function(event) {
                    transport.processMessage(event.data);
                };
                websocket.onopen = function() {
                    //self.setConnected(true);
                };
                websocket.onclose = function() {
                    if (!transport.isConnected()) transport.errorCount = transport.errorCount + 1;
                    transport.onDisconnected();
                    if (transport.errorCount > 5) alert("Connection failed");
                    else connectWebSocket(connectionId);
                };
            }

            this.startConnection = function() {
                xhr(Transport.cometUrl() + 'connection', {
                    method: 'GET'
                }).then(connectWebSocket);
            };
            this.sendMessage = function(msg) {
                websocket.send(msg);
            };
            this.reconnect = function() {
                websocket.close();
            };
        }
    });
    WebSocketTransport.probe = function() {
        var result = new Deferred();
        if (!window.WebSocket) result.resolve(false);
        else {
            var testSocket = new WebSocket(socketUrl + 'Ping');
            testSocket.onmessage = function(event) {
                if (event.data == "OK") result.resolve(true)
            };
            testSocket.onclose = function() {
                if (!result.isResolved()) result.resolve(false);
            };
            setInterval(function() {
                if (!result.isResolved()) result.resolve(false);
                testSocket.close();
            }, 5000);
        }
        return result;
    };
    return WebSocketTransport;
});
