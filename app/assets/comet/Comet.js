define([
    'dojo/_base/declare',
    'dojo/Evented',
    'tools/Deferred'
], function(
    declare,
    Evented,
    Deferred
    ) {

    return declare([Evented], {
        askTimeout: 10000, //ms
        messageHandlers: {},
        constructor: function() {
            var tempQueue = [];
            this.transport = {
                publish : function(type,data) {
                    var id = 'pre-' + tempQueue.length.toString();
                    tempQueue.push({
                        id: id,
                        type: type,
                        data: data
                    });
                    return id;
                }
            };

            this.loadTransport = function(transportCls) {
                var transport = this.transport = new transportCls;
                var comet = this;
                transport.on('connected', function(method) {
                    comet.emit('connected', method);
                });
                transport.on('disconnected', function() {
                    comet.emit('disconnected');
                });
                transport.on('message', function(data) {
                    comet.processMessage(data);
                });
                tempQueue.forEach(function(msg) {
                    transport.publish(msg.type, msg.data, msg.id);
                });
                tempQueue = undefined;
                transport.startConnection();
            }

        },
        processMessage: function(message) {
            var msgHandler = this.messageHandlers[message.id];
            if (msgHandler) msgHandler.handleMessage(message);
            else this.emit(message.type, message.body);
        },
        publish: function(type, data) {
            return this.transport.publish(type, data);
        },
        ask: function(type, data, timeout) {
            var msgId = this.publish(type, data);
            var result = new Deferred();
            var handlers = this.messageHandlers;
            var timeoutListener = setTimeout(function() {
                result.reject("Ask timeout");
                delete handlers[msgId];
            }, timeout || this.askTimeout);
            handlers[msgId] = {
                handleMessage: function(message) {
                    result.resolve(message.body);
                    delete handlers[msgId];
                    clearTimeout(timeoutListener);
                }
            };
            return result;
        },
        subscribe: function(type, data, listener) {
            var msgId = this.publish(type, data);
            var handlers = this.messageHandlers;
            handlers[msgId] = {
                handleMessage: function(message) {
                    listener(message.body);
                }
            };
            return {
                remove: function() {
                    delete handlers[msgId];
                }
            }
        }
    })
});