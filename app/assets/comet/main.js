define([
    'dojo/ready',
    './LongPolling',
    './WebSocket',
    './Comet'
], function(
    ready,
    LongPollingTransport,
    WebSocketTransport,
    Comet
    ) {
    var comet = new Comet;
    ready(function() {
        WebSocketTransport.probe().then(function(websocketAvailable) {
            if (websocketAvailable) comet.loadTransport(WebSocketTransport);
            else comet.loadTransport(LongPollingTransport);
        })
    });
    return comet;
});