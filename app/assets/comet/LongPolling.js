define([
    'dojo/_base/declare',
    'dojo/request/xhr',
    './Transport'
], function (declare, xhr, Transport) {
    var streamPollCheckTimeout = 1000;
    var disconnectTimeout = 1000;
    return declare([Transport], {
            streaming: navigator.appName != 'Microsoft Internet Explorer',
            checkPingTimeout: function() {
                return this.streaming;
            },
            connectMethod: function() {
                return this.streaming ? 'stream poll' : 'long poll';
            },
            constructor: function () {
                this.inherited(arguments);
                var transport = this;
                var xmlHttpRequest = undefined;
                var connectionId = undefined;
                function longpollUrl() {
                    return Transport.cometUrl() + 'longpoll' + (transport.streaming ? 'Stream':'')
                        + '?cid=' + connectionId + '&key=' + new Date().getTime().toString(16);
                }

                var offset = 0;
                function startLongPoll() {
                    offset = 0;
                    xmlHttpRequest = new XMLHttpRequest();
                    xmlHttpRequest.open('GET', longpollUrl(), true);
                    xmlHttpRequest.onreadystatechange = function () {
                        switch (xmlHttpRequest.readyState) {
                            case 3:
                                if (transport.streaming) {
                                    var msgsText = xmlHttpRequest.responseText.substr(offset);
                                    offset = offset + processMessages(msgsText);
                                }
                                break;
                            case 4:
                                if ((!transport.streaming) && xmlHttpRequest.status == "200") {
                                    processMessages(xmlHttpRequest.responseText);
                                }
                                if (!transport.disconnectTimeoutHandle) transport.disconnectTimeoutHandle = setTimeout(function() {
                                    if (transport.isConnected()) transport.onDisconnected();
                                });
                                if (xmlHttpRequest.status != "200") transport.errorCount = transport.errorCount + 1;
                                if (transport.errorCount > 5) alert("Connection failed");
                                else startLongPoll();
                                break;
                        }
                    };
                    xmlHttpRequest.send(null);
                    if (!(transport.streaming || transport.isConnected())) transport.onConnected();
                }

                function processMessages(msgsText) {
                    var processedLength = 0;
                    while (msgsText.length > 0) {
                        var braceCounter = undefined;
                        for (var i = 0; i < msgsText.length; i++) {
                            if (msgsText[i] == "{") braceCounter = braceCounter === undefined ? 1 : braceCounter + 1;
                            else if (msgsText[i] == "}") braceCounter = braceCounter - 1;
                            if (braceCounter === 0) break;
                        }
                        if (braceCounter === 0) {
                            var messageLength = i + 1;
                            transport.processMessage(msgsText.substr(0, messageLength));
                            msgsText = msgsText.substr(messageLength);
                            processedLength = processedLength + messageLength;
                        } else {
                            break;
                        }
                    }
                    return processedLength;
                }

                this.startConnection = function() {
                    var longPollTransport = this;
                    xhr(Transport.cometUrl() + 'connection', {
                        method: 'GET'
                    }).then(function (conn) {
                        connectionId = conn;
                        startLongPoll();
                        function streamPollCheck() {
                            if (!longPollTransport.isConnected()) {
                                longPollTransport.streaming = false;
                                longPollTransport.reconnect();
                            }
                        }
                        if (longPollTransport.streaming) setTimeout(streamPollCheck, streamPollCheckTimeout);
                    });
                };
                this.sendMessage = function(msg) {
                    xhr(longpollUrl(), {
                        method: 'POST',
                        data: msg,
                        headers: {'Content-Type': 'application/json'}
                    });
                };
                this.reconnect = function() {
                    xmlHttpRequest.abort();
                };
            }
        }
    )
});
