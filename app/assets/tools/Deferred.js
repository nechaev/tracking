define([
    'dojo/Deferred',
    'dojo/promise/Promise'
], function (Deferred, Promise) {
    Deferred.liftMethod = Promise.liftMethod = function (fn) {
        return function () {
            var args = arguments;
            var deferredResult = new Deferred();
            this.then(function (result) {
                deferredResult.resolve(fn.apply(result, args));
            }, function (error) {
                deferredResult.reject(error);
            });
            return deferredResult.promise;
        }
    };

    Deferred.prototype.map = Promise.prototype.map = function (fn) {
        var deferredResult = new Deferred();
        this.then(function (result) {
            deferredResult.resolve(fn(result));
        }, function (error) {
            deferredResult.reject(error);
        });
        return deferredResult.promise;
    };

    Deferred.prototype.flatMap = Promise.prototype.flatMap = function (fn) {
        var deferredResult = new Deferred();
        this.map(fn).then(function (result) {
            result.then(function (value) {
                deferredResult.resolve(value);
            }, function (err) {
                deferredResult.reject(err);
            })
        }, function (err) {
            deferredResult.reject(err);
        });
        return deferredResult.promise;
    };
    Deferred.p
    return Deferred;
});
