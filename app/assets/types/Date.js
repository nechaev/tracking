define(['dojo/date/stamp'], function(stamp) {
    var DateType = {
        parseJSON: function(json) {
            var value = stamp.fromISOString(json);
            value.toJSON = function() {
                return DateType.toJSON(this);
            }
        },
        toJSON: function(value) {
            return stamp.toISOString(value, {selector:'date'});
        }
    };
    return DateType;
});