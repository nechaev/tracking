define(function() {
    return {
        parseJSON: function(json) {
            var value = new Date(json);
            value.toJSON = function() {
                return this.getTime();
            };
            return value;
        }
    };
});
