define(function() {
    return function(properties, strict) {
        return {
            parseJSON: function(json) {
                var result = {};
                for(var property in properties) {
                    if (strict || json.hasOwnProperty(property)) result[property] = properties[property].parseJSON(json[property]);
                }
                return result;
            }
        };
    };
});
