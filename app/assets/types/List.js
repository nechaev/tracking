define(function() {
    return function(elementType) {
        return {
            parseJSON: function(json) {
                return json.map(elementType.parseJSON);
            }
        };
    };
});
