define(function() {
    return {
        parseJSON: function(json) {
            var value = +json;
            if (isNaN(value)) throw new Error("Float parse error");
            return value;
        }
    };
});
