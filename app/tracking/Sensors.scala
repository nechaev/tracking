package tracking

/**
 * Author: sgl
 * Date: 04.08.13
 */
object Sensors {
  case class Sensor(name: String)
  val IGNITION = Sensor("ignition")
  val FUEL = Sensor("fuel")
}
