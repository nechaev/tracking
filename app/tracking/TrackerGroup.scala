package tracking

/**
 * Created by: sgl
 * Date: 01.02.13
 */
case class TrackerGroup(
  id: String,
  name: String,
  trackers: List[TrackerId]
)
