package tracking.server

import akka.actor.Actor
import play.api.Configuration
import tracking.TrackingEvent
import tracking.web.{TrackerMessage, TrackingServerManager, TrackerStateRequest, TrackerEventBus}
import user.UserMessage

/**
 * Author: sgl
 * Date: 21.07.13
 */
class LocalConnector(conf: Configuration) extends Actor {

  override def preStart() {
    TrackingServerManager.route(conf.getString("domain").getOrElse(throw new IllegalArgumentException), self)
    TrackerServer() ! AddListener(self)
  }

  def receive = {
    case msg:TrackingEvent[Any] => TrackerEventBus.publish(msg)
    case msg:TrackerMessage => TrackerServer().tell(msg, sender)
  }

}
