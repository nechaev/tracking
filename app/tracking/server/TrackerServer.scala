package tracking.server

import akka.actor.{ActorRef, Actor}
import akka.event.Logging
import anorm.SqlParser._
import anorm._
import framework.{ActorDbTools, SingletonActorManager}
import java.util.Date
import org.joda.time.{Duration, Instant}
import play.api.Configuration
import play.api.Play.current
import play.api.db.DB
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps
import tracking._
import tracking.web._
import play.api.libs.concurrent.Akka
import gis.Coordinates
import tracking.web.TrackerDescriptionRequest
import tracking.web.TrackRequest
import tracking.web.TrackPoint
import tracking.Tracker
import anorm.~
import tracking.BoolState
import anorm.TypeDoesNotMatch
import tracking.PositionState
import tracking.TrackingEvent
import tracking.web.TrackerStateRequest
import anorm.MetaDataItem

/**
 * Author: sgl
 * Date: 28.08.13
 */
class TrackerServer(conf: Configuration) extends Actor with ActorDbTools {
  implicit private val log = Logging(context.system, self)

  //private val parkingSpeedThreshold = 0.1

  protected val dataSource = conf.getString("dataSource")
    .getOrElse(throw new RuntimeException("Data source isn't defined in configuration"))
  private var listeners: List[ActorRef] = Nil

  private val findKeySql = SQL("SELECT k.tracker, t.trackerId FROM trackerKeys k inner join trackers t on k.tracker = t.id WHERE trackerKey={trackerKey}")

  private val updatePositionSql = SQL(
    """INSERT INTO positionEvents(tracker, eventTime, latitude, longitude, altitude, heading, speed, accuracy)
      |VALUES({tracker}, {eventTime}, {latitude}, {longitude}, {altitude}, {heading}, {speed}, {accuracy})
    """.stripMargin)

  private val updateBoolEventSql = SQL(
    """INSERT INTO boolValuedEvents(tracker, eventTime, eventType, value)
      |VALUES({tracker}, {eventTime}, {eventType}, {value})
    """.stripMargin)

  private val updateFloatEventSql = SQL(
    """INSERT INTO floatValuedEvents(tracker, eventTime, eventType, value)
      |VALUES({tracker}, {eventTime}, {eventType}, {value})
    """.stripMargin)

  private val selectPositionStateSql = SQL(
    """SELECT p.eventTime, p.latitude, p.longitude, p.altitude, p.heading, p.speed, p.accuracy
      |FROM positionEvents p
      |INNER JOIN (
      | SELECT pd.tracker, MAX(pd.eventTime) eventTime FROM positionEvents pd
      | WHERE pd.tracker = (SELECT id FROM trackers WHERE trackerId = {trackerId})
      | GROUP BY pd.tracker
      |) pt ON pt.tracker = p.tracker AND pt.eventtime = p.eventtime
    """.stripMargin)

  private def selectStateSql(tableName: String) =
    s"""SELECT e.eventTime, e.eventType, e.value
      |FROM $tableName e
      |INNER JOIN (
      |SELECT ed.tracker, ed.eventType, MAX(ed.eventTime) eventTime FROM $tableName ed
      | WHERE ed.tracker = (SELECT id FROM trackers WHERE trackerId = {trackerId})
      | GROUP BY ed.tracker, ed.eventType
      |) pt ON pt.tracker = e.tracker AND pt.eventType = e.eventType AND pt.eventTime = e.eventTime
    """.stripMargin

  private val selectBoolStateSql = SQL(selectStateSql("boolValuedEvents"))
  private val selectFloatStateSql = SQL(selectStateSql("floatValuedEvents"))

  private val trackerDescriptionSql = SQL("SELECT name, trackerType FROM trackers WHERE trackerId={trackerId}")

  private val trackSql = SQL(
    """SELECT eventTime, latitude, longitude, altitude, speed, heading, accuracy,
      |EXTRACT(MILLISECONDS FROM duration) AS duration
      |FROM positionEvents
      |WHERE tracker = (SELECT id FROM trackers WHERE trackerId = {trackerId})
      |AND eventTime BETWEEN {start} AND {end}
      |ORDER BY eventTime
    """.stripMargin)

  private val addPacketInfoSql = SQL(
    """INSERT INTO receivedPackets(receivedTime, fromTime, toTime, eventCount, tracker)
      |VALUES({receivedTime}, {fromTime}, {toTime}, {eventCount}, {tracker})
    """.stripMargin)

  private val runAggregateCalculationSql = SQL("SELECT doPendingAggregateCalculations()")

  private var isAggregateCalculationPending = false

  import context.dispatcher

  implicit def rowToFloat: Column[Float] = Column.nonNull {
    (value, meta) =>
      val MetaDataItem(qualified, _, _) = meta
      value match {
        case d: Float => Right(d)
        case _ => Left(TypeDoesNotMatch("Cannot convert " + value + ":" + value.asInstanceOf[AnyRef].getClass + " to Float for column " + qualified))
      }
  }

  private def onNewEvent(event: TrackingEvent[_], triggerAggregateCalculation: Boolean = true) {
    listeners.foreach(_ ! event)
    if (triggerAggregateCalculation && !isAggregateCalculationPending) {
      Akka.system.scheduler.scheduleOnce(30 seconds, self, RunAggregateCalculation)
      isAggregateCalculationPending = true
    }
  }

  def receive = {
    case AddListener(listener) => listeners = listener :: listeners
    case TrackerDataPacket(trackerKey, events, receivedTime) => asyncDbOp { implicit conn =>
      findKeySql.on("trackerKey" -> trackerKey).as(int("tracker") ~ str("trackerId") singleOpt).foreach({
        case tracker ~ tId => {
          val trackerId = TrackingServerManager.parseTrackerId(tId)
          val (locEvents, boolEvents, floatEvents, tops) = events
            .sortBy(_.timestamp.getMillis)
            .foldLeft(
              (List[TrackerLocationData](), List[TrackerBoolEventData](), List[TrackerFloatEventData](), Map[(Class[_], Int), TrackerEventData]())
            )((acc, event) => {
              val (locs, bools, floats, tops) = acc
              def addTopEvent(topKey: (Class[_], Int)) = tops +
                (topKey -> tops.get(topKey).map(topEvent => if (event.timestamp.getMillis > topEvent.timestamp.getMillis) event else topEvent).getOrElse(event))
              event match {
                case loc: TrackerLocationData => (loc :: locs, bools, floats, addTopEvent((classOf[TrackerLocationData], 0)))
                case bool: TrackerBoolEventData => (locs, bool :: bools, floats, addTopEvent(classOf[TrackerBoolEventData], bool.eventType))
                case float: TrackerFloatEventData => (locs, bools, float :: floats, addTopEvent(classOf[TrackerFloatEventData], float.eventType))
                case _ => acc
              }
            })

          tops.values.foreach({
            case TrackerLocationData(time, lat, lon, altitude, heading, speed, accuracy) => onNewEvent(
              TrackingEvent(time, trackerId, PositionState(Coordinates(lat, lon), altitude, accuracy, speed, heading)))
            case TrackerBoolEventData(time, eventType, value) => onNewEvent(TrackingEvent(time, trackerId, BoolState(eventType, value)))
            case TrackerFloatEventData(time, eventType, value) => onNewEvent(TrackingEvent(time, trackerId, FloatState(eventType, value)))
            case TrackerPingData(_) => {}
          })

          if (!locEvents.isEmpty) updatePositionSql.asBatch.addBatchList(locEvents.map({
            case TrackerLocationData(time, lat, lon, altitude, heading, speed, accuracy) => List[(String, ParameterValue[_])](
              "tracker" -> tracker, "eventTime" -> time.toDate,
              "latitude" -> lat, "longitude" -> lon, "altitude" -> altitude, "heading" -> heading,
              "speed" -> speed, "accuracy" -> accuracy
            )
          })).execute()
          if (!boolEvents.isEmpty) updateBoolEventSql.asBatch.addBatchList(boolEvents.map({
            case TrackerBoolEventData(time, eventType, value) => List[(String, ParameterValue[_])](
              "tracker" -> tracker, "eventTime" -> time.toDate, "eventType" -> eventType, "value" -> value
            )
          })).execute()
          if (!floatEvents.isEmpty) updateFloatEventSql.asBatch.addBatchList(floatEvents.map({
            case TrackerFloatEventData(time, eventType, value) => List[(String, ParameterValue[_])](
              "tracker" -> tracker, "eventTime" -> time.toDate, "eventType" -> eventType, "value" -> value
            )
          })).execute()
          if (locEvents.isEmpty && boolEvents.isEmpty && floatEvents.isEmpty) events.headOption.foreach(pingEvent => onNewEvent(
            event = TrackingEvent(pingEvent.timestamp, trackerId, PingEvent),
            triggerAggregateCalculation = false
          ))

          if (events.length > 0)  {
            val (fromTime, toTime) = events
              .map(_.timestamp.getMillis)
              .foldLeft((Long.MaxValue, 0L))((acc, ts) => (math.min(acc._1, ts), math.max(acc._2, ts)))
            addPacketInfoSql.on("receivedTime" -> receivedTime.toDate,
              "fromTime" -> new Date(fromTime),
              "toTime" -> new Date(toTime),
              "eventCount" -> events.length,
              "tracker" -> tracker).execute()
          }

        }
      })

    }

    case TrackerStateRequest(trackerId) => {
      log.debug("Querying state for {}", trackerId)
      asyncReplyDbOp {
        implicit connection =>
          selectPositionStateSql.on("trackerId" -> trackerId.toString).as(
            get[Date]("eventTime") ~ get[Double]("latitude") ~ get[Double]("longitude")
              ~ get[Option[Float]]("altitude") ~ get[Option[Float]]("accuracy")
              ~ get[Option[Float]]("speed") ~ get[Option[Float]]("heading") *).map({
            case eventTime ~ latitude ~ longitude ~ altitude ~ accuracy ~ speed ~ heading =>
              TrackingEvent(new Instant(eventTime), trackerId, PositionState(
                Coordinates(latitude, longitude), altitude.map(_.toDouble),
                accuracy.map(_.toDouble), speed.map(_.toDouble), heading.map(_.toDouble)
              ))
          }).toList ++
          selectBoolStateSql.on("trackerId" -> trackerId.toString).as(
            get[Date]("eventTime") ~ int("eventType") ~ bool("value") *).map({
            case eventTime ~ eventType ~ value => TrackingEvent(new Instant(eventTime), trackerId, BoolState(eventType, value))
          }).toList ++
          selectFloatStateSql.on("trackerId" -> trackerId.toString).as(
            get[Date]("eventTime") ~ int("eventType") ~ get[Double]("value") *).map({
            case eventTime ~ eventType ~ value => TrackingEvent(new Instant(eventTime), trackerId, FloatState(eventType, value))
          }).toList
      }
    }
    case TrackerDescriptionRequest(trackerId) => asyncReplyDbOp {
      implicit conn =>
        log.debug("Querying description for {}", trackerId)
        trackerDescriptionSql.on("trackerId" -> trackerId.toString)
          .as((str("name") ~ get[Option[String]]("trackerType") single)
          .map({
          case name ~ trackerType => Tracker(trackerId, name, trackerType)
        })
        )
    }
    case TrackRequest(trackerId, start, end) => asyncReplyDbOp {
      implicit conn =>
        trackSql.on("trackerId" -> trackerId.toString, "start" -> start.toDate, "end" -> end.toDate)
          .as(get[Date]("eventTime") ~ get[Double]("latitude") ~ get[Double]("longitude")
          ~ get[Option[Float]]("altitude") ~ get[Option[Float]]("speed") ~ get[Option[Float]]("heading")
          ~ get[Option[Float]]("accuracy") ~ get[Option[Double]]("duration") *)
          .map({
          case timestamp ~ lat ~ lon ~ alt ~ speed ~ heading ~ accuracy ~ duration => TrackPoint(new Instant(timestamp),
            PositionState(Coordinates(lat, lon), alt.map(_.toDouble), accuracy.map(_.toDouble),
              speed.map(_.toDouble), heading.map(_.toDouble)),
            new Duration(duration.getOrElse(0.0).longValue))
        })
    }
    case RunAggregateCalculation => Future {
      DB.withConnection(dataSource) {
        implicit connection =>
          try {
            runAggregateCalculationSql.execute()
          } catch {
            case e: Throwable => log.error(e, "Database operation error")
          } finally {
            self ! AggregateCalculationFinished
          }
      }
    }
    case AggregateCalculationFinished => {
      isAggregateCalculationPending = false
    }
  }
}

object TrackerServer extends SingletonActorManager[TrackerServer]

case object RunAggregateCalculation

case object AggregateCalculationFinished