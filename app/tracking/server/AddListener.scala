package tracking.server

import akka.actor.ActorRef

/**
 * User: vnechaev
 * Date: 30.08.13
 */
case class AddListener(listener: ActorRef)
