package tracking.server

import akka.actor.{Status, Actor}
import anorm.SqlParser._
import anorm._
import java.sql.Connection
import framework.{ActorTools, SingletonActorManager}
import framework.Defaults.timeout
import play.api.db.DB
import user._
import play.api.{Configuration, Play}
import play.api.Play.current
import scala.language.postfixOps
import akka.event.Logging
import play.api.libs.concurrent.Akka
import play.api.libs.openid.UserInfo
import user.UserMessage
import scala.Some
import user.Login
import user.UserId

/**
 * Author: sgl
 * Date: 31.08.13
 */
class UserDirectory(conf: Configuration) extends Actor with ActorTools {
  implicit private val log = Logging(context.system, this)

  protected val dataSourceName = conf.getString("dataSource")
    .getOrElse(throw new RuntimeException("Data source name for User directory not defined"))
  protected val openIdDomain = conf.getString("OpenIDdomain")

  private val loginSql = SQL(
    """SELECT COUNT(*) FROM userCredentials c INNER JOIN users u ON c.user = u.id
      |WHERE u.domain = {domain} AND u.userId = {userId} AND c.credential = {credential}
    """.stripMargin)

  private val findOpenIdUserSql = SQL(
    """SELECT userId
      |FROM users u
      |WHERE userId = {userId} AND domain = {domain}
    """.stripMargin)
  private val addUserSql = SQL(
    """INSERT INTO users(userId, name, domain, email, country, lang)
      |VALUES({userId}, {name}, {domain}, {email}, {country}, {lang})
    """.stripMargin)

  override def preStart() {
    DB.withConnection(dataSourceName) { implicit connection =>
      SQL("SELECT id FROM domains").as(scalar[String] *).foreach(UserManager.route(_, self))
    }
    if (openIdDomain != None) UserManager.openIdDomain = Some(context.actorSelection(self.path))
  }

  def receive = {
    case UserMessage(userId, Login(credential)) => asyncReply { logThrows { DB.withConnection(dataSourceName) { implicit connection =>
      loginSql.on("domain" -> userId.domain, "userId" -> userId.userId, "credential" -> credential).as(scalar[Long].single) > 0
    }}}
    case userInfo: UserInfo => openIdDomain match {
      case Some(domain) => asyncReply { logThrows { DB.withConnection(dataSourceName) { implicit connection =>
        val userId = userInfo.attributes("email")
        findOpenIdUserSql.on("userId" -> userId, "domain" -> domain).as(str("userId") singleOpt) match {
          case Some(userid) => UserId(userid, domain)
          case None => {
            log.info("Adding user")
            val userName = buildUserName(userInfo.attributes.get("firstname"),userInfo.attributes.get("lastname"))
            addNewUser(User(
              userId = UserId(userId, domain),
              name = if (userName.isEmpty) userInfo.id else userName,
              email = userInfo.attributes.get("email"),
              country = userInfo.attributes.get("country"),
              language = userInfo.attributes.get("language")
            ))
          }
        }
      } } }
      case None => sender ! Status.Failure(new RuntimeException("OpenID domain isn't enabled"))
    }

  }

  private def buildUserName(fname: Option[String], lname: Option[String]) = (for {
    f <- fname
    l <- lname
  } yield f + " " + l).getOrElse(fname.getOrElse("") + lname.getOrElse(""))

  protected def addNewUser(user: User)(implicit connection:Connection): UserId = {
    addUserSql.on(
      "userId" -> user.userId.userId,
      "name" -> user.name,
      "domain" -> user.userId.domain,
      "email" -> user.email,
      "country" -> user.country,
      "lang" -> user.language
    ).executeUpdate()
    user.userId
  }

}

object UserDirectory extends SingletonActorManager[TrackingDirectoryExtension]
