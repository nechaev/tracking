package tracking.server

import akka.actor.Actor
import anorm.SqlParser._
import anorm._
import java.sql.Connection
import gis.{GeocodingServiceManager, GeocodingServiceRequest}
import play.api.Play.current
import play.api.db.DB
import reporting.{ReportingContextRequest, ReportingContext}
import scala.concurrent.Future
import scala.language.postfixOps
import tracking.TrackerGroup
import tracking.web.{TrackingServerManager, TrackingUserContextRequest, TrackingUserContext}
import user.{UserId, User, UserMessage}
import play.api.Configuration
import akka.event.Logging

/**
 * Author: sgl
 * Date: 03.09.13
 */
class TrackingDirectoryExtension(conf: Configuration) extends UserDirectory(conf) {
  implicit private val log = Logging(context.system, this)

  private val reportingDataSourceSql = SQL("SELECT reportingDataSource FROM domains WHERE id = {domain}")
  private val geocodingServiceSql = SQL("SELECT geocodingServiceName FROM domains WHERE id = {domain}")
  private val groupsSql = SQL(
    """SELECT g.id, g.name, gt.trackerId
      |FROM trackerGroups g INNER JOIN trackerGroupTrackers gt ON gt.group = g.id
      |WHERE EXISTS(
      |  SELECT * FROM userTrackerGroups utg
      |  INNER JOIN users u ON utg.user = u.id
      |  WHERE utg.group = g.id AND u.userId = {userId} AND u.domain = {domain}
      |)
    """.stripMargin)

  private def trackingHandler: Actor.Receive = {
    case UserMessage(userId, TrackingUserContextRequest) => asyncReply { logThrows { DB.withConnection(dataSourceName) { implicit connection =>
      log.debug("Querying tracking context for {}", userId)
      val ctx = TrackingUserContext(groupsSql.on("userId" -> userId.userId, "domain" -> userId.domain).as(int("id") ~ str("name") ~ str("trackerId") *).groupBy({
        case groupId ~ groupName ~ _ => (groupId, groupName)
      }).toList.map({
        case ((groupId, groupName), members) => TrackerGroup(groupId.toString, groupName, members map {
          case _ ~ _ ~ trackerId => TrackingServerManager.parseTrackerId(trackerId)
        })
      }))
      log.debug("Returning {} groups", ctx.groups.length)
      ctx
    } } }
    case UserMessage(userId, ReportingContextRequest) => asyncReply { logThrows { DB.withConnection(dataSourceName) { implicit connection =>
      log.debug("Querying reporting context for {}", userId)
      ReportingContext(userId, reportingDataSourceSql.on("domain" -> userId.domain)
        .as(scalar[String].single))
    } } }
    case UserMessage(userId, GeocodingServiceRequest) => asyncReply { logThrows { DB.withConnection(dataSourceName) { implicit connection =>
      log.debug("Querying geocoding service for {}", userId)
      geocodingServiceSql.on("domain" -> userId.domain).as(scalar[String].singleOpt)
        .flatMap(serviceName => GeocodingServiceManager.get(serviceName) )
    } } }
  }

  override def receive = trackingHandler orElse super.receive

  /*
  override protected def addNewUser(user: User)(implicit connection: Connection): UserId = {
    val userId = super.addNewUser(user)
    val id = findUserSql.on("userId" -> userId.userId, "domain" -> userId.domain).as(long("id") single)
    val groupId = addUserGroupSql.on("owner" -> id, "name" -> "*").as(scalar[Long].single)
    addVisibleGroupSql.on("user" -> id, "group" -> groupId).executeUpdate()
    userId
  } */

}
