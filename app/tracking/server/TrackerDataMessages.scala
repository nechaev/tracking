package tracking.server

import framework.JsonFormats._
import org.joda.time.Instant
import play.api.libs.functional.syntax._
import play.api.libs.json._

/**
 * Author: sgl
 * Date: 21.07.13
 */
sealed trait TrackerEventData {
  def timestamp: Instant
}

case class TrackerPingData (
  timestamp: Instant
) extends TrackerEventData

case class TrackerLocationData (
  timestamp: Instant,
  latitude: Double,
  longitude: Double,
  altitude: Option[Double],
  heading: Option[Double],
  speed: Option[Double],
  accuracy: Option[Double]
) extends TrackerEventData

case class TrackerBoolEventData (
  timestamp: Instant,
  eventType: Int,
  value: Boolean
) extends TrackerEventData

case class TrackerFloatEventData (
  timestamp: Instant,
  eventType: Int,
  value: Double
) extends TrackerEventData

case class TrackerDataPacket(
  trackerKey: String,
  events: List[TrackerEventData],
  receivedTime: Instant
)

object TrackerDataJsonFormats {
  implicit val locationEventReads = (
    (__ \ "t").read[Instant] ~
    (__ \ "la").read[Double] ~
    (__ \ "lo").read[Double] ~
    (__ \ "al").readNullable[Double] ~
    (__ \ "h").readNullable[Double] ~
    (__ \ "s").readNullable[Double] ~
    (__ \ "ac").readNullable[Double]
  )(TrackerLocationData)

  implicit val boolEventReads = (
    (__ \ "t").read[Instant] ~
    (__ \ "et").read[Int] ~
    (__ \ "bv").read[Boolean]
  )(TrackerBoolEventData)

  implicit val floatEventReads = (
    (__ \ "t").read[Instant] ~
    (__ \ "et").read[Int] ~
    (__ \ "fv").read[Double]
    )(TrackerFloatEventData)

  implicit val pingEventReads = (__ \ "t").read[Instant] fmap TrackerPingData

  implicit val trackerEventDataReads = new Reads[TrackerEventData] {
    def reads(jsEvent: JsValue): JsResult[TrackerEventData] = jsEvent match {
      case obj:JsObject if obj.keys.contains("la") && obj.keys.contains("lo") => jsEvent.validate[TrackerLocationData]
      case obj:JsObject if obj.keys.contains("bv") => jsEvent.validate[TrackerBoolEventData]
      case obj:JsObject if obj.keys.contains("fv") => jsEvent.validate[TrackerFloatEventData]
      case obj:JsObject => jsEvent.validate[TrackerPingData]
      case _ => JsError("Unknown message")
    }
  }

}
