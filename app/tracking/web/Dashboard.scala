package tracking.web

import akka.actor.{ActorSelection, Actor}
import akka.pattern.{ask, pipe}
import anorm.SqlParser._
import anorm._
import framework.{SingletonActorManager, ActorTools}
import framework.Defaults.timeout
import gis.{GeocodingServiceRequest, GenerateAddressEvents}
import java.math.BigDecimal
import org.joda.time.{Duration, LocalDate}
import play.api.db.DB
import play.api.Play.current
import reporting.{ReportingContextRequest, ReportingContext}
import scala.collection.mutable
import scala.concurrent.Future
import scala.language.postfixOps
import session.SessionForwardable
import tracking._
import user.{UserId, UserManager, UserMessage}
import akka.event.Logging

/**
 * Author: sgl
 * Date: 08.09.13
 */
class Dashboard extends Actor with ActorTools {
  implicit val log = Logging(context.system, this)
  val dashboardQuerySql = SQL(
    """SELECT
      | T.trackerId,
      | T.name trackerName,
      | T.groupName,
      | SUM(D.mileage) mileage,
      | SUM(D.engineHours) engineHours,
      | SUM(D.gpsLostTime) gpsLostTime,
      | SUM(D.parkingTime) parkingTime
      |FROM TRACKING_V_USERTRACKERS T
      |LEFT JOIN TRACKING_V_DASHBOARD D ON T.id=D.tracker AND D.eventTime BETWEEN {dayStart} AND {dayEnd}
      |WHERE T.userId={userId} AND T.userdomain={userDomain}
      |GROUP BY T.trackerId, T.name, T.groupName
    """.stripMargin)

  import context.dispatcher

  def receive = {
    case UserMessage(userId, DashboardRequest) => {
      (for {
        reportingContext <- (UserManager() ? UserMessage(userId, ReportingContextRequest)).mapTo[ReportingContext]
        geocodingService <- (UserManager() ? UserMessage(userId, GeocodingServiceRequest)).mapTo[Option[ActorSelection]]
        dashboardInfo <- collectDashboardInfo(userId, reportingContext, geocodingService)
      } yield {
        log.debug("Dashboard entries: {}", dashboardInfo)
        dashboardInfo
      }) pipeTo sender
    }
  }

  def collectDashboardInfo(userId: UserId, reportingContext: ReportingContext, geocodingService: Option[ActorSelection]): Future[List[DashboardEntry]] = {
    val ReportingContext(UserId(userId, userDomain), dataSource) = reportingContext
    DB.withConnection(dataSource) { implicit conn => {
        val currentDate = LocalDate.now()
        val dayStart = currentDate.toDate
        val dayEnd = currentDate.plusDays(1).toDate
        log.debug("Querying dashboard info for {} from {} to {}, using geocoding service {}",
          userId + "@" + userDomain, dayStart, dayEnd, geocodingService)
        val queryResult = dashboardQuerySql
          .on("userId" -> userId, "userDomain" -> userDomain, "dayStart" -> dayStart, "dayEnd" -> dayEnd)
          .as(str("trackerId")
          ~ str("trackerName")
          ~ get[String]("groupName")
          ~ get[Option[BigDecimal]]("mileage")
          ~ get[Option[BigDecimal]]("engineHours")
          ~ get[Option[BigDecimal]]("gpsLostTime")
          ~ get[Option[BigDecimal]]("parkingTime") *
        ).map({
          case trackerId ~ trackerName ~ groupName ~ mileage ~ engineHours ~ gpsLostTime ~ parkingTime => DashboardEntry(
            trackerId = TrackingServerManager.parseTrackerId(trackerId),
            name = trackerName,
            groupName = groupName,
            positionState = None,
            address = "",
            dailyMileage = mileage.map(_.doubleValue()),
            dailyEngineHours = engineHours.map(_.longValue()).map(new Duration(_)),
            dailyGpsLostTime = gpsLostTime.map(_.longValue()).map(new Duration(_)),
            dailyParkingTime = parkingTime.map(_.longValue()).map(new Duration(_))
          )
        }).toList
        val entryMap = Map(queryResult map (entry => entry.trackerId -> entry) : _*)
        log.debug("Got {} rows of dashboard data", queryResult.length)
        for {
          stateEvents <- Future.sequence(
            queryResult.map(_.trackerId).map(trackerId => (TrackingServerManager() ? TrackerStateRequest(trackerId)).mapTo[List[TrackingEvent[_]]])
          ).map(_.flatten)
          addressEvents <- geocodingService.map(gs => (gs ? GenerateAddressEvents(stateEvents)).mapTo[List[TrackingEvent[_]]]).getOrElse(Future.successful(Nil))
        } yield {
          (stateEvents ++ addressEvents).foldLeft(entryMap)((acc, evt) => {
            val TrackingEvent(_, trackerId, stateEvent) = evt
            stateEvent match {
              case positionState: PositionState => acc + (trackerId -> acc(trackerId).copy(positionState = Some(positionState)))
              case AddressState(address) => acc + (trackerId -> acc(trackerId).copy(address = address))
              case _ => acc
            }
          }).values.toList
        }
      }
    }
  }
}

object Dashboard extends SingletonActorManager[Dashboard]

case object DashboardRequest extends SessionForwardable {
  def forwardTo = Dashboard()
}