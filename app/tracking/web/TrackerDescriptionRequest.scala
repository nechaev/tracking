package tracking.web

import tracking.TrackerId

/**
 * User: vnechaev
 * Date: 18.09.13
 */
case class TrackerDescriptionRequest(trackerId: TrackerId) extends TrackerMessage
