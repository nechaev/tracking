package tracking.web

import akka.actor.{ActorSelection, Actor}
import akka.event.Logging
import anorm.SqlParser._
import anorm._
import framework.{ActorDbTools, SingletonActorManager}
import java.security.MessageDigest
import java.sql.Connection
import java.util.Date
import javax.xml.bind.DatatypeConverter
import play.api.Configuration
import scala.language.postfixOps
import session.SessionForwardable
import tracking.{TrackerGroup, TrackerId, Tracker}
import user.{UserId, UserMessage}

/**
 * User: vnechaev
 * Date: 25.10.13
 */
class AdminDao(conf: Configuration) extends Actor with ActorDbTools {
  implicit val log = Logging(context.system, this)

  protected val dataSource = conf.getString("dataSource").getOrElse(throw new IllegalArgumentException("dataSource parameter isn't defined in configuration"))
  private val trackerDomain = conf.getString("trackerDomain").getOrElse(throw new IllegalArgumentException("trackerDomain parameter isn't defined in configuration"))

  private val selectUserTrackers = """SELECT trackerId, name, trackerType FROM trackers
                                     |WHERE owner = (SELECT id FROM users WHERE domain = {userDomain} AND userId = {userId})
                                   """.stripMargin

  private val selectUserTrackersSql = SQL(selectUserTrackers)
  private val selectUserTrackerSql = SQL(selectUserTrackers + " AND trackerId = {trackerId}")

  private val selectTrackerKeySql = SQL(
    """SELECT k.trackerKey
      |FROM trackerKeys k 
      |WHERE k.tracker = (SELECT t.id FROM trackers t WHERE t.trackerId = {trackerId})
    """.stripMargin)
  private val deleteKeySql = SQL(
    """DELETE FROM trackerKeys
      |WHERE tracker = (
      | SELECT t.id FROM trackers t
      | INNER JOIN users u ON t.owner = u.id
      | WHERE t.trackerId = {trackerId}
      | AND u.userId = {userId} AND u.domain = {userDomain}
      |)
    """.stripMargin)
  private val insertKeySql = SQL(
    """INSERT INTO trackerKeys(tracker, trackerKey)
      |SELECT t.id, {key} FROM trackers t
      |INNER JOIN users u ON t.owner = u.id
      |WHERE trackerId = {trackerId}
      |AND u.userId = {userId} AND u.domain = {userDomain}
    """.stripMargin)
  private val findKeySql = SQL("SELECT tracker FROM trackerKeys WHERE trackerKey = {key}")
  private val createNewTrackerSql = SQL(
    """INSERT INTO trackers(name, trackerType, trackerId, owner)
      |SELECT {name}, {trackerType}, CAST(u.id AS VARCHAR) || '-' || CAST(COALESCE(t.trackerCount, 0) + 1 AS VARCHAR) || {domainPart}, u.id
      |FROM users u
      |LEFT JOIN (SELECT owner, COUNT(1) trackerCount FROM trackers GROUP BY owner) t ON t.owner = u.id
      |WHERE u.userId = {userId} AND u.domain = {userDomain}
      |RETURNING trackerId
    """.stripMargin)
  private val updateTrackerSql = SQL(
    """UPDATE trackers
      |SET name = {name}, trackerType = {trackerType}
      |WHERE trackerId = {trackerId}
      |AND owner = (SELECT id FROM users WHERE userId = {userId} AND domain = {userDomain})
    """.stripMargin)
  private val getUserGroups =
    """SELECT g.id, g.name, gt.trackerId FROM trackerGroups g LEFT JOIN trackerGroupTrackers gt ON g.id = gt.group
      |WHERE owner = (SELECT id FROM users WHERE userId = {userId} AND domain = {userDomain})
    """.stripMargin
  private val getUserGroupsSql = SQL(getUserGroups)
  private val getUserGroupSql = SQL(getUserGroups + " AND g.id = {groupId}")
  private val addUserGroupSql = SQL(
    """INSERT INTO trackerGroups(owner,name)
      |SELECT id, {name} FROM users WHERE userId = {userId} AND domain = {userDomain}
      |RETURNING id
    """.stripMargin)
  private val updateUserGroupSql = SQL(
    """UPDATE trackerGroups SET name = {name}
      |WHERE id = {groupId} AND owner = (SELECT id FROM users WHERE userId = {userId} AND domain = {userDomain})
    """.stripMargin)
  private val addTrackerToGroupSql = SQL(
    """INSERT INTO trackerGroupTrackers("group", trackerId)
      |SELECT g.id, t.trackerId
      |FROM trackerGroups g INNER JOIN trackers t ON g.owner = t.owner
      |WHERE g.id = {groupId} AND t.trackerId = {trackerId}
      |AND g.owner = (SELECT id FROM users WHERE userId = {userId} AND domain = {userDomain})
    """.stripMargin)
  private val removeTrackerFromGroupSql = SQL(
    """DELETE FROM trackerGroupTrackers
      |WHERE "group" = (SELECT id FROM trackerGroups
      | WHERE id = {groupId}
      | AND owner = (SELECT id FROM users WHERE userId = {userId} AND domain = {userDomain})
      |) AND trackerId = {trackerId}
    """.stripMargin)
  private val addVisibleGroupSql = SQL(
    """INSERT INTO userTrackerGroups("user", "group")
      |SELECT u.id, g.id FROM users u
      |INNER JOIN trackerGroups g ON g.owner = u.id
      |WHERE g.id = {group}
      |AND u.userId = {userId} AND u.domain = {userDomain}
    """.stripMargin)
  private val removeVisibleGroup = SQL(
    """DELETE FROM userTrackerGroups
      |WHERE "user"=(SELECT id FROM users WHERE userId = {userId} AND domain = {userDomain}
      |AND group=(SELECT g.id FROM groups g INNER JOIN users u ON u.id=g.owner WHERE u.userId = {userId} AND u.domain = {userDomain})
    """.stripMargin)

  import AdminDao._

  def receive = {
    case UserMessage(userId, GetTrackers) => asyncReplyDbOp { implicit conn =>
      log.debug("Requesting trackers for {}", userId)
      selectUserTrackersSql.on(
        "userId" -> userId.userId,
        "userDomain" -> userId.domain
      ).as(str("trackerId") ~ str("name") ~ get[Option[String]]("trackerType") *)
        .map({
          case id ~ name ~ ttype => Tracker(TrackingServerManager.parseTrackerId(id), name, ttype)
        }).toList
    }
    case UserMessage(userId, GetTracker(id)) => asyncReplyDbOp { implicit conn =>
      log.debug("Requesting tracker {} for {}", id, userId)
      selectUserTrackerSql.on(
        "userId" -> userId.userId,
        "userDomain" -> userId.domain,
        "trackerId" -> id.toString
      ).as(str("trackerId") ~ str("name") ~ get[Option[String]]("trackerType") singleOpt)
      .map({
        case trackerId ~ name ~ ttype => Tracker(TrackingServerManager.parseTrackerId(trackerId), name, ttype)
      })
    }
    case UserMessage(userId, Create(name, trackerType)) => asyncReplyDbOp { implicit conn =>
      log.debug("Creating new tracker")
      Tracker(
        id = TrackingServerManager.parseTrackerId(
          createNewTrackerSql.on(
            "name" -> name,
            "trackerType" -> trackerType,
            "userId" -> userId.userId,
            "userDomain" -> userId.domain,
            "domainPart" -> ("@" + trackerDomain))
            .as(scalar[String].single)
        ),
        name = name,
        trackerType = trackerType
      )

    }
    case UserMessage(userId, Update(tracker@Tracker(id, name, trackerType))) => asyncReplyDbOp { implicit conn =>
      log.debug("Updating tracker {}", id)
      updateTrackerSql.on(
        "trackerId" -> id.toString,
        "name" -> name,
        "userId" -> userId.userId,
        "userDomain" -> userId.domain,
        "trackerType" -> trackerType
      ).executeUpdate()
      tracker
    }
    case UserMessage(userId, GetKey(id)) => asyncReplyDbOp { implicit conn =>
      log.debug("Requesting key for {}", id)
      selectTrackerKeySql.on("trackerId" -> id.toString)
        .as(scalar[String].singleOpt)
        .getOrElse(createNewKey(id, userId))
    }
    case UserMessage(userId, NewKey(id)) => asyncReplyDbOp { implicit conn =>
      createNewKey(id, userId)
    }

    case UserMessage(userId, GetGroups) => asyncReplyDbOp { implicit conn =>
      getUserGroupsSql.on(
        "userId" -> userId.userId,
        "userDomain" -> userId.domain
      ).as(long("id") ~ str("name") ~ get[Option[String]]("trackerId") *).foldLeft(Map.empty[Long, TrackerGroup])((groupMap, item) => {
        val groupId ~ groupName ~ trackerIdStr = item
        val trackerId = trackerIdStr.map(TrackingServerManager.parseTrackerId)
        groupMap + (groupId -> groupMap.get(groupId)
          .map(group => group.copy(trackers = trackerId.get :: group.trackers))
          .getOrElse(TrackerGroup(groupId.toString, groupName, trackerId.toList)))
      }).values.toList
    }
    case UserMessage(userId, GetGroup(id)) => asyncReplyDbOp { implicit conn =>
      getUserGroupSql.on(
        "groupId" -> id.toLong,
        "userId" -> userId.userId,
        "userDomain" -> userId.domain
      ).as(long("id") ~ str("name") ~ get[Option[String]]("trackerId") *).foldLeft(Map.empty[Long, TrackerGroup])((groupMap, item) => {
        val groupId ~ groupName ~ trackerIdStr = item
        val trackerId = trackerIdStr.map(TrackingServerManager.parseTrackerId)
        groupMap + (groupId -> groupMap.get(groupId)
          .map(group => group.copy(trackers = trackerId.get :: group.trackers))
          .getOrElse(TrackerGroup(groupId.toString, groupName, trackerId.toList)))
      }).values.headOption
    }
    case UserMessage(userId, CreateGroup(name)) => asyncReplyDbOp { implicit conn =>
      val id = addUserGroupSql.on(
        "name" -> name,
        "userId" -> userId.userId,
        "userDomain" -> userId.domain
      ).as(scalar[Long].single)
      addVisibleGroupSql.on(
        "group" -> id,
        "userId" -> userId.userId,
        "userDomain" -> userId.domain
      ).execute()
      TrackerGroup(id.toString, name, Nil)
    }

    case UserMessage(userId, UpdateGroup(group@TrackerGroup(groupId, name, _))) => asyncReplyDbOp { implicit conn =>
      updateUserGroupSql.on(
        "groupId" -> groupId.toLong,
        "name" -> name,
        "userId" -> userId.userId,
        "userDomain" -> userId.domain
      ).executeUpdate()
      group
    }

    case UserMessage(userId, AddTrackerToGroup(groupId, trackerId)) => asyncReplyDbOp { implicit conn =>
      addTrackerToGroupSql.on(
        "groupId" -> groupId.toLong,
        "trackerId" -> trackerId,
        "userId" -> userId.userId,
        "userDomain" -> userId.domain
      ).executeUpdate()
    }

    case UserMessage(userId, RemoveTrackerFromGroup(groupId, trackerId)) => asyncReplyDbOp { implicit conn =>
      removeTrackerFromGroupSql.on(
        "groupId" -> groupId.toLong,
        "trackerId" -> trackerId,
        "userId" -> userId.userId,
        "userDomain" -> userId.domain
      ).executeUpdate()
    }

  }

  private def createNewKey(id: TrackerId, userId: UserId)(implicit conn: Connection) = {
    log.debug("Requesting new key for {}", id)
    val newKey = generateNewKey(id.toString)
    deleteKeySql.on(
      "trackerId" -> id.toString,
      "userId" -> userId.userId,
      "userDomain" -> userId.domain
    ).executeUpdate()
    if (insertKeySql.on(
      "trackerId" -> id.toString,
      "key" -> newKey,
      "userId" -> userId.userId,
      "userDomain" -> userId.domain
    ).executeUpdate() > 0) newKey
    else throw new IllegalArgumentException(s"Tracker $id not found")
  }

  private lazy val hashGenerator = MessageDigest.getInstance("MD5")
  private def generateNewKey(oldKey:String)(implicit conn: Connection): String = {
    hashGenerator.reset()
    hashGenerator.update((oldKey + new Date().getTime.toString).getBytes)
    val key = DatatypeConverter.printBase64Binary(hashGenerator.digest()).replace('+','-').replace('/','_')
    findKeySql.on("key" -> key).as(scalar[Long].singleOpt) match {
      case None => key
      case Some(_) => generateNewKey(key)
    }
  }

}

object AdminDao extends SingletonActorManager[AdminDao] {
  trait AdminDaoForwardable extends SessionForwardable {
    def forwardTo: ActorSelection = AdminDao()
  }

  case object GetTrackers extends AdminDaoForwardable
  case class GetTracker(id: TrackerId) extends AdminDaoForwardable
  case class Create(name: String, trackerType: Option[String]) extends AdminDaoForwardable
  case class Update(tracker: Tracker) extends AdminDaoForwardable
  case class Delete(trackerId: TrackerId) extends AdminDaoForwardable

  case class GetKey(trackerId: TrackerId) extends AdminDaoForwardable
  case class NewKey(trackerId: TrackerId) extends AdminDaoForwardable

  case object GetGroups extends AdminDaoForwardable
  case class GetGroup(id: String) extends AdminDaoForwardable
  case class CreateGroup(name: String) extends AdminDaoForwardable
  case class UpdateGroup(group: TrackerGroup) extends AdminDaoForwardable
  case class DeleteGroup(groupId: String) extends AdminDaoForwardable
  case class AddTrackerToGroup(groupId: String, trackerId: String) extends AdminDaoForwardable
  case class RemoveTrackerFromGroup(groupId: String, trackerId: String) extends AdminDaoForwardable
}
