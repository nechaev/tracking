package tracking.web

import framework.JsonFormats._
import play.api.libs.json._
import tracking._
import play.api.libs.json.JsSuccess
import tracking.Tracker
import tracking.PositionState
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import tracking.AddressState
import tracking.TrackingEvent
import tracking.TrackerGroup

/**
 * Created by: sgl
 * Date: 13.02.13
 */
object TrackingJsonFormats {
  private object trackerIdReads extends Reads[TrackerId] {
    def reads(json: JsValue): JsResult[TrackerId] = json match {
      case JsString(str) => TrackingServerManager.parseTrackerIdOpt(str).map(JsSuccess(_)).getOrElse(JsError("Cannot parse trackerId"))
      case _ => JsError("Expected JsString")
    }
  }
  private object trackerIdWrites extends Writes[TrackerId] {
    def writes(o: TrackerId): JsValue = JsString(o.toString)
  }

  implicit val trackerIdFormat = Format(trackerIdReads, trackerIdWrites)
  implicit val trackerFormat = Json.format[Tracker]
  implicit val trackerGroupFormat = Json.format[TrackerGroup]
  implicit val positionStateWrites = Json.writes[PositionState]
  implicit val addressStateWrs = Json.writes[AddressState]
  implicit val trackingUserContextWrs = Json.writes[TrackingUserContext]
  implicit val trackPointWrs = Json.writes[TrackPoint]
  //implicit val parkingPointWrs = Json.writes[ParkingPoint]
  //implicit val trackInfoWrs = Json.writes[TrackInfo]
  implicit val trackRequestReads = Json.reads[TrackRequest]

  class TrackingEventWrites[T](eventAttrCode:String)(implicit eventWrites: Writes[T]) extends Writes[TrackingEvent[T]] {
    def writes(o: TrackingEvent[T]): JsValue = Json.obj(
      eventAttrCode -> eventWrites.writes(o.event).asInstanceOf[JsObject],
      "timestamp" -> Json.toJson(o.timestamp),
      "trackerId" -> Json.toJson(o.trackerId)
    )
  }
  implicit val positionEventWrites = new TrackingEventWrites[PositionState]("position")
  implicit val addressEventWrites = new TrackingEventWrites[AddressState]("address")
  implicit val boolEventWrites = new Writes[TrackingEvent[BoolState]] {
    def writes(o: TrackingEvent[BoolState]): JsValue = Json.obj(
      "timestamp" -> Json.toJson(o.timestamp),
      "trackerId" -> Json.toJson(o.trackerId),
      "bool-" + o.event.id.toString -> Json.toJson(o.event.value)
    )
  }
  implicit val floatEventWrites = new Writes[TrackingEvent[FloatState]] {
    def writes(o: TrackingEvent[FloatState]): JsValue = Json.obj(
      "timestamp" -> Json.toJson(o.timestamp),
      "trackerId" -> Json.toJson(o.trackerId),
      "float-" + o.event.id.toString -> Json.toJson(o.event.value)
    )
  }
  implicit val pingEventWrites = new Writes[TrackingEvent[PingEvent.type]] {
    def writes(o: TrackingEvent[PingEvent.type]): JsValue = Json.obj(
      "timestamp" -> Json.toJson(o.timestamp),
      "trackerId" -> Json.toJson(o.trackerId)
    )
  }
}

