package tracking.web

import akka.actor.Actor
import akka.event.Logging
import collection.immutable.Queue
import collection.mutable
import concurrent.duration.FiniteDuration
import gis.{GeocodingServiceManager, GeocodingServiceRequest, GisFunctions, Coordinates}
import java.lang.{Double => JDouble}
import java.util.concurrent.TimeUnit
import org.joda.time.{Duration, Instant}
import play.api.Play.current
import play.api.{Play, Configuration}
import reporting.ReportingContext
import reporting.ReportingContextRequest
import tracking._
import user.Login
import user.UserManager
import user.UserMessage
import xml.XML

/**
 * Created by: sgl
 * Date: 21.02.13
 */
class VirtualTestTrackerServer(conf: Configuration) extends Actor {
  private val log = Logging(context.system, this)

  private val MAX_LOG_SIZE = 10000

  case class RoutePoint(coordinates: Coordinates, next: RoutePoint)
  case class VirtualTracker(tracker: Tracker, speed: Double, routePoints: Queue[Coordinates])

  private val connName = conf.getString("domain").getOrElse(throw new IllegalArgumentException)

  private val routes = conf.getConfig("routes").map({ routeConfig =>
    Map(routeConfig.subKeys.map({ routeName =>
      routeName -> routeConfig.getString(routeName).map(readRoutePoints).getOrElse(Nil)
    }).toList: _*)
  }).getOrElse(Map.empty)
  private def readRoutePoints(pointsUrl: String) = Option(Play.classloader.getResource(pointsUrl)).map({ res =>
    (XML.load(res.openStream()) \ "point").map(point => Coordinates(
      new JDouble((point \ "lat").text),
      new JDouble((point \ "lon").text))
    )
  }).getOrElse(Nil)

  private val eventLog: mutable.Buffer[TrackingEvent[PositionState]] = mutable.Buffer.empty

  log.debug("Initialized {} routes", routes.size)

  val trackers: Iterable[Tracker] = conf.getConfig("trackers") map { trackersConfig =>
    for {
      trackerId <- trackersConfig.subKeys
      trackerConfig <- trackersConfig.getConfig(trackerId)
      trackerName <- trackerConfig.getString("name")
      trackerSpeed <- trackerConfig.getDouble("speed")
      trackerRoute <- trackerConfig.getString("route")
      routeStartPoint <- trackerConfig.getInt("startPoint")
      routePoints <- routes.get(trackerRoute)
    } yield {
      val tracker = Tracker(TrackerId(trackerId,connName), trackerName, None)
      runTracker(VirtualTracker(tracker, trackerSpeed, Queue((routePoints.drop(routeStartPoint)
        ++ routePoints.take(routeStartPoint)).toList: _*)))
      tracker
    }
  } getOrElse Nil

  log.debug("Initialized {} trackers", trackers.size)



  override def preStart() {
    UserManager.route(connName, self)
    TrackingServerManager.route(connName, self)
  }

  case class RunTracker(tracker: VirtualTracker)

  import context.dispatcher

  val currentState = mutable.HashMap[String, PositionState]()

  private def runTracker(tracker: VirtualTracker) {
    val (nextPoint, remainingPoints) = tracker.routePoints.dequeue
    val targetPoint = remainingPoints.head
    val positionEvent = TrackingEvent(
      trackerId = tracker.tracker.id,
      timestamp = new Instant,
      event = PositionState(
        coordinates = nextPoint,
        altitude = None,
        accuracy = Some(10.0),
        speed = Some(tracker.speed),
        heading = Some(GisFunctions.bearing(nextPoint, targetPoint))
      )
    )
    TrackerEventBus.publish(positionEvent)
    eventLog append positionEvent
    if (eventLog.size > MAX_LOG_SIZE) eventLog remove 0
    val targetDistance = GisFunctions.fccDistance(nextPoint, targetPoint)
    val targetTime = targetDistance * 3600 / tracker.speed
    //log.debug("Tracker {}: point {}, distance: {} m, time: {} ms", tracker.tracker.name, nextPoint, targetDistance, targetTime)
    context.system.scheduler.scheduleOnce(
      FiniteDuration(targetTime.toLong, TimeUnit.MILLISECONDS),
      self,
      RunTracker(tracker.copy(routePoints = remainingPoints enqueue nextPoint))
    )
  }

  def receive = {
    case UserMessage(userId, Login(userPassword)) => {
      sender ! (userId.userId == "test" && userId.domain == connName && userPassword == "test")
    }
    case UserMessage(userId, TrackingUserContextRequest) => sender ! TrackingUserContext(
      groups = List(TrackerGroup("0", "Virtual trackers", trackers.map(_.id).toList), TrackerGroup("1", "Empty group", Nil))
    )
    case UserMessage(userId, ReportingContextRequest) => {
      val dbName = conf.getString("reportingDataSource").getOrElse(connName)
      sender ! ReportingContext(userId, dbName)
    }
    case UserMessage(_, GeocodingServiceRequest) => sender ! conf.getString("geocoding").flatMap(GeocodingServiceManager.get)
    case TrackerDescriptionRequest(trackerId) => trackers.find(_.id == trackerId).foreach(sender ! _)
    case TrackerStateRequest(trackerId) => {
      //sender ! TrackingEvent(new Instant(), trackerId, ConnectionState(true))
      log.debug("Requested state for {}", trackerId)
      sender ! eventLog.reverseIterator.find(_.trackerId == trackerId).toList
    }
    case RunTracker(tracker) => runTracker(tracker)

    case TrackRequest(trackerId, start, end) => {
      log.debug("Requested event log for {} from {} to {}", trackerId, start, end)
      sender ! eventLog
        .filter(te => (te.trackerId == trackerId
        && te.timestamp.getMillis > start.getMillis
        && te.timestamp.getMillis < end.getMillis))
        .map(te =>TrackPoint(te.timestamp, te.event, new Duration(0)))
        .toList
    }

  }

}
