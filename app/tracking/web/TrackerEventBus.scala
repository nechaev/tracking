package tracking.web

import akka.actor.ActorRef
import akka.event.{LookupClassification, ActorEventBus}
import tracking.{TrackerId, TrackingEvent}

/**
 * Created by: sgl
 * Date: 27.01.13
 */
object TrackerEventBus extends ActorEventBus with LookupClassification {
  type Classifier = TrackerId
  type Event = TrackingEvent[Any]
  private var catchAllListeners: List[ActorRef] = Nil

  def subscribeToAll(listener: ActorRef) {
    catchAllListeners = listener :: catchAllListeners
  }

  override def publish(event: TrackingEvent[Any]) {
    super.publish(event)
    catchAllListeners foreach (_ ! event)
  }

  protected def classify(event: TrackingEvent[Any]): TrackerId = event.trackerId
  protected def publish(event: TrackingEvent[Any], subscriber: ActorRef) {
    subscriber ! event
  }
  protected def mapSize(): Int = 0x10000
}
