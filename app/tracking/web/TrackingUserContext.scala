package tracking.web

import tracking.{TrackerGroup, Tracker}
import session.SessionForwardable
import akka.actor.ActorRef
import user.UserManager

/**
 * Created by: vnechaev
 * Date: 13.02.13
 */
case class TrackingUserContext(
  groups: List[TrackerGroup]
)

case object TrackingUserContextRequest extends SessionForwardable {
  def forwardTo = UserManager()
}