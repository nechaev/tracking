package tracking.web

import akka.actor.{ActorRef, Props, Actor}
import akka.event.Logging
import akka.pattern.{ask, pipe}
import framework.{DomainRouter, SingletonActorManager}
import framework.Defaults.timeout
import play.api.Play
import play.api.Play.current
import scala.concurrent.Future
import scala.language.postfixOps
import tracking.{TrackerId, TrackingEvent}
import user.UserMessage
import org.slf4j.LoggerFactory
import java.text.ParseException

/**
 * Created by: vnechaev
 * Date: 06.02.13
 */
object TrackingServerManager extends DomainRouter {
  type Classifier = String
  type RoutableMessage = TrackerMessage
  def classify(message: TrackerMessage): String = message.trackerId.domain

  def parseTrackerId(trackerId: String) = trackerId split "@" match {
    case Array(trackerName, connName) => TrackerId(trackerName, connName)
    case _ => throw new ParseException("Cannot parse tracker ID (must be in form {id}@{domain})",0)
  }
  def parseTrackerIdOpt(trackerId: String) = try {
    Some(parseTrackerId(trackerId))
  } catch {
    case _:ParseException => None
  }
}

trait TrackerMessage {
  def trackerId: TrackerId
}
