package tracking.web

import tracking.TrackerId

/**
 * User: vnechaev
 * Date: 06.09.13
 */
case class TrackerStateRequest(trackerId: TrackerId) extends TrackerMessage
