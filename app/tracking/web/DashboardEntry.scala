package tracking.web

import org.joda.time.Duration
import tracking.{TrackerId, PositionState}

/**
 * User: vnechaev
 * Date: 02.07.13
 */
case class DashboardEntry(
  trackerId: TrackerId,
  name: String,
  groupName: String,
  positionState: Option[PositionState],
  address: String,
  dailyMileage: Option[Double],
  dailyEngineHours: Option[Duration],
  dailyGpsLostTime: Option[Duration],
  dailyParkingTime: Option[Duration]
)
