package tracking.web

import akka.actor.{ActorRef, Actor}
import akka.event.Logging
import akka.pattern.{ask, pipe}
import anorm._
import framework.{ActorTools, SingletonActorManager}
import framework.Defaults.timeout
import gis.Coordinates
import java.util.Date
import org.joda.time.{Duration, Instant}
import play.api.db.DB
import play.api.Play.current
import reporting.{ReportingContextRequest, ReportingContext}
import scala.concurrent.Future
import tracking.{TrackerId, PositionState}
import user.{UserManager, UserMessage}
import session.SessionForwardable

/**
 * User: vnechaev
 * Date: 03.09.13
 */
/*
class TrackQuery extends Actor with ActorTools {
  private implicit val log = Logging(context.system, this)

  private val eventLogSql = SQL(
    """SELECT eventtime, latitude, longitude, speed, heading, accuracy FROM tracking_v_track
      |WHERE userId={userId} AND trackerId={trackerId}
      |AND eventtime BETWEEN {start} AND {end}
      |ORDER BY eventtime
      | """.stripMargin)

  private val parkingsSql = SQL(
    """SELECT eventtime, latitude, longitude, duration FROM tracking_v_parkings
      |WHERE userId={userId} AND trackerId={trackerId}
      |AND eventtime BETWEEN {start} AND {end}
      |ORDER BY eventtime
      | """.stripMargin)

  import context.dispatcher

  def receive = {
    case UserMessage(userId, TrackRequest(trackerId, start, end)) => (UserManager() ? ReportingContextRequest).mapTo[ReportingContext]
      .map({ reportingContext => DB.withConnection(reportingContext.dataSourceName) { implicit connection => logThrows {
        log.debug("Querying track info for {} within {} and {}", trackerId, start, end)
        eventLogSql
          .on("userId" -> userId, "trackerId" -> trackerId, "start" -> start.toDate, "end" -> end.toDate)()
          .map({ row => TrackPoint(
          timestamp = new Instant(row[Date]("eventtime")),
          position = PositionState(
            coordinates = Coordinates(
              row[Double]("latitude"),
              row[Double]("longitude")
            ),
            accuracy = row[Double]("accuracy"),
            speed = row[Double]("speed"),
            heading = row[Double]("heading")))
        }).toList ++
          parkingsSql
            .on("userId" -> userId, "trackerId" -> trackerId, "start" -> start.toDate, "end" -> end.toDate)()
            .map({ row => ParkingPoint(
            timestamp = new Instant(row[Date]("eventtime")),
            coordinates = Coordinates(
              row[Double]("latitude"),
              row[Double]("longitude")
            ),
            duration = new Duration(row[Int]("duration") * 1000))
          }).toList
        )
    }}}) pipeTo sender
  }

}

object TrackQuery extends SingletonActorManager[TrackQuery]
*/

case class TrackRequest(trackerId: TrackerId, start: Instant, end: Instant) extends TrackerMessage with SessionForwardable {
  //def forwardTo = TrackQuery()
  def forwardTo = TrackingServerManager()
}
case class TrackPoint(timestamp: Instant, position: PositionState, duration: Duration)
