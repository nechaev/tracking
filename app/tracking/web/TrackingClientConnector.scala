package tracking.web

import TrackingJsonFormats._
import akka.actor.ActorDSL._
import akka.actor._
import akka.event.Logging
import akka.pattern.{ask, pipe}
import framework.Defaults.timeout
import framework.JsonFormats._
import gis._
import play.api.libs.json._
import reporting.{ReportingContextRequest, ReportingContext}
import scala.language.postfixOps
import session.{GetOrCreateChild, InboundClientMessage, InboundMessageBus, OutboundClientMessage}
import tracking._
import tracking.web.TrackingJsonFormats._
import framework.SingletonActorManager
import scala.concurrent.Future

/**
 * Created by: sgl
 * Date: 11.03.13
 */
class TrackingClientConnector extends Actor {
  private val log = Logging(context.system, this)

  private val CONNECTION_LISTENER_NAME = "tracking-listener"

  import context.dispatcher
  import TrackingClientConnector._

  def receive = {
    case InboundClientMessage(msgId, TRACKING_CONTEXT, _, connection, session) => {
      log.debug("Context requested")
      (session ? TrackingUserContextRequest).mapTo[TrackingUserContext] foreach { userContext =>
        connection ! OutboundClientMessage(msgId, TRACKING_CONTEXT, Json.toJson(userContext))
      }
    }
    case InboundClientMessage(msgId, TRACKER_DESCRIPTION, msgBody, connection, session) => {
      msgBody.validate[TrackerId].foreach(trackerId => {
        (TrackingServerManager() ? TrackerDescriptionRequest(trackerId)).mapTo[Tracker].foreach(tracker => {
          connection ! OutboundClientMessage(msgId, TRACKER_DESCRIPTION, Json.toJson(tracker))
        })
      })
    }
    case InboundClientMessage(msgId, OBSERVE_TRACKER_STATE, msgBody, connection, _) => {
      msgBody.validate[TrackerId].foreach(trackerId => {
        log.debug("Received observer request for {}", trackerId)
        (connection ? GetOrCreateChild(Props(classOf[ConnectionTrackingListener], msgId), CONNECTION_LISTENER_NAME))
          .mapTo[ActorRef].foreach({ listener =>
          TrackerEventBus.subscribe(listener, trackerId)
          TrackingServerManager().tell(TrackerStateRequest(trackerId), listener)
        })
      })
    }
    case InboundClientMessage(msgId, REMOVE_LISTENER, msgBody, connection, _) => {
      msgBody.validate[TrackerId].foreach(trackerId => {
        (connection ? GetOrCreateChild(Props(classOf[ConnectionTrackingListener], msgId), CONNECTION_LISTENER_NAME))
          .mapTo[ActorRef].foreach(TrackerEventBus.unsubscribe(_, trackerId))
      })
    }
    case InboundClientMessage(msgId, TRACK, msgBody, connection, session) => {
      msgBody.validate[TrackRequest].map({ queryCommand =>
        log.debug("Querying event log for {}", queryCommand.trackerId)
        (TrackingServerManager() ? queryCommand).mapTo[List[TrackPoint]]
          .foreach(trackInfo => connection ! OutboundClientMessage(msgId, TRACK, Json.toJson(trackInfo)))
//        for {
//          reportingContext <- (session ? ReportingContextRequest).mapTo[ReportingContext]
//          track <- (TrackQuery() ? (queryCommand, reportingContext)).mapTo[TrackInfo]
//        } {
//          connection ! OutboundClientMessage(msgId, TRACK, Json.toJson(track))
//        }
      }).recoverTotal({err => for ((path, ve) <- err.errors) log.error("Parsing {} failed: {}", path, ve)})
    }
  }

  override def preStart() {
    Array(
      TRACKING_CONTEXT,
      TRACKER_DESCRIPTION,
      OBSERVE_TRACKER_STATE,
      REMOVE_LISTENER,
      TRACK
    ).foreach(InboundMessageBus.subscribe(self, _))
  }

}

class ConnectionTrackingListener(msgId: String) extends Actor {
  import TrackingClientConnector._
  import context.dispatcher
  val geocodingService = (context.parent ? GeocodingServiceRequest).mapTo[Option[ActorSelection]]

  def receive = {
    case events: List[_] => events.foreach(self.!)
    case evt@TrackingEvent(timestamp, trackerId, event: PositionState) => {
      context.parent ! OutboundClientMessage(msgId, STATE_CHANGE_EVENT, Json.toJson(evt.asInstanceOf[TrackingEvent[PositionState]]))
      geocodingService foreach (_ foreach (gs =>
        (gs ? GenerateAddressEvents(List(evt))).mapTo[List[TrackingEvent[_]]]
          .foreach(events => events.foreach(self.!))
      ))
    }
    case evt@TrackingEvent(_, _, _: BoolState) =>
      context.parent ! OutboundClientMessage(msgId, STATE_CHANGE_EVENT, Json.toJson(evt.asInstanceOf[TrackingEvent[BoolState]]))
    case evt@TrackingEvent(_, _, _: FloatState) =>
      context.parent ! OutboundClientMessage(msgId, STATE_CHANGE_EVENT, Json.toJson(evt.asInstanceOf[TrackingEvent[FloatState]]))
    case evt@TrackingEvent(_, _, _: AddressState) =>
      context.parent ! OutboundClientMessage(msgId, STATE_CHANGE_EVENT, Json.toJson(evt.asInstanceOf[TrackingEvent[AddressState]]))
    case evt@TrackingEvent(_, _, PingEvent) =>
      context.parent ! OutboundClientMessage(msgId, STATE_CHANGE_EVENT, Json.toJson(evt.asInstanceOf[TrackingEvent[PingEvent.type]]))
  }

  override def postStop() {
    TrackerEventBus.unsubscribe(self)
  }
}

object TrackingClientConnector extends SingletonActorManager[TrackingClientConnector] {
  // in/out
  val TRACKING_CONTEXT = "tracking.user-context"
  // in
  val TRACKER_DESCRIPTION = "tracking.tracker-description"
  // in
  val OBSERVE_TRACKER_STATE = "tracking.observe-tracker-state"
  // in
  val REMOVE_LISTENER = "tracking.remove-state-listener"
  // out
  val STATE_CHANGE_EVENT = "tracking.event"
  // in/out
  val TRACK = "tracking.track"
}