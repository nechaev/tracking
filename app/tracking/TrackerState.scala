package tracking

import gis.Coordinates
import org.joda.time.Instant


/**
 * Created by: vnechaev
 * Date: 27.12.12
 */

case class PositionState(
  coordinates: Coordinates,
  altitude: Option[Double],
  accuracy: Option[Double],
  speed: Option[Double],
  heading: Option[Double]
)

case class AddressState(address: String)

case class BoolState (
  id: Int,
  value: Boolean
)

case class FloatState (
  id: Int,
  value: Double
)

object PredefinedStates {
  val MOTION_STATE = -1
  val FINE_LOCATION_STATE = -2
  val NETWORK_AVAILABLE_STATE = -3
}