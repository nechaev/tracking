package tracking

import org.joda.time.Instant

/**
 * Created by: vnechaev
 * Date: 27.12.12
 */
case class TrackingEvent[+T] (
  timestamp: Instant,
  trackerId: TrackerId,
  event: T
)

case class TextMessageEvent (
  text: String
)

case object PingEvent