package tracking

/**
 * Created by: sgl
 * Date: 13.01.13
 */
case class Tracker(
  id: TrackerId,
  name: String,
  trackerType: Option[String]
)

case class TrackerId(id: String, domain: String) {
  override def toString: String = id + "@" + domain
}
