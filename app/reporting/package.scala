import play.api.libs.json.Json
import tracking.web.TrackingJsonFormats._
/**
 * Created by: sgl
 * Date: 06.04.13
 */
package object reporting {
  implicit val trackingReportRequestReads = Json.reads[TrackingReportRequest]
}
