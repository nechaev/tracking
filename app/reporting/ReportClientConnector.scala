package reporting

import akka.actor.Actor
import akka.event.Logging
import akka.pattern.ask
import akka.util.ByteString
import framework.Defaults.timeout
import play.api.libs.iteratee.{Iteratee, Enumerator}
import play.api.libs.json._
import scala.language.reflectiveCalls
import session.{OutboundClientMessage, InboundClientMessage, InboundMessageBus}

/**
 * Created by: sgl
 * Date: 28.03.13
 */
class ReportClientConnector extends Actor {
  private val log = Logging.getLogger(context.system, self)
  private val GENERATE_REPORT = "reporting.generate-report"

  import context.dispatcher

  def receive = {
    case InboundClientMessage(msgId, GENERATE_REPORT, msgBody, connection, session) => {
      (msgBody.asInstanceOf[JsObject] + ("format" -> JsString("embeddable"))).validate[TrackingReportRequest] match {
        case JsSuccess(request, _) => (session ? ReportingContextRequest).mapTo[ReportingContext]
          .flatMap(userContext => (context.parent ? (request, userContext)).mapTo[Enumerator[Array[Byte]]])
          .flatMap(_ |>>> Iteratee.consume[Array[Byte]]())
          .map(ByteString(_).decodeString("UTF-8"))
          .recover({
            case e: Throwable => {
              log.error(e, "Report generation error")
              e.getMessage + "\n" + e.getStackTraceString
            }
          })
          .foreach(response => connection ! OutboundClientMessage(msgId, GENERATE_REPORT, JsString(response)))
        case JsError(errors) => connection ! OutboundClientMessage(msgId, GENERATE_REPORT, JsString(errors.flatMap(_._2).foldLeft("")(
          _ + "," + _.message
        )))
      }
    }
  }

  override def preStart() {
    InboundMessageBus.subscribe(self, GENERATE_REPORT)
  }
}
