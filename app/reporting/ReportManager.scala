package reporting

import akka.actor.{Props, Actor}
import akka.event.Logging
import anorm.SqlParser._
import anorm._
import framework.SingletonActorManager
import java.util.Locale
import org.eclipse.birt.core.framework.Platform
import org.eclipse.birt.report.engine.api._
import org.eclipse.core.internal.registry.RegistryProviderFactory
import org.joda.time.LocalDate
import play.api.Play.current
import play.api.db.DB
import play.api.libs.iteratee.Enumerator
import scala.collection.JavaConversions._

/**
 * Created by: sgl
 * Date: 20.03.13
 */
class ReportManager extends Actor {
  private val log = Logging.getLogger(context.system, self)

  private val selectTopSelectionIndexSql = SQL("SELECT MAX(selectionId) FROM TRACKING_TRACKER_SELECTION")
  private val insertSelectionItemSql = SQL(
    """INSERT INTO TRACKING_TRACKER_SELECTION(selectionId, trackerId)
      | VALUES({selectionId}, {trackerId})
    """.stripMargin)
  private val cleanupSelectionSql = SQL("DELETE FROM TRACKING_TRACKER_SELECTION WHERE selectionId = {selectionId}")

  var engine: IReportEngine = _

  override def preStart() {
    super.preStart()
    log.debug("Starting reporting engine")
    val config = new EngineConfig
    try {
      Platform.startup(config)
    } catch {
      case e: Throwable => log.error(e, "BIRT platform startup error")
    }
    engine = Platform
      .createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY).asInstanceOf[IReportEngineFactory]
      .createReportEngine(config)

    context.actorOf(Props[ReportClientConnector])
  }


  override def postStop() {
    super.postStop()
    log.debug("Stopping reporting engine")
    engine.destroy()
    Platform.shutdown()
    RegistryProviderFactory.releaseDefault()
  }

  private def formatLocalDate(o: LocalDate) = new java.sql.Date(o.toDate.getTime)

  import context.dispatcher

  def receive = {
    case (req@TrackingReportRequest(reportName, lang, start, end, trackers, format), ReportingContext(_, dataSource)) => {
      sender ! Enumerator.outputStream { output =>
        log.debug(s"Report generation started: $req using data source $dataSource with locale $lang")
        var selectionIndex: Long = -1
        try {
          selectionIndex = DB.withConnection(dataSource) {
            implicit conn =>
              val maxIndex = selectTopSelectionIndexSql.as(scalar[Option[Long]].single).getOrElse(0L) + 1
              insertSelectionItemSql.asBatch.addBatchList(trackers.map(
                trackerId => List("selectionId" -> toParameterValue(maxIndex), "trackerId" -> toParameterValue(trackerId.toString))
              )).execute()
              maxIndex
          }
          val loader = getClass.getClassLoader
          val report = engine.openReportDesign(loader.getResource("reports/" + reportName + ".xml").openStream())
          val task = engine.createRunAndRenderTask(report)
          task.getAppContext.asInstanceOf[java.util.Map[String, Object]]
            .put(EngineConstants.APPCONTEXT_CLASSLOADER_KEY, loader)
          task.setParameterValues(Map(
            "dataSourceJndi" -> dataSource,
            "start" -> formatLocalDate(start),
            "end" -> formatLocalDate(end),
            "selectionId" -> selectionIndex.toInt
          ))
          task.setLocale(new Locale(lang.replace("-", "_")))
          val renderOpts = format match {
            case "embeddable" => {
              val opts = new HTMLRenderOption()
              opts.setEmbeddable(true)
              opts
            }
            case "pds" => {
              new PDFRenderOption()
            }
            case "xls" => {
              new EXCELRenderOption()
            }
            case _ => {
              val opts = new HTMLRenderOption()
              opts.setEmbeddable(false)
              opts
            }
          }
          renderOpts.setOutputStream(output)
          task.setRenderOption(renderOpts)
          task.run()
          task.close()
        } catch {
          case e: Throwable => log.error(e, "Report generation error")
        } finally {
          output.close()
          if (selectionIndex >= 0) DB.withConnection(dataSource) {
            implicit conn =>
              cleanupSelectionSql.on("selectionId" -> selectionIndex).executeUpdate()
          }
        }
        log.debug("Report generation finished: {}", reportName)
      }
    }
  }

}

object ReportManager extends SingletonActorManager[ReportManager]
