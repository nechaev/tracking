package reporting

import org.joda.time.LocalDate
import tracking.TrackerId

/**
 * Created by: sgl
 * Date: 06.04.13
 */
case class TrackingReportRequest(name: String, lang: String, start: LocalDate, end: LocalDate, trackers: List[TrackerId], format: String)
