package reporting

import user.{UserManager, UserId}
import session.SessionForwardable
import akka.actor.ActorRef

/**
 * User: vnechaev
 * Date: 28.05.13
 */
case class ReportingContext(
  userId: UserId,
  dataSourceName: String
)

case object ReportingContextRequest extends SessionForwardable {
  def forwardTo = UserManager()
}