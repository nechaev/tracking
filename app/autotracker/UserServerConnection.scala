package autotracker

import akka.actor.ActorRef
import akka.event.Logging
import scala.concurrent.Promise
import user.{UserMessage, UserId}

class UserServerConnection(host: String, port: Int, userId: UserId, userPassword: String, connName: String,
                           databaseConnection: ActorRef, result: Promise[Boolean])
  extends TrackingConnection(host, port, userId.userId, userPassword) {

  private val log = Logging(context.system, this)

  var userTrackers: List[String] = Nil
  def handleServerMessages = {
    case _: ServerMessages.LoginFailed => {
      result.success(false)
      context.stop(self)
      log.debug("Login failed for {}", userId.userId)
    }
    case msg: ServerMessages.MachineAdded => {
      val trackerid = trackerId(msg.phoneNumber, connName)
      userTrackers = trackerid :: userTrackers
    }
    case ServerMessages.FinishLoadLoginData => {
      result.success(true)
      databaseConnection ! UserMessage(userId, UpdateUserTrackers(userTrackers))
      context.stop(self)
      log.debug(s"Captured user context(${userTrackers.length} trackers)")
    }
  }
}