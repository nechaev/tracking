package autotracker

import akka.util.ByteString

/**
 * Created by: sgl
 * Date: 09.01.13
 */
object ClientMessages {
  object Heartbeat extends ClientDataPacket {
    def length = 0
    def commandId = 0
    def write = ByteString.empty
  }

  class Login(
    val interfaceRole: Int,
    val login: String,
    val password: String) extends ClientDataPacket {

    def length = PHONE_NUMBER_LENGTH * 2 + 4
    def commandId = 3
    def write = ByteString.newBuilder
      .putInt(interfaceRole)
      .++=(writeString(login, PHONE_NUMBER_LENGTH))
      .++=(writeString(password, PHONE_NUMBER_LENGTH))
      .result()
  }

  abstract class UserPacket(login: String) extends ClientDataPacket {
    def length = PHONE_NUMBER_LENGTH
    def write = writeString(login, PHONE_NUMBER_LENGTH)
  }

  class GetUserModules(login: String) extends UserPacket(login) {
    def commandId = 274
  }
}
