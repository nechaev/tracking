package autotracker

import akka.actor.Actor
import akka.event.Logging
import anorm.SqlParser._
import anorm._
import anorm.~
import framework.{ActorDbTools, ActorTools}
import gis.Coordinates
import java.math.BigDecimal
import java.util.Date
import org.joda.time.{Duration, Instant}
import play.api.Play.current
import play.api.db.DB
import scala.language.postfixOps
import tracking.PositionState
import tracking.TrackerGroup
import tracking.TrackingEvent
import tracking._
import tracking.web.TrackerStateRequest
import tracking.web.TrackingUserContext
import tracking.web._
import user.UserMessage

/**
 * Created by: sgl
 * Date: 09.02.13
 */
class DatabaseConnection(protected val dataSource: String, connName: String) extends Actor with ActorDbTools {
  private implicit val log = Logging(context.system, this)

  /*
  private val authenticateSql = SQL(
    """SELECT USERLOGIN, USERDESCRIPTION
      | FROM USERS
      | WHERE USERLOGIN={login} AND USERPASSWORD={password}""".stripMargin)
      */
  private val trackersSql = SQL(
    """SELECT C.CARMODULEDESCRIPTION, C.CARMODULEPHONE, P.TRACKER_TYPE
      |FROM CARMODULE C LEFT JOIN TRACKING_TRACKER_PROPERTIES P ON C.CARMODULEPHONE = P.PHONENUMBER
      |WHERE C.CARMODULEPHONE = {phoneNumber}""".stripMargin)
  private val groupsSql = SQL("SELECT trackerId, cargroupId, groupName FROM tracking_v_trackers WHERE T_USERID = {login}")
  private val cleanUserTrackersSql = SQL("DELETE FROM TRACKING_USERTRACKERS WHERE USERID={login}")
  private val updateUserTrackersSql = SQL("INSERT INTO TRACKING_USERTRACKERS (USERID, PHONENUMBER) VALUES({login},{phoneNumber})")
  private val lastHeadingSql = SQL("SELECT LASTCOURSE FROM CARMODULE WHERE CARMODULEPHONE={phoneNumber} " +
    "AND NOT (CARMODULESTATUS = 17)")
  private val positionStateSql = SQL(
    """SELECT
      |LASTEVENTTIME eventtime,
      |LASTLAN latitude,
      |LASTLON longitude,
      |LASTSPEED speed,
      |LASTCOURSE heading
      |FROM carmodule
      |WHERE carmodulephone = {phoneNumber}
    """.stripMargin)
  private val trackSql = SQL(
    """SELECT
      |GMTEVENTTIME,
      |CARMODULEHISTORYLATITUDE lat,
      |CARMODULEHISTORYLONGITUDE lon,
      |CARMODULEHISTORYSPEED speed,
      |CARMODULEHISTORYCOURCE heading,
      |CARMODULEHISTORYGPSMISTAKE accuracy
      |FROM CARMODULEHISTORY
      |WHERE CARMODULEID IN (
      | SELECT CARMODULEID FROM CARMODULE
      | WHERE CARMODULEPHONE = {phoneNumber}
      | AND NOT CARMODULESTATUS = 17
      |) AND GMTEVENTTIME BETWEEN {start} AND {end}
      |ORDER BY GMTEVENTTIME
    """.stripMargin)
  def receive = {
    /*
    case UserMessage(userId, Login(password)) => asyncReply {
      DB.withConnection(database) {
        implicit connection =>
          authenticateSql.on("login" -> login, "password" -> password)().map({
            row =>
              User(row[String]("USERLOGIN"), row[String]("USERDESCRIPTION"))
          }).headOption
      }
    }
    */
    case UserMessage(userId, UpdateUserTrackers(trackerIds)) => asyncReplyDbOp { implicit conn =>
        cleanUserTrackersSql.on("login" -> userId.toString).execute()
        trackerIds.foldLeft(updateUserTrackersSql.asBatch)(
          (batch, trackerId) => batch.addBatch("login" -> userId.toString, "phoneNumber" -> trackerId2phoneNumber(trackerId))
        ).execute()
      }

    case UserMessage(userId, TrackingUserContextRequest) => asyncReplyDbOp { implicit conn =>
        TrackingUserContext(
          groups = groupsSql.on("login" -> userId.toString).as(
            get[Option[BigDecimal]]("cargroupId") ~ get[Option[String]]("groupName") ~ str("trackerId") *
          ).groupBy({
              case Some(groupId) ~ Some(groupName) ~ _ => (groupId.intValue(), groupName)
              case _ => ("null", "")
            }).toList.map({
              case ((groupId, groupName), groupMaps) => TrackerGroup(groupId.toString, groupName, groupMaps map {
                case _ ~ _ ~ trackerId => TrackingServerManager.parseTrackerId(trackerId)
              })
            })
        )
    }

    case FixPositionHeading(phoneNumber, position) => asyncReplyDbOp { implicit conn =>
        lastHeadingSql.on("phoneNumber" -> phoneNumber).as(scalar[BigDecimal].singleOpt).map( newHeading =>
          position.copy(heading = Some(newHeading.doubleValue()))
        ).getOrElse(position)
      }

    case TrackerStateRequest(trackerId) => asyncReplyDbOp { implicit conn =>
        positionStateSql.on("phoneNumber" -> trackerId.id)
          .as(date("eventtime") ~ get[BigDecimal]("latitude") ~ get[BigDecimal]("longitude")
              ~ get[BigDecimal]("speed") ~ get[BigDecimal]("heading") *).map({
          case eventtime ~ latitude ~ longitude ~ speed ~ heading =>
            TrackingEvent(new Instant(eventtime), trackerId, PositionState(
              Coordinates(latitude.doubleValue(), longitude.doubleValue()),
              None,
              None,
              Some(speed.doubleValue()),
              Some(heading.doubleValue())
            ))
          //TODO: connection event
        })
      }

    case TrackerDescriptionRequest(trackerId) => asyncReplyDbOp { implicit conn =>
        trackersSql.on("phoneNumber" -> trackerId.id).as((str("CARMODULEDESCRIPTION") ~ str("CARMODULEPHONE") ~ get[Option[String]]("TRACKER_TYPE") single).map({
          case name ~ phone ~ trackerType => Tracker(trackerId, s"$name ($phone)", trackerType)
        }))
      }

    case TrackRequest(trackerId, start, end) => asyncReplyDbOp { implicit conn =>
      trackSql.on(
        "phoneNumber" -> trackerId.id,
        "start" -> start.getMillis / 1000,
        "end" -> end.getMillis / 1000
      ).as(get[BigDecimal]("GMTEVENTTIME") ~ get[BigDecimal]("lat") ~ get[BigDecimal]("lon") ~ get[BigDecimal]("speed")
        ~ get[BigDecimal]("heading") ~ get[BigDecimal]("accuracy") *).map({
        case eventTime ~ lat ~ lon ~ speed ~ heading ~ accuracy => TrackPoint(new Instant(eventTime.longValue() * 1000),
          PositionState(Coordinates(lat.doubleValue(), lon.doubleValue()), None, Some(accuracy.doubleValue()),
            Some(speed.doubleValue()), Some(heading.doubleValue())),
          new Duration(0L))
      })
    }
  }

}

case class UpdateUserTrackers(trackerIds: Seq[String])
case class FixPositionHeading(phoneNumber: String, position: PositionState)
