package autotracker

import org.joda.time.Instant
import akka.util.{ByteIterator, ByteString}

/**
 * Created by: sgl
 * Date: 09.01.13
 */
trait Packet

trait ClientPacket extends Packet {
  def length: Int
  def write: ByteString
}

trait ClientDataPacket extends ClientPacket {
  def commandId: Int
}

class PacketHeader(
  val dataSize: Int,
  val commandId: Int,
  val tryCount: Int,
  val time: Instant,
  val version: Int) extends ClientPacket {

  def this(dataSize: Int, commandId: Int) = this(dataSize, commandId, 0, new Instant, PROTOCOL_VERSION)
  def this(dataPacket: ClientDataPacket) = this(dataPacket.length, dataPacket.commandId)
  def this(buffer: ByteIterator) = this(buffer.getInt, buffer.getInt, buffer.getInt, readInstant(buffer), buffer.getInt)

  def length = HEADER_SIZE
  def write = ByteString.newBuilder.putInt(dataSize).putInt(commandId).putInt(tryCount).++=(writeInstant(time)).putInt(version).result()

}

object HeaderHeader extends PacketHeader(HEADER_SIZE, -1, 0, null, PROTOCOL_VERSION)
