import akka.util.{ByteString, ByteIterator}
import autotracker.ServerMessages.{NewPoint, DbPoint}
import gis.Coordinates
import java.nio.ByteOrder
import org.joda.time.Instant
import tracking._

/**
 * Created by: sgl
 * Date: 09.01.13
 */
package object autotracker {
  val PHONE_NUMBER_LENGTH = 32
  val STATUS_STRING_LENGTH = 200
  val PACKET_TEXT_LENGTH = 2000
  val GSM_NET_NAME_LENGTH = 18
  val HEADER_SIZE = 20
  val PROTOCOL_VERSION = 8
  val CHARSET = "WINDOWS-1251"
  val HEARTBEAT_INTERVAL = 60000
  implicit val byteOrder = ByteOrder.LITTLE_ENDIAN

  def readString(buffer: ByteIterator, length: Int): String = {
    val bytes = new Array[Byte](length)
    buffer.getBytes(bytes)
    ByteString(bytes.takeWhile(_ != 0)).decodeString(CHARSET)
  }

  def writeString(value: String, length: Int): ByteString = ByteString(value.take(length), CHARSET) ++
    (if (value.length < length) ByteString(new Array[Byte](length - value.length)) else ByteString.empty)

  def readInstant(buffer: ByteIterator): Instant = new Instant(buffer.getInt.toLong * 1000)

  def writeInstant(value: Instant): ByteString = ByteString.newBuilder.putInt((value.getMillis / 1000).toInt).result()

  def dbPoint2Position(dbPoint: DbPoint) = PositionState(
    coordinates = Coordinates(dbPoint.latitude, dbPoint.longitude),
    altitude = None,
    accuracy = Some(dbPoint.dop * 2.66), //DOP to meters
    speed = Some(dbPoint.speed),
    heading = if (dbPoint.heading >= 0) Some(dbPoint.heading) else None
  )

  @inline def trackerId(phoneNumber: String, connName: String) = phoneNumber + "@" + connName
  @inline def trackerId2phoneNumber(trackerId: String) = trackerId.split("@").head
  /*
  def mashineAdded2State(msg: ServerMessages.MachineAdded, connName: String) =
    TrackingEvent(
      trackerId = trackerId(msg.phoneNumber, connName),
      timestamp = msg.point.time,
      event = TrackerState(
        timestamp = msg.point.time,
        position = dbPoint2Position(msg.point),
        connection = ConnectionState(
          isGpsConnected = msg.isLostGps == 0,
          isGsmConnected = msg.status == 0 && msg.dataConnectionState > 1
        )
      )
    )
  */
  def newPoint2TrackingEvent[T](msg: NewPoint, evt: T, connName: String) = TrackingEvent[T](
    timestamp = msg.point.time,
    trackerId = TrackerId(msg.phoneNumber, connName),
    event = evt
  )

}
