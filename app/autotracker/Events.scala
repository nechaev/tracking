package autotracker

/**
 * Created by: sgl
 * Date: 26.02.13
 */
object Events {
  case class AtEvent(name: String) {
    def isPositional = positionalEvents contains this
    def isConnection = connectionEvents contains this
  }

  val Unknown       = AtEvent("EV_NONE")
  val Move          = AtEvent("EV_DIST")
  val Heading       = AtEvent("EV_VECT")
  val Timeout       = AtEvent("EV_TIME")
  val Start         = AtEvent("EV_MOVE")
  val Stop          = AtEvent("EV_STOP")
  val Parking       = AtEvent("EV_PARKING")
  val GpsTrackLost  = AtEvent("EV_GPS_TRACK_LOST")
  val GpsTrackFound = AtEvent("EV_GPS_TRACK_FOUND")
  val IgnitionOn    = AtEvent("EV_MOTOR_START")
  val IgnitionOff   = AtEvent("EV_MOTOR_STOP")
  val GpsLostSat    = AtEvent("EV_GPS_LOST_SAT")
  val GsmLostSig    = AtEvent("EV_GSM_LOST_SIG")
  val GsmChangeNet  = AtEvent("EV_GSM_CHANGE_NET")
  val GpsFound      = AtEvent("EV_GPS_FOUND")

  private val atEvents = Map(
    1 -> Move,
    2 -> Heading,
    3 -> Timeout,
    9 -> GpsLostSat,
    11 -> GsmLostSig,
    13 -> Start,
    14 -> Stop,
    31 -> Parking,
    36 -> GpsTrackLost,
    37 -> GpsTrackFound,
    38 -> IgnitionOn,
    39 -> IgnitionOff,
    40 -> GpsFound
  )

  private val positionalEvents = Set(Move, Heading, Timeout, Start, Stop)
  private val connectionEvents = Set(GpsTrackLost, GpsTrackFound, GpsLostSat, GpsFound, GsmLostSig, GsmChangeNet)

  def apply(event: Short): AtEvent = atEvents.get(event).getOrElse(Unknown)

}
