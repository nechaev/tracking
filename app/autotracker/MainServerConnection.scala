package autotracker

import akka.actor.Kill
import akka.event.Logging
import akka.pattern.ask
import framework.Defaults.timeout
import tracking.{PredefinedStates, BoolState, PositionState}
import tracking.web.TrackerEventBus

class MainServerConnection(host: String, port: Int, login: String, password: String, connName: String)
  extends TrackingConnection(host, port, login, password) {

  private val log = Logging(context.system, this)

  import context.dispatcher

  def handleServerMessages = {
    case _: ServerMessages.LoginFailed => {
      log.error("Main server connection failed - invalid credentials")
      context.stop(self)
    }
    case _: ServerMessages.LoginSuccess => log.debug("Main server connection started")
//    case msg: ServerMessages.UserAdded => {
//      context.parent ! User(login = msg.login, name = msg.description)
//      //log.debug("User {} ({})", msg.login, msg.description)
//    }
    case msg: ServerMessages.NewPoint => {
      val event = Events(msg.point.eventCode)
      if (event.isPositional) {
        val position = dbPoint2Position(msg.point)
        if (position.heading == None) (context.parent ? FixPositionHeading(msg.phoneNumber, position)).mapTo[PositionState]
          .foreach(TrackerEventBus publish newPoint2TrackingEvent(msg, _, connName))
        else TrackerEventBus publish newPoint2TrackingEvent(msg, position, connName)
      }
      if (event.isConnection) {
        TrackerEventBus publish newPoint2TrackingEvent(msg, BoolState(PredefinedStates.NETWORK_AVAILABLE_STATE, msg.isGprs != 0), connName)
        TrackerEventBus publish newPoint2TrackingEvent(msg, BoolState(PredefinedStates.FINE_LOCATION_STATE, msg.validGps != 0), connName)
      }
    }
    case ServerMessages.FinishLoadLoginData => log.debug("Receiving live data")
  }

  override protected def onDisconnect() {
    log.debug("Connection failed, restarting")
    self ! Kill
  }
}