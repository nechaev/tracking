package autotracker

import akka.util.ByteIterator
import scala.math._

/**
 * Created by: sgl
 * Date: 08.01.13
 */
object ServerMessages {

  def parsePacket(command: Int, data: ByteIterator): Packet = command match {
    case 0 => Heartbeat
    case 1 => Connect
    case 2 => Disconnect
    case 4 => new LoginSuccess(data)
    case 5 => new LoginFailed(data)
    case 12 => new UserAdded(data)
    case 68 => new MachineAdded(data)
    case 72 => new MachineChangeStatus(data)
    case 98 => new NewPoint(data)
    case 99 => new NewTextEvent(data)
    case 275 => new UserModule(data)
    case 351 => new MachineGroupAdded(data)
    case 360 => FinishLoadLoginData
    case _ => UnknownMessage
  }

  object UnknownMessage extends Packet
  object Heartbeat extends Packet
  object Connect extends Packet
  object Disconnect extends Packet
  object FinishLoadLoginData extends Packet

  class LoginSuccess(data: ByteIterator) extends Packet {
    val databaseId = data.getInt
    val restrictedObserver = data.getInt
  }

  class LoginFailed(data: ByteIterator) extends Packet {
    val result = data.getInt
    val resultString = readString(data, STATUS_STRING_LENGTH)
  }

  class DbPoint(data: ByteIterator) extends Packet {
    val latitude = data.getFloat
    val longitude = data.getFloat
    val time = readInstant(data)
    val eventCode = data.getShort
    val dop = data.getDouble
    val value = data.getInt
    val speed = data.getInt
    val heading = data.getInt
    val isOldMessage = data.getByte
    data.getByte //alignment byte
  }

  class NewPoint(data: ByteIterator) extends Packet {
    val point = new DbPoint(data)
    val phoneNumber = readString(data, PHONE_NUMBER_LENGTH)
    val invNumber = readString(data, PHONE_NUMBER_LENGTH)
    val odometer = data.getInt
    val zoneNumber = data.getInt
    val pgNumber = data.getInt
    //<SENSOR_EVENT_STATE>
    //<DB_SEN_STATE>
    val sensorState = data.getByte
    //</DB_SEN_STATE>
    val otherState = data.getByte
    //</SENSOR_EVENT_STATE>
    val validGps = data.getInt
    val tableIndex = data.getInt
    val isExtEvent = data.getInt
    val isGprs = data.getInt
    val gsmGps = new DbDataGsmGps(data)
    val datav3 = new DbDataV3(data)
    val extVal = data.getShort
    val adc = new DbDataAdc(data)
    val mode = new DbStateGps(data)
    val altitude = data.getInt
    val progEventType = readString(data, STATUS_STRING_LENGTH)
    val eventSrc = data.getByte
    val eventExt = data.getByte
    val dataSrcType = data.getInt
    val precSpeed = data.getInt
  }

  class DbDataGsmGps(data: ByteIterator) extends Packet {
    val sat = data.getByte
    val inZone = data.getByte
    val antShort = data.getByte
    val rssi = data.getByte
  }

  class DbDataAdc(data: ByteIterator) extends Packet {
    val adcAx = data.getFloat
    val adcBx = data.getFloat
    val sen5AdcAx = data.getFloat
    val sen5AdcBx = data.getFloat
    val sen6AdcAx = data.getFloat
    val sen6AdcBx = data.getFloat
  }

  class DbStateGps(data: ByteIterator) extends Packet {
    val isValidGps = data.getByte
    val isValidGpsFixTime = data.getByte
    val mode = data.getShort
    val gpsFixTime = data.getShort
  }

  class MachineAdded(data: ByteIterator) extends Packet {
    val point = new DbPoint(data)
    val machineType = data.getInt
    val status = data.getInt
    val oldPhoneNumber = readString(data, PHONE_NUMBER_LENGTH)
    val phoneNumber = readString(data, PHONE_NUMBER_LENGTH)
    val statusString = readString(data, STATUS_STRING_LENGTH)
    val number = readString(data, PHONE_NUMBER_LENGTH)
    val description = readString(data, PHONE_NUMBER_LENGTH)
    val smsCenter = readString(data, PHONE_NUMBER_LENGTH)
    val text = readString(data, PACKET_TEXT_LENGTH)
    val routeName = readString(data, STATUS_STRING_LENGTH)
    val routeDescription = readString(data, PACKET_TEXT_LENGTH)
    val responsibleLogin = readString(data, PHONE_NUMBER_LENGTH)
    val routeId = data.getInt
    val routeSubId = data.getInt
    val routeStatus = data.getInt
    val switchStatus = data.getInt
    val switchTime = readInstant(data)
    val	routeDbId = data.getInt
    val modeName = readString(data, PHONE_NUMBER_LENGTH)
    val responsibleStatus = data.getInt
    val isOnRoute = data.getInt
    val lastQueryTime = readInstant(data)
    val lastResponseTime = readInstant(data)
    val versionNumber = readString(data, PHONE_NUMBER_LENGTH)
    val serialNumber = readString(data, PHONE_NUMBER_LENGTH)
    val dataConnectionState = data.getInt
    val dataConnectionType = data.getInt
    val guardState = data.getInt
    val getPositionState = data.getInt
    val isCurrentModeExt = data.getInt
    val dataBaseID = readString(data, PHONE_NUMBER_LENGTH)
    val adcAx = data.getFloat
    val adcBx = data.getFloat
    val sen5AdcAx = data.getFloat
    val sen5AdcBx = data.getFloat
    val lastGpVar = new GpVar(data)
    val lastGsVar = new GsVar(data)
    val сreateTime = readInstant(data)
    val sen6AdcAx = data.getFloat
    val sen6AdcBx = data.getFloat
    val groupName = readString(data, PHONE_NUMBER_LENGTH)
    val groupID = readString(data, PHONE_NUMBER_LENGTH)
    val lastDbData = new DbDataV2(data)
    val lastSwitchedState = data.getInt
    val lastSwitchedStateTime = readInstant(data)
    val isLostGps = data.getInt
  }

  class GpVar(data: ByteIterator) extends Packet {
    val latitude = toDegrees(data.getFloat) /* Широта, радианы */
    val longitude = toDegrees(data.getFloat) /* Долгота, радианы */
    val timestamp = readInstant(data) /* Время получения координат */
    val speed = data.getShort /* Текущая скорость, км/ч */
    val bearing =  data.getShort /* Направление движения, град. */
    val accuracy = data.getByte /* Погрешность GPS PDOP*10 */
    /*
		unsigned char	sat :4;			/* Количество принимаемых GPS спутников */
		unsigned char	in_zone :1;		/* Флаг нахождение в зоне */
		unsigned char	ant_short :1;	/* Замыкание кабеля антены */
		unsigned char	guard :1;		/* Состояние охраны */
		unsigned char	move :1;		/* Событие движение */
         */
    val bits = data.getByte
    val logLen = data.getShort /* Кол-во точек в журнале */
  }

  class GsVar(data: ByteIterator) extends Packet {
    val rssi = data.getByte					/* received signal strength indication */
    val ber = data.getByte					/* bit error rate */
    val net = readString(data, GSM_NET_NAME_LENGTH)		/* Название GSM сети */

  }

  class DbDataV2(data: ByteIterator) extends Packet {
    val	odom = data.getInt				/* общий пробег, м */
    val	motor = data.getInt				/* моточасы, секунды */
    val	smsMaster = data.getByte	/* Индекс ведомого номера для отправки SMS */
    val	relayState = data.getByte	/* состояния 3-х реле (включено/выключено) */
    val gpsPwr  = data.getByte		/* состояние GPS приемника (включен/выключен) */
    val senState = data.getByte		/* Состояние сенсоров. Заполняется при отправке */
    val	vBortx10 = data.getByte		/* Напряжение бортовой сети, V_BORT*10 */
    val vAccx10 = data.getByte		/* Напряжение резервного аккумулятора, V_ACC*10 */
    val vSen5 = data.getByte			/* Напряжение сенсора 5, V_SEN5*10 */
    val vSen6 = data.getByte			/* Напряжение сенсора 6, V_SEN5*10 */
    val sen5 = data.getShort		  /* Значение сенсора 5 */
    val sen6 = data.getShort			/* Значение сенсора 6 */
  }

  class DbDataV3(data: ByteIterator) extends Packet {
    val relayState = data.getByte /* состояния 3-х реле (включено/выключено) */
    val vBortx10 = data.getByte   /* Напряжение бортовой сети, V_BORT*10 */
    val vAccx10 = data.getByte		/* Напряжение резервного аккумулятора, V_ACC*10 */
    val	vSen5x10 = data.getByte		/* Напряжение сенсора 5, V_SEN5*10 */
    val	vSen6x10 = data.getByte		/* Напряжение сенсора 6, V_SEN5*10 */
    data.getByte //alignment byte
    val sen5 = data.getShort			/* Значение сенсора 5 */
    val sen6 = data.getShort			/* Значение сенсора 6 */
  }

  class UserAdded(data: ByteIterator) extends Packet {
    val login = readString(data, PHONE_NUMBER_LENGTH)
    val password = readString(data, PHONE_NUMBER_LENGTH)
    val roles = new UserRoles(data)
    val status = data.getInt
    val statusString = readString(data, STATUS_STRING_LENGTH)
    val description = readString(data, STATUS_STRING_LENGTH)
    val loginCount = data.getInt
    val isGroup = data.getInt
    val databaseId = data.getInt
  }

  class UserRoles(data: ByteIterator) extends Packet {
    val bits = data.getInt
    //TODO: role getters
  }

  class UserModule(data: ByteIterator) extends Packet {
    val command = data.getInt
    val login = readString(data, PHONE_NUMBER_LENGTH)
    val phoneNumber = readString(data, PHONE_NUMBER_LENGTH)
  }

  class MachineGroupAdded(data: ByteIterator) extends Packet {
    val name = readString(data, PHONE_NUMBER_LENGTH)
    val id = readString(data, PHONE_NUMBER_LENGTH)
    val parent = readString(data, STATUS_STRING_LENGTH)
  }

  class NewTextEvent(data: ByteIterator) extends Packet {
    val id = data.getInt
    val eventType = data.getInt
    val infoType = data.getInt
    val displayType = data.getInt
    val destination = data.getInt
    val destinationId = data.getInt
    val time = readInstant(data)
    val showTime = readInstant(data)
    val description = readString(data, PACKET_TEXT_LENGTH)
    val responsible = readString(data, PHONE_NUMBER_LENGTH)
    val deviceName = readString(data, PHONE_NUMBER_LENGTH)
    val alarmConfig = data.getInt
    val responsibleIP = readString(data, PHONE_NUMBER_LENGTH)
    val event = data.getShort
    val value = data.getInt
    val eventTime = readInstant(data)
  }

  class MachineChangeStatus(data: ByteIterator) extends Packet {
    val status = data.getInt
    val phoneNumber = readString(data, PHONE_NUMBER_LENGTH)
    val statusString = readString(data, STATUS_STRING_LENGTH)
  }

}

