package autotracker

import akka.actor.{ReceiveTimeout, Actor}
import akka.io.{IO, Tcp}
import akka.util.ByteString
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.language.postfixOps
import akka.event.Logging
import annotation.tailrec
import akka.io.Tcp._
import java.net.InetSocketAddress
import akka.io.Tcp.Connected
import akka.io.Tcp.Register
import akka.io.Tcp.Connect
import akka.io.Tcp.CommandFailed

/**
 * Created by: vnechaev
 * Date: 24.01.13
 */
class ServerConnection(host: String, port: Int) extends Actor {
  val log = Logging(context.system, this)

  var data: ByteString = ByteString.empty
  var header: PacketHeader = HeaderHeader

  import context.system

  log.debug("Connecting to {}:{}", host, port)
  IO(Tcp) ! Connect(new InetSocketAddress(host,port))

  val heartbeatTimer = context.system.scheduler.schedule(1 minute , 1 minute, self, ReceiveTimeout)

  def receive = {
    case CommandFailed(_: Connect) => {
      log.error("Connect failed")
      context.stop(self) //TODO: handle restart
    }
    case Connected(remote, local) => {
      log.info("Connected to {}", remote)
      val connection = sender()
      connection ! Register(self)
      context become {
        case Received(bytes) => {
          data = data ++ bytes
          parsePackets()
        }
        case CommandFailed(_: Write) => {
          log.error("Write error")
          throw new RuntimeException
        }
        case _: ConnectionClosed => {
          log.warning("Connection closed")
          context.stop(self) //TODO: handle restart
        }
        case ReceiveTimeout => {
          self ! ClientMessages.Heartbeat
          //log.debug("Sent heartbeat message")
        }
        case msg: ClientDataPacket => {
          connection ! Write(new PacketHeader(msg.length, msg.commandId).write)
          connection ! Write(msg.write)
          //log.debug("Sent message of type {} and length {}", msg.commandId, msg.length)
        }
      }
    }
  }

  @tailrec
  private def parsePackets() {
    if (data.length >= header.dataSize) {
      val dataSize = header.dataSize
      if (header == HeaderHeader) {
        header = new PacketHeader(data.iterator)
      } else {
        //log.debug("msg type {} size {}", header.commandId, header.dataSize)
        context.parent ! ServerMessages.parsePacket(header.commandId, data.iterator)
        header = HeaderHeader
      }
      data = data.drop(dataSize)
      parsePackets()
    }
  }

  override def postStop() {
    heartbeatTimer.cancel()
    log.info("Disconected")
  }

}
