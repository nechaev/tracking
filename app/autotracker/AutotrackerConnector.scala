package autotracker

import akka.actor._
import akka.event.Logging
import akka.pattern.{pipe, after}
import concurrent.duration._
import concurrent.{Promise, Future}
import java.util.concurrent.TimeoutException
import play.api.Configuration
import play.api.Play.current
import play.api.libs.concurrent.Akka
import reporting.ReportingContextRequest
import scala.language.postfixOps
import tracking.web._
import user._
import gis.{GeocodingServiceManager, GeocodingServiceRequest}
import tracking.web.TrackerDescriptionRequest
import user.UserMessage
import reporting.ReportingContext
import user.Login
import user.UserId
import tracking.web.TrackerStateRequest

/**
 * Created by: sgl
 * Date: 09.01.13
 */

class AutotrackerConnector(configuration: Configuration) extends Actor {
  private val log = Logging(context.system, this)
  private val (connName, login, password, host, port, databaseName, geocodingService, reportingDataSource) = readConfig(configuration)

  private var serverConnection: ActorRef = _
  private var databaseConnection: ActorRef = _


  import context.dispatcher

  override def preStart() {
    serverConnection = context.actorOf(Props(classOf[MainServerConnection], host, port, login, password, connName), "at-server")
    databaseConnection = context.actorOf(Props(classOf[DatabaseConnection], databaseName, connName), "at-database")
    UserManager.route(connName, self)
    TrackingServerManager.route(connName, self)
  }

  def receive = {
    case UserMessage(userId, Login(userPassword)) => {
      log.debug("Logging in {}", userId.userId)
      startUserSession(userId, userPassword).recover({
        case e: Exception => false
      }) pipeTo sender
    }
    case msg:FixPositionHeading => databaseConnection forward msg
    case msg:TrackerStateRequest => databaseConnection forward msg
    case msg:TrackerDescriptionRequest => databaseConnection forward msg
    case msg:TrackRequest => databaseConnection forward msg

    case msg@UserMessage(_, TrackingUserContextRequest) => databaseConnection forward msg
    case UserMessage(userId, ReportingContextRequest) => sender ! ReportingContext(userId, reportingDataSource)
    case UserMessage(_, GeocodingServiceRequest) => sender ! geocodingService.flatMap(GeocodingServiceManager.get)
  }

  private def readConfig(conf: Configuration) = (for {
    connName <- conf.getString("domain")
    host <- conf.getString("host")
    port <- conf.getInt("port")
    login <- conf.getString("login")
    password <- conf.getString("password")
    database <- conf.getString("database")
    geocodingService = conf.getString("geocoding")
    reportingDataSource = conf.getString("reportingDataSource").getOrElse(database)
  } yield (connName, login, password, host, port, database, geocodingService, reportingDataSource))
    .getOrElse(throw new IllegalArgumentException)


  def startUserSession(userId: UserId, userPassword: String): Future[Boolean] = {
    log.debug(s"Authenticating user {}", userId)
    val result = Promise[Boolean]()
    val userConn: ActorRef = context.actorOf(Props(classOf[UserServerConnection],
      host, port, userId, userPassword, connName, databaseConnection, result)
    )
    val timeout = after(1 minute, using=Akka.system.scheduler) {
      log.debug("Login timeout")
      userConn ! PoisonPill
      Future.failed(new TimeoutException)
    }
    Future firstCompletedOf Seq(result.future, timeout)
  }

  override def postStop() {
    super.postStop()
    log.debug("Autotracker connector stopped")
  }
}
