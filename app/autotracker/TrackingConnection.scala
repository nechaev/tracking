package autotracker

import akka.actor._
import autotracker.ClientMessages.Login
import akka.actor.Terminated

/**
 * Created by: sgl
 * Date: 24.01.13
 */
abstract class TrackingConnection(host: String, port: Int, login: String, password: String) extends Actor {

  private val serverConnection = context.actorOf(Props(classOf[ServerConnection], host, port))

  context.watch(serverConnection)

  def receive = {
    case ServerMessages.Connect => serverConnection ! new Login(2, login, password)
    case ServerMessages.Disconnect => context.stop(self)
    case msg: Packet => if(handleServerMessages isDefinedAt msg) handleServerMessages(msg)
    case Terminated => onDisconnect()
  }

  protected def handleServerMessages: PartialFunction[Packet, Unit]

  protected def onDisconnect() {
    context.stop(self)
  }

}
