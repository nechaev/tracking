package session

import akka.actor.{Status, ActorRef, Props, Actor}
import akka.event.Logging
import framework.SingletonActorManager
import java.security.MessageDigest
import java.util.Date
import javax.xml.bind.DatatypeConverter
import play.api.libs.iteratee.{Enumerator, Iteratee}
import play.api.libs.json.JsValue
import scala.language.existentials
import user._

/**
 * Created by: vnechaev
 * Date: 11.02.13
 */
class SessionManager extends Actor {
  private val log = Logging(context.system, this)

  import SessionManager._

  def receive = {
    case UserMessage(userId, Connect(clientHost)) => {
      log.debug("Attempting to create session for {} from {}", userId, clientHost)
      val sessionId = generateSessionId(userId, clientHost)
      log.debug("Created session {}", sessionId)
      sender ! Connected(sessionId, context.actorOf(Props(classOf[Session], userId, clientHost), sessionId))
    }
    case ConnectSession(sessionId, clientHost) => {
      context.child(sessionId) match {
        case Some(session) => sender ! Connected(sessionId, session)
        case None => sender ! Status.Failure(SessionNotFoundException(sessionId))
      }
    }
  }

  private lazy val hashGenerator = MessageDigest.getInstance("MD5")
  private def generateSessionId(userId: UserId, clientHost: String) = {
    hashGenerator.reset()
    hashGenerator.update((userId.toString + clientHost + new Date().getTime.toString).getBytes)
    DatatypeConverter.printBase64Binary(hashGenerator.digest()).replace('+','-').replace('/','_')
  }
}

object SessionManager extends SingletonActorManager[SessionManager] {
  val ACTOR_NAME = "session-manager"
  val SESSION_ID_KEY = "sid"
  val CONNECTION_ID_KEY = "cid"
  val PING_MESSAGE_TYPE = "connection.ping"

  //Manager events
  case class Connect(clientHost: String) // Start new session
  case class ConnectSession(sessionId: String, clientHost: String) //Connect to existing session
  case class Connected(sessionId: String, session: ActorRef)

  //Session events
  object StartConnection //Start new connection
  case class FindConnection(connectionName: String)
  case class ConnectionStarted(input: Iteratee[JsValue, Unit], output: Enumerator[JsValue], connection: ActorRef)
  object Logout //Terminates session
  case class SessionStarted(userId: UserId, session: ActorRef) // sent by Session to EventStream
  case class SessionStopped(userId: UserId, session: ActorRef) // sent by Session to EventStream
  object ClientPing
  //case class SessionInfo(userId: UserId, clientHost: String)
}

case class SessionNotFoundException(sessionId: String) extends RuntimeException
