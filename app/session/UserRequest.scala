package session

import akka.actor.ActorRef

/**
 * Author: sgl
 * Date: 08.09.13
 */
case class UserRequest(request: Any, forwardTo: ActorRef)
