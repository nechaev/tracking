package session

import akka.actor.ActorSelection

/**
 * Author: sgl
 * Date: 08.09.13
 */
trait SessionForwardable {
  def forwardTo: ActorSelection
}
