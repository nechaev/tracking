package session

import akka.actor.{Status, Props, ReceiveTimeout, Actor}
import akka.event.Logging
import akka.pattern.{ask,pipe}
import framework.Defaults.timeout
import play.api.Play.current
import play.api.libs.concurrent.Akka
import scala.concurrent.duration._
import scala.language.postfixOps
import session.SessionManager._
import user.{UserId, UserMessage, UserEventBus}

/**
 * Created by: vnechaev
 * Date: 11.02.13
 */
class Session(userId: UserId, clientHost: String) extends Actor {
  private val log = Logging(context.system, this)
  import context.dispatcher
  def receive = {
    case msg: OutboundClientMessage => context.children.foreach(_ ! msg)
//    case ConnectSession(sessionId, host) => if (host == clientHost) sender ! Connected(sessionId, self)
//      else sender ! Status.Failure(
//        new RuntimeException(s"Session request from unauthenticated host:${host} instead of ${clientHost}")
//      )
    case StartConnection => {
      log.debug("Starting connection for {} from {}", userId, clientHost)
      context.actorOf(Props(classOf[Connection], userId)) ? StartConnection pipeTo sender
    }
    case FindConnection(connectionName) => context.child(connectionName) match {
      case Some(connection) => connection ? StartConnection pipeTo sender
      case None => {
        log.error("Connection lookup failed for {}", connectionName)
        sender ! Status.Failure(new RuntimeException(s"Connection $connectionName not found"))
      }
    }
    case Logout => context.stop(self)
    case ReceiveTimeout => {
      log.debug("Session {} stopped due to inactivity", self.path)
      context.stop(self)
    }
    case ClientPing => {
      log.debug("Received client heartbeat message")
    }
    //case Request(contentType) if contentType == classOf[SessionInfo] => sender ! SessionInfo(userId, clientHost)
    case UserRequest(request, forwardTo) => forwardTo forward UserMessage(userId, request)
    case msg:SessionForwardable => {
      log.debug("Forwarding {} to {}", msg, userId)
      msg.forwardTo.tell(UserMessage(userId, msg), sender)
    }
  }

  override def preStart() {
    super.preStart()
    context.setReceiveTimeout(30 minutes)
    UserEventBus.subscribe(self, userId.toString)
    Akka.system.eventStream.publish(SessionStarted(userId, self))
  }

  override def postStop() {
    super.postStop()
    UserEventBus.unsubscribe(self, userId.toString)
    Akka.system.eventStream.publish(SessionStopped(userId, self))
  }
}
