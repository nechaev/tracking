package session

import akka.event.{LookupClassification, ActorEventBus}
import org.slf4j.LoggerFactory

/**
 * Created by: vnechaev
 * Date: 11.02.13
 */
object InboundMessageBus extends ActorEventBus with LookupClassification { //with Logging { Enable with scala-logging
  private lazy val logger = LoggerFactory.getLogger(getClass)

  type Event = InboundClientMessage
  type Classifier = String

  protected def mapSize(): Int = 1000

  protected def classify(event: InboundClientMessage): String = event.messageType
  protected def publish(event: InboundClientMessage, subscriber: InboundMessageBus.Subscriber) {
    logger.trace("Sending {} to {}", event, subscriber.path, null)
    subscriber ! event
  }
}
