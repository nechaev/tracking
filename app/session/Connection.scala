package session

import akka.actor.{Props, Stash, ReceiveTimeout, Actor}
import akka.event.Logging
import akka.pattern.pipe
import play.api.libs.iteratee.Concurrent.Channel
import play.api.libs.iteratee.{Iteratee, Concurrent}
import play.api.libs.json.JsString
import play.api.libs.json.{Json, JsValue}
import scala.concurrent.duration._
import scala.language.postfixOps
import session.SessionManager._
import user.{UserMessage, UserId}

/**
 * Created by: sgl
 * Date: 11.02.13
 */
class Connection(userId: UserId) extends Actor with Stash {
  private val log = Logging(context.system, this)

  import context.dispatcher

  private var outputChannel: Option[Channel[JsValue]] = None

  private def outputEnumerator = Concurrent.unicast[JsValue](onStart = { channel =>
    self ! ConnectionChannelOpened(channel)
    self ! OutboundClientMessage("",PING_MESSAGE_TYPE, JsString(""))
  }, onComplete = {
    self ! ConnectionChannelClosed
  })
  private def inputIteratee = Iteratee.foreach[JsValue](receiveMessage).map(
    _ => waitForReconnect()
  )
  private def waitForReconnect() {
    context.system.scheduler.scheduleOnce(2 minute, self, ReconnectTimeout)
  }

  def receive = {
    case ConnectionChannelOpened(channel) => {
      outputChannel = Some(channel)
      unstashAll()
      log.debug("Connection channel opened")
      //context.system.scheduler.scheduleOnce(15 seconds, self, Disconnect) // for connection recovery debugging
    }
    case ConnectionChannelClosed => {
      outputChannel = None
      log.debug("Connection channel closed")
    }
    case StartConnection => sender ! ConnectionStarted(inputIteratee, outputEnumerator, self)
    case msg: OutboundClientMessage => pushMessage(msg)
    case ReceiveTimeout => context.stop(self)
    case ReconnectTimeout => if (outputChannel == None) {
      log.debug("Stopping connection due to reconnect timeout")
      context.stop(self)
    }
    case Disconnect => outputChannel.foreach(_.eofAndEnd())
    case NextMessage => {
      Concurrent.unicast[JsValue](onStart = { channel =>
        self ! ConnectionChannelOpened(channel)
      }, onComplete = {
        self ! ConnectionChannelClosed
      }).map {msg =>
        self ! Disconnect
        msg
      } |>>> Iteratee.fold(Nil.asInstanceOf[List[JsValue]])((msgs, msg) => msg :: msgs) pipeTo sender
    }
    case GetOrCreateChild(props, name) => {
      context.child(name) match {
        case Some(ref) => sender ! ref
        case None => sender ! context.actorOf(props, name)
      }
    }
    case msg:SessionForwardable => {
      log.debug("Forwarding {} to {}", msg, userId)
      msg.forwardTo.tell(UserMessage(userId, msg), sender)
    }
  }

  private def pushMessage(msg: OutboundClientMessage) {
    outputChannel match {
      case Some(channel) => {
        val json = Json.obj(
          "id" -> msg.messageId,
          "type" -> msg.messageType,
          "body" -> msg.messageBody
        )
        log.debug("Sent {}", json)
        channel.push(json)
      }
      case None => stash()
    }
  }

  private def receiveMessage(msg: JsValue) {
    for {
      msgId <- (msg \ "id").asOpt[String]
      messageType <- (msg \ "type").asOpt[String]
    } {
      if (messageType == PING_MESSAGE_TYPE) {
        context.parent ! ClientPing
        self ! OutboundClientMessage(msgId, PING_MESSAGE_TYPE, JsString(""))
      }
      else {
        log.debug("Received {}", msg)
        InboundMessageBus.publish(InboundClientMessage(
          messageId = msgId,
          messageType = messageType,
          messageBody = msg \ "body",
          connection = self,
          session = context.parent)
        )
      }
    }
  }

  override def preStart() {
    super.preStart()
    log.debug("Connection started")
    context.setReceiveTimeout(30 minutes)
  }

  override def postStop() {
    super.postStop()
    log.debug("Connection closed")
    outputChannel.foreach(_.eofAndEnd())
  }
}

case class ConnectionChannelOpened(channel: Channel[JsValue])
case class GetOrCreateChild(props: Props, name: String)
case object ConnectionChannelClosed
case object ReconnectTimeout
case object Disconnect
case object NextMessage
