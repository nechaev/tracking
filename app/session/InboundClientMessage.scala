package session

import play.api.libs.json.JsValue
import akka.actor.ActorRef
import user.UserId

/**
 * Created by: vnechaev
 * Date: 11.02.13
 */
case class InboundClientMessage(
  messageId: String,
  messageType: String,
  messageBody: JsValue,
  connection: ActorRef,
  session: ActorRef
)
