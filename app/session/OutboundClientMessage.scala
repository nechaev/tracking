package session

import play.api.libs.json.JsValue

/**
 * Created by: sgl
 * Date: 11.02.13
 */
case class OutboundClientMessage(
  messageId:String,
  messageType: String,
  messageBody: JsValue
)
